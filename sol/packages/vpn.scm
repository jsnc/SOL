(define-module (sol packages vpn)
  #:use-module (guix build-system copy)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix gexp))

(define-public mullvad-vpn-bin
  (package
    (name "mullvad-vpn-bin")
    (version "2023.5-beta2")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://github.com/mullvad/mullvadvpn-app/releases/download/"
                    version
                    "/"
                    "MullvadVPN-"
                    version
                    "_amd64.deb"))
              (sha256
               (base32
                "04l6v3p30zbzyn1bvjihacm6limy726afnyf9v6g9rza6dd6bjdc"))))
    (build-system copy-build-system)
    (supported-systems '("x86_64-linux"))
    (home-page "https://github.com/mullvad/mullvad-app")
    (synopsis "Mullvad app desktop client and daemon")
    (description
     "This binary package contains both the Mullvad VPN daemon as well as
the desktop client in precompiled form")
    (license license:gpl3)))

; https://github.com/mullvad/mullvadvpn-app/releases/download/2023.5-beta2/MullvadVPN-2023.5-beta2_arm64.deb
