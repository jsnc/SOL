(define-module (sol packages wm)
  #:use-module (guix packages)
  #:use-module (guix build-system cargo)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages crates-graphics)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages xdisorg)
  #:use-module (sol packages crates-io)
  #:use-module (sol packages crates-graphics))

;;; SHOWSTOPPERS:

;; A newer version of the rust compiler is needed (>=1.70)

;; <https://issues.guix.gnu.org/64804>

;;  Versioning issue in the project's Cargo.toml file

;; <https://github.com/Horus645/swww/issues/174>

(define-public swww-0.8
  (package
    (name "swww")
    (version "0.8.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url (string-append "https://github.com/Horus645/" name))
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0c4ra7slxvgjxhziaq4hx2fwbrkj5ij2n8znpigxmz1zd43fmkzm"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs
       ;; utils
       (("rust-lazy-static" ,rust-lazy-static-1)
        ("rust-lzzzz" ,rust-lzzzz-1)
        ("rust-rkyv" ,rust-rkyv-0.7)
        ;; swww-daemon
        ("rust-log" ,rust-log-0.4)
        ("rust-simplelog" ,rust-simplelog-0.12)
        ("rust-wayland-client" ,rust-wayland-client-0.30)
        ("rust-smithay-client-toolkit" ,rust-smithay-client-toolkit-0.17)
        ("rust-nix" ,rust-nix-0.26)
        ("rust-keyframe" ,rust-keyframe-1)
        ("rust-rkyv" ,rust-rkyv-0.7)
        ("rust-sd-notify" ,rust-sd-notify-0.4)
        ;; swww
        ("rust-image" ,rust-image-0.24)
        ("rust-fast-image-resize" ,rust-fast-image-resize-2)
        ("rust-clap" ,rust-clap-4)
        ("rust-clap-complete" ,rust-clap-complete-4)
        ("rust-rand" ,rust-rand-0.8))
       #:cargo-development-inputs
       ;; utils
       (("rust-criterion" ,rust-criterion-0.5)
        ;; swww-daemon
        ("rust-rand" ,rust-rand-0.8)
        ;; swww
        ("rust-assert-cmd" ,rust-assert-cmd-2))))
    ;; swww depends on the lz4 compression algorithim at runtime
    (propagated-inputs (list lz4))
    (home-page "https://github.com/Horus645/")
    (synopsis "Solution to your wayland wallpaper woes")
    (description
     "Efficient animated wallpaper daemon for wayland, controlled at runtime.")
    (license license:gpl3)))

(define-public swww-0.7
  (package
    (inherit swww-0.8)
    (name "swww")
    (version "0.7.3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url (string-append "https://github.com/Horus645/" name))
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "09i9xl4z3ir10jms632fwfgvnc24p1rzrlfgl76kpdazmf5x9k77"))))
    (arguments
     `(#:cargo-inputs
       ;; utils
       (("rust-lazy-static" ,rust-lazy-static-1)
        ("rust-lzzzz" ,rust-lzzzz-1)
        ("rust-rkyv" ,rust-rkyv-0.7)
        ;; swww-daemon
        ("rust-log" ,rust-log-0.4)
        ("rust-simplelog" ,rust-simplelog-0.12)
        ("rust-wayland-client" ,rust-wayland-client-0.29)
        ("rust-smithay-client-toolkit" ,rust-smithay-client-toolkit-0.16)
        ("rust-nix" ,rust-nix-0.26)
        ("rust-keyframe" ,rust-keyframe-1)
        ("rust-rkyv" ,rust-rkyv-0.7)
        ("rust-sd-notify" ,rust-sd-notify-0.4)
        ;; swww
        ("rust-image" ,rust-image-0.24)
        ("rust-fast-image-resize" ,rust-fast-image-resize-2)
        ("rust-clap" ,rust-clap-4)
        ("rust-clap-complete" ,rust-clap-complete-4)
        ("rust-rand" ,rust-rand-0.8))
       #:cargo-development-inputs
       ;; utils
       (("rust-criterion" ,rust-criterion-0.5)
        ;; swww-daemon
        ("rust-rand" ,rust-rand-0.8)
        ;; swww
        ("rust-assert-cmd" ,rust-assert-cmd-2))))
    (inputs (list libxkbcommon))
    (native-inputs (list pkg-config))))
