(define-module (sol packages crates-graphics)
  #:use-module (guix packages)
  #:use-module (guix build-system cargo)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages crates-graphics)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gnustep)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)
  #:use-module (sol packages crates-io))

(define-public rust-amethyst-0.10
  (package
    (name "rust-amethyst")
    (version "0.10.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "amethyst" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1gj74wan7a3j39zb4rnc4754aym7cj69lnc27yibdx3g1xvcn4xb"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-amethyst-animation" ,rust-amethyst-animation-0.5)
                       ("rust-amethyst-assets" ,rust-amethyst-assets-0.6)
                       ("rust-amethyst-audio" ,rust-amethyst-audio-0.5)
                       ("rust-amethyst-config" ,rust-amethyst-config-0.9)
                       ("rust-amethyst-controls" ,rust-amethyst-controls-0.4)
                       ("rust-amethyst-core" ,rust-amethyst-core-0.5)
                       ("rust-amethyst-derive" ,rust-amethyst-derive-0.3)
                       ("rust-amethyst-input" ,rust-amethyst-input-0.6)
                       ("rust-amethyst-locale" ,rust-amethyst-locale-0.4)
                       ("rust-amethyst-network" ,rust-amethyst-network-0.3)
                       ("rust-amethyst-renderer" ,rust-amethyst-renderer-0.10)
                       ("rust-amethyst-ui" ,rust-amethyst-ui-0.5)
                       ("rust-amethyst-utils" ,rust-amethyst-utils-0.5)
                       ("rust-crossbeam-channel" ,rust-crossbeam-channel-0.3)
                       ("rust-derivative" ,rust-derivative-1)
                       ("rust-fern" ,rust-fern-0.5)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-rayon" ,rust-rayon-1)
                       ("rust-rustc-version-runtime" ,rust-rustc-version-runtime-0.1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-derive" ,rust-serde-derive-1)
                       ("rust-thread-profiler" ,rust-thread-profiler-0.3)
                       ("rust-vergen" ,rust-vergen-3)
                       ("rust-winit" ,rust-winit-0.18))
       #:cargo-development-inputs (("rust-amethyst-gltf" ,rust-amethyst-gltf-0.5)
                                   ("rust-env-logger" ,rust-env-logger-0.5)
                                   ("rust-genmesh" ,rust-genmesh-0.6)
                                   ("rust-ron" ,rust-ron-0.4))))
    (home-page "https://amethyst.rs/")
    (synopsis "Data-oriented game engine written in Rust")
    (description "Amethyst is a data-oriented game engine written in Rust.")
    (license (list license:expat license:asl2.0))))

(define-public rust-amethyst-animation-0.5
  (package
    (name "rust-amethyst-animation")
    (version "0.5.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "amethyst-animation" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1dws0s896aj53piscv2xxlnzy1gzwhnk1cj6ff9hn1sqzc8ciqvy"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-amethyst-assets" ,rust-amethyst-assets-0.6)
                       ("rust-amethyst-core" ,rust-amethyst-core-0.5)
                       ("rust-amethyst-derive" ,rust-amethyst-derive-0.3)
                       ("rust-amethyst-renderer" ,rust-amethyst-renderer-0.10)
                       ("rust-derivative" ,rust-derivative-1)
                       ("rust-fnv" ,rust-fnv-1)
                       ("rust-hibitset" ,rust-hibitset-0.5)
                       ("rust-itertools" ,rust-itertools-0.7)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-minterpolate" ,rust-minterpolate-0.4)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-thread-profiler" ,rust-thread-profiler-0.3))))
    (home-page "https://amethyst.rs/")
    (synopsis "Animation support for Amethyst")
    (description "This package provides animation support for Amethyst.")
    (license (list license:expat license:asl2.0))))

(define-public rust-amethyst-assets-0.6
  (package
    (name "rust-amethyst-assets")
    (version "0.6.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "amethyst-assets" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1nprakfanj2vyaaxsid671g0rkni6y92c3a6d45ysykasgx3bjp3"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-amethyst-core" ,rust-amethyst-core-0.5)
                       ("rust-crossbeam" ,rust-crossbeam-0.4)
                       ("rust-derivative" ,rust-derivative-1)
                       ("rust-error-chain" ,rust-error-chain-0.12)
                       ("rust-fnv" ,rust-fnv-1)
                       ("rust-hibitset" ,rust-hibitset-0.5)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-parking-lot" ,rust-parking-lot-0.6)
                       ("rust-rayon" ,rust-rayon-1)
                       ("rust-ron" ,rust-ron-0.4)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-shred" ,rust-shred-0.7)
                       ("rust-shred-derive" ,rust-shred-derive-0.5)
                       ("rust-thread-profiler" ,rust-thread-profiler-0.3))))
    (home-page "https://amethyst.rs/")
    (synopsis "Asynchronous asset management for games")
    (description
     "This package provides asynchronous asset management for games.")
    (license (list license:expat license:asl2.0))))

(define-public rust-amethyst-audio-0.5
  (package
    (name "rust-amethyst-audio")
    (version "0.5.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "amethyst-audio" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "08xb4vlxi11nlafnfsmqbb3ag1129kifkx7xjwmsvhmh6fhv9yhd"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-amethyst-assets" ,rust-amethyst-assets-0.6)
                       ("rust-amethyst-core" ,rust-amethyst-core-0.5)
                       ("rust-cpal" ,rust-cpal-0.8)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-rodio" ,rust-rodio-0.8)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-smallvec" ,rust-smallvec-0.6)
                       ("rust-thread-profiler" ,rust-thread-profiler-0.3))))
    (home-page "https://amethyst.rs/")
    (synopsis "Audio support for Amethyst")
    (description "This package provides audio support for Amethyst.")
    (license (list license:expat license:asl2.0))))

(define-public rust-amethyst-config-0.9
  (package
    (name "rust-amethyst-config")
    (version "0.9.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "amethyst-config" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0bj7bq851d4100028iv1iiz1j8yi5h6a3w23hn2jnbngl0s0an72"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-log" ,rust-log-0.4)
                       ("rust-ron" ,rust-ron-0.4)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-thread-profiler" ,rust-thread-profiler-0.3))
       #:cargo-development-inputs (("rust-serde-derive" ,rust-serde-derive-1))))
    (home-page "https://amethyst.rs/")
    (synopsis "Config support for Amethyst")
    (description
     "This package provides loading from .ron files into Rust structures
with defaults to prevent hard errors.")
    (license (list license:expat license:asl2.0))))

(define-public rust-amethyst-controls-0.4
  (package
    (name "rust-amethyst-controls")
    (version "0.4.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "amethyst-controls" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0icv41rn46pwkvj2vp91gkrr1mnd709rk3ym5srmchdq5g8693k9"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-amethyst-assets" ,rust-amethyst-assets-0.6)
                       ("rust-amethyst-core" ,rust-amethyst-core-0.5)
                       ("rust-amethyst-input" ,rust-amethyst-input-0.6)
                       ("rust-amethyst-renderer" ,rust-amethyst-renderer-0.10)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-thread-profiler" ,rust-thread-profiler-0.3)
                       ("rust-winit" ,rust-winit-0.18))))
    (home-page "https://amethyst.rs/")
    (synopsis "Amethyst controls")
    (description "This package provides Amethyst controls.")
    (license (list license:expat license:asl2.0))))

(define-public rust-amethyst-core-0.5
  (package
    (name "rust-amethyst-core")
    (version "0.5.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "amethyst-core" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1q91bdx5dj7lalprlnk50bdzqc62xzy3ipx0pzr4qsra39z2f083"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-approx" ,rust-approx-0.3)
                       ("rust-error-chain" ,rust-error-chain-0.12)
                       ("rust-fnv" ,rust-fnv-1)
                       ("rust-hibitset" ,rust-hibitset-0.5)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-nalgebra" ,rust-nalgebra-0.16)
                       ("rust-rayon" ,rust-rayon-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-shred" ,rust-shred-0.7)
                       ("rust-shrev" ,rust-shrev-1)
                       ("rust-specs" ,rust-specs-0.14)
                       ("rust-specs-hierarchy" ,rust-specs-hierarchy-0.3)
                       ("rust-thread-profiler" ,rust-thread-profiler-0.3))
       #:cargo-development-inputs (("rust-amethyst" ,rust-amethyst-0.10))))
    (home-page "https://amethyst.rs/")
    (synopsis "Amethyst core")
    (description "This package provides the Amethyst core crate.")
    (license (list license:expat license:asl2.0))))

(define-public rust-amethyst-derive-0.3
  (package
    (name "rust-amethyst-derive")
    (version "0.3.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "amethyst-derive" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0015jvp7mddkdbgisz17v4kga1q4ymshw38ildy5kxx4dk3rvyrd"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-0.4)
                       ("rust-quote" ,rust-quote-0.6)
                       ("rust-syn" ,rust-syn-0.15))
       #:cargo-development-inputs (("rust-amethyst-assets" ,rust-amethyst-assets-0.6)
                                   ("rust-amethyst-core" ,rust-amethyst-core-0.5))))
    (home-page "https://amethyst.rs/")
    (synopsis "Amethyst derive")
    (description "The Amethyst derive crate.")
    (license (list license:expat license:asl2.0))))

(define-public rust-amethyst-gltf-0.5
  (package
    (name "rust-amethyst-gltf")
    (version "0.5.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "amethyst-gltf" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "09x0x26avfyljzxvxy8265vcgj7b3ylllg970yj4wdpg6rhlbzw8"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-amethyst-animation" ,rust-amethyst-animation-0.5)
                       ("rust-amethyst-assets" ,rust-amethyst-assets-0.6)
                       ("rust-amethyst-core" ,rust-amethyst-core-0.5)
                       ("rust-amethyst-renderer" ,rust-amethyst-renderer-0.10)
                       ("rust-base64" ,rust-base64-0.9)
                       ("rust-fnv" ,rust-fnv-1)
                       ("rust-gfx" ,rust-gfx-0.17)
                       ("rust-gltf" ,rust-gltf-0.11)
                       ("rust-hibitset" ,rust-hibitset-0.5)
                       ("rust-itertools" ,rust-itertools-0.7)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-mikktspace" ,rust-mikktspace-0.1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-thread-profiler" ,rust-thread-profiler-0.3))))
    (home-page "https://amethyst.rs/")
    (synopsis "GLTF asset loading")
    (description "This package provides GLTF asset loading for Amethyst.")
    (license (list license:expat license:asl2.0))))

(define-public rust-amethyst-input-0.6
  (package
    (name "rust-amethyst-input")
    (version "0.6.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "amethyst-input" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1wwz5qcxypxa2y65yizajvmh4csfnmah120pgip086jkzfgls7h6"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-amethyst-config" ,rust-amethyst-config-0.9)
                       ("rust-amethyst-core" ,rust-amethyst-core-0.5)
                       ("rust-amethyst-renderer" ,rust-amethyst-renderer-0.10)
                       ("rust-derivative" ,rust-derivative-1)
                       ("rust-fnv" ,rust-fnv-1)
                       ("rust-sdl2" ,rust-sdl2-0.31)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-smallvec" ,rust-smallvec-0.6)
                       ("rust-thread-profiler" ,rust-thread-profiler-0.3)
                       ("rust-winit" ,rust-winit-0.18))))
    (home-page "https://amethyst.rs/")
    (synopsis "Input rebinding ")
    (description "This package provides input rebinding for Amethyst.")
    (license (list license:expat license:asl2.0))))

(define-public rust-amethyst-locale-0.4
  (package
    (name "rust-amethyst-locale")
    (version "0.4.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "amethyst-locale" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0yv2zxnhgzlni9dv93gwys8n5zrygffmh1q1vfzf8mxc58rkr1jh"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-amethyst-assets" ,rust-amethyst-assets-0.6)
                       ("rust-amethyst-core" ,rust-amethyst-core-0.5)
                       ("rust-fluent" ,rust-fluent-0.4)
                       ("rust-thread-profiler" ,rust-thread-profiler-0.3))))
    (home-page "https://amethyst.rs/")
    (synopsis "Localisation bindings")
    (description "This package provides localisation bindings for Amethyst.")
    (license (list license:expat license:asl2.0))))

(define-public rust-amethyst-network-0.3
  (package
    (name "rust-amethyst-network")
    (version "0.3.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "amethyst-network" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "11zp0p02ipnnjjx8ni5lv28rpwjw9xbdwl9ibsy8ihw2vy09q2rw"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-amethyst-core" ,rust-amethyst-core-0.5)
                       ("rust-bincode" ,rust-bincode-1)
                       ("rust-laminar" ,rust-laminar-0.1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-shred" ,rust-shred-0.7)
                       ("rust-shrev" ,rust-shrev-1)
                       ("rust-thread-profiler" ,rust-thread-profiler-0.3)
                       ("rust-uuid" ,rust-uuid-0.7))))
    (home-page "https://amethyst.rs/")
    (synopsis "Amethyst networking crate")
    (description "This package provides the Amethyst networking crate.")
    (license (list license:expat license:asl2.0))))

(define-public rust-amethyst-renderer-0.10
  (package
    (name "rust-amethyst-renderer")
    (version "0.10.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "amethyst-renderer" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0vcxddx5xqf92pwmjmhg1j5ir4x325jf5m72vpsd3wv7fydpicld"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-amethyst-assets" ,rust-amethyst-assets-0.6)
                       ("rust-amethyst-core" ,rust-amethyst-core-0.5)
                       ("rust-amethyst-derive" ,rust-amethyst-derive-0.3)
                       ("rust-derivative" ,rust-derivative-1)
                       ("rust-error-chain" ,rust-error-chain-0.12)
                       ("rust-fnv" ,rust-fnv-1)
                       ("rust-genmesh" ,rust-genmesh-0.6)
                       ("rust-gfx" ,rust-gfx-0.17)
                       ("rust-gfx-core" ,rust-gfx-core-0.8)
                       ("rust-gfx-device-dx11" ,rust-gfx-device-dx11-0.7)
                       ("rust-gfx-device-gl" ,rust-gfx-device-gl-0.15)
                       ("rust-gfx-device-metal" ,rust-gfx-device-metal-0.3)
                       ("rust-gfx-device-vulkan" ,rust-gfx-device-vulkan-0.1)
                       ("rust-gfx-macros" ,rust-gfx-macros-0.2)
                       ("rust-gfx-window-dxgi" ,rust-gfx-window-dxgi-0.17)
                       ("rust-gfx-window-glutin" ,rust-gfx-window-glutin-0.27)
                       ("rust-gfx-window-metal" ,rust-gfx-window-metal-0.6)
                       ("rust-gfx-window-vulkan" ,rust-gfx-window-vulkan-0.1)
                       ("rust-glsl-layout" ,rust-glsl-layout-0.1)
                       ("rust-glutin" ,rust-glutin-0.19)
                       ("rust-hetseq" ,rust-hetseq-0.2)
                       ("rust-hibitset" ,rust-hibitset-0.5)
                       ("rust-image" ,rust-image-0.20)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-rayon" ,rust-rayon-1)
                       ("rust-ron" ,rust-ron-0.4)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-shred" ,rust-shred-0.7)
                       ("rust-shred-derive" ,rust-shred-derive-0.5)
                       ("rust-smallvec" ,rust-smallvec-0.6)
                       ("rust-thread-profiler" ,rust-thread-profiler-0.3)
                       ("rust-wavefront-obj" ,rust-wavefront-obj-5)
                       ("rust-winit" ,rust-winit-0.18))))
    (home-page "https://www.amethyst.rs/")
    (synopsis "High-level rendering engine with multiple backends")
    (description
     "This package provides the High-level rendering engine with multiple
backends for Amethyst.")
    (license (list license:expat license:asl2.0))))

(define-public rust-amethyst-ui-0.5
  (package
    (name "rust-amethyst-ui")
    (version "0.5.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "amethyst-ui" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1qlhpmp9c6c5js5cb73kj5m7kwyfxrqqs20hna7323q1ga63zf2q"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-amethyst-assets" ,rust-amethyst-assets-0.6)
                       ("rust-amethyst-audio" ,rust-amethyst-audio-0.5)
                       ("rust-amethyst-core" ,rust-amethyst-core-0.5)
                       ("rust-amethyst-input" ,rust-amethyst-input-0.6)
                       ("rust-amethyst-renderer" ,rust-amethyst-renderer-0.10)
                       ("rust-clipboard" ,rust-clipboard-0.5)
                       ("rust-derivative" ,rust-derivative-1)
                       ("rust-fnv" ,rust-fnv-1)
                       ("rust-font-kit" ,rust-font-kit-0.1)
                       ("rust-gfx" ,rust-gfx-0.17)
                       ("rust-gfx-glyph" ,rust-gfx-glyph-0.13)
                       ("rust-glsl-layout" ,rust-glsl-layout-0.1)
                       ("rust-hibitset" ,rust-hibitset-0.5)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-ron" ,rust-ron-0.4)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-shred" ,rust-shred-0.7)
                       ("rust-shred-derive" ,rust-shred-derive-0.5)
                       ("rust-thread-profiler" ,rust-thread-profiler-0.3)
                       ("rust-unicode-normalization" ,rust-unicode-normalization-0.1)
                       ("rust-unicode-segmentation" ,rust-unicode-segmentation-1)
                       ("rust-winit" ,rust-winit-0.18))))
    (native-inputs (list cmake))
    (home-page "https://amethyst.rs/")
    (synopsis "Amethyst UI crate")
    (description "This package provides the Amethyst UI crate.")
    (license (list license:expat license:asl2.0))))

(define-public rust-amethyst-utils-0.5
  (package
    (name "rust-amethyst-utils")
    (version "0.5.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "amethyst-utils" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0pdvwwsn6p8v8vb57azg5bc3q061s0g7z5z5nvyly45gmdvc7aq6"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-amethyst-assets" ,rust-amethyst-assets-0.6)
                       ("rust-amethyst-controls" ,rust-amethyst-controls-0.4)
                       ("rust-amethyst-core" ,rust-amethyst-core-0.5)
                       ("rust-amethyst-derive" ,rust-amethyst-derive-0.3)
                       ("rust-amethyst-renderer" ,rust-amethyst-renderer-0.10)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-shred" ,rust-shred-0.7)
                       ("rust-shred-derive" ,rust-shred-derive-0.5)
                       ("rust-specs-hierarchy" ,rust-specs-hierarchy-0.3)
                       ("rust-thread-profiler" ,rust-thread-profiler-0.3))))
    (home-page "https://amethyst.rs/")
    (synopsis "Amethyst utils")
    (description "Amethyst utils")
    (license (list license:expat license:asl2.0))))

(define-public rust-ash-0.37
  (package
    (name "rust-ash")
    (version "0.37.3+1.3.251")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ash" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0jndbsi5c8xifh4fdp378xpbyzdhs7y38hmbhih0lsv8bn1w7s9r"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;Tests are deprecated
       #:cargo-inputs (("rust-libloading" ,rust-libloading-0.7))))
    (home-page "https://github.com/MaikKlein/ash")
    (synopsis "Vulkan bindings for Rust")
    (description "This package provides the Vulkan bindings for Rust.")
    (license (list license:expat license:asl2.0))))

(define-public rust-ash-0.32
  (package
    (inherit rust-ash-0.37)
    (name "rust-ash")
    (version "0.32.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ash" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "04l1si7pgrradyjbrri2wmzvfj3irvsfhx6v65377lkp5803l1h6"))))))

(define-public rust-ash-window-0.12
  (package
    (name "rust-ash-window")
    (version "0.12.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ash-window" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1armbqzr0x905yypvh9ywgjj91kn93y5mxd6gkwaiwr9gid2h4mr"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-ash" ,rust-ash-0.37)
                       ("rust-raw-window-handle" ,rust-raw-window-handle-0.5)
                       ("rust-raw-window-metal" ,rust-raw-window-metal-0.3))
       #:cargo-development-inputs (("rust-ash" ,rust-ash-0.37)
                                   ("rust-winit" ,rust-winit-0.27))))
    (home-page "https://github.com/MaikKlein/ash")
    (synopsis "Interop library between ash and raw-window-handle")
    (description "This package proivdes an Interop library between ash and
raw-window-handle.")
    (license (list license:expat license:asl2.0))))

(define-public rust-ash-window-0.6
  (package
    (inherit rust-ash-window-0.12)
    (name "rust-ash-window")
    (version "0.6.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ash-window" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "18zzcsv9adcf5ky07hvisybvmsm67rv4wp32kdp0qhd8lcv8819d"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-ash" ,rust-ash-0.32)
                       ("rust-raw-window-handle" ,rust-raw-window-handle-0.3)
                       ("rust-raw-window-metal" ,rust-raw-window-metal-0.1))
       #:cargo-development-inputs (("rust-winit" ,rust-winit-0.19))))))

(define-public rust-basis-universal-0.1
  (package
    (name "rust-basis-universal")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "basis-universal" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0kisirafgfrjadylvabizpjmvcvxa58vyafwcl3vb8x1z2va9r6h"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-basis-universal-sys" ,rust-basis-universal-sys-0.1)
                       ("rust-bitflags" ,rust-bitflags-1)
                       ("rust-lazy-static" ,rust-lazy-static-1))
       #:cargo-development-inputs (("rust-image" ,rust-image-0.23)
                                   ("rust-lz4" ,rust-lz4-1))))
    (home-page "https://github.com/aclysma/basis-universal-rs")
    (synopsis
     "Bindings for the basis-universal Supercompressed GPU Texture Codec by
Binomial")
    (description
     "Bindings for Binomial LLC's @code{basis-universal} Supercompressed GPU
Texture Codec.")
    (license (list license:expat license:asl2.0))))

(define-public rust-basis-universal-sys-0.1
  (package
    (inherit rust-basis-universal-0.1)
    (name "rust-basis-universal-sys")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "basis-universal-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0rnmcg4pzxacj4s598lpg9nf7750an3rjnzj3flibqwh0gdhgqky"))))
    (arguments
     `(#:cargo-inputs (("rust-cc" ,rust-cc-1))))))

(define-public rust-core-graphics-0.13
  (package
    (inherit rust-core-graphics-0.22)
    (name "rust-core-graphics")
    (version "0.13.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "core-graphics" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1qysmxpg95pksd6rn34hsyn7pbgygm0akyrqc91apy9jvigx83pv"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-core-foundation" ,rust-core-foundation-0.5)
                       ("rust-foreign-types" ,rust-foreign-types-0.3)
                       ("rust-libc" ,rust-libc-0.2))))))

(define-public rust-core-graphics-0.8
  (package
    (inherit rust-core-graphics-0.22)
    (name "rust-core-graphics")
    (version "0.8.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "core-graphics" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "08gwdsyc5akhsd7m69nm15mkrbiz6f88v2i1q703ggrbi2adi5wp"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.9)
                       ("rust-core-foundation" ,rust-core-foundation-0.3)
                       ("rust-libc" ,rust-libc-0.2))))))

(define-public rust-core-graphics-0.5
  (package
    (inherit rust-core-graphics-0.22)
    (name "rust-core-graphics")
    (version "0.5.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "core-graphics" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0r0szvdazkx6p79aj72m5hya3rzx99jliz3hnvpbzg1b1c7lvncb"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-core-foundation" ,rust-core-foundation-0.2)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-serde" ,rust-serde-0.8))))))

(define-public rust-core-graphics-0.4
  (package
    (inherit rust-core-graphics-0.22)
    (name "rust-core-graphics")
    (version "0.4.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "core-graphics" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1n0mqki2sp4p02hzb8051j66g9izv0a0qaw8xnwai3va21ms704w"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-core-foundation" ,rust-core-foundation-0.2)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-serde" ,rust-serde-0.8))))))

(define-public rust-cgl-0.1
  (package
    (inherit rust-cgl-0.3)
    (name "rust-cgl")
    (version "0.1.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cgl" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "044cycnn760gki4jnvsagwr3wds9pdmnpgsx8ysrqwsslv67ipcb"))))
    (arguments
     `(#:skip-build? #t ;macOS only!
       #:cargo-inputs (("rust-gleam" ,rust-gleam-0.2)
                       ("rust-libc" ,rust-libc-0.2))))))

(define-public rust-cgmath-0.15
  (package
    (inherit rust-cgmath-0.18)
    (name "rust-cgmath")
    (version "0.15.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cgmath" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1rd2kyqzr604dbydwlgxphb5jyva81lckv22xiqziayglw12qdyj"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-approx" ,rust-approx-0.1)
                       ("rust-mint" ,rust-mint-0.4)
                       ("rust-num-traits" ,rust-num-traits-0.1)
                       ("rust-rand" ,rust-rand-0.3)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-simd" ,rust-simd-0.2))
       #:cargo-development-inputs (("rust-glium" ,rust-glium-0.16)
                                   ("rust-serde-json" ,rust-serde-json-1))))))

(define-public rust-cgmath-0.14
  (package
    (inherit rust-cgmath-0.18)
    (name "rust-cgmath")
    (version "0.14.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cgmath" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1020iimy1zynyjvixkjy27b9qpvdjw1pjy2w054hvwykgahjbw47"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-approx" ,rust-approx-0.1)
                       ("rust-num-traits" ,rust-num-traits-0.1)
                       ("rust-rand" ,rust-rand-0.3)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-derive" ,rust-serde-derive-1)
                       ("rust-simd" ,rust-simd-0.2))
       #:cargo-development-inputs (("rust-glium" ,rust-glium-0.16)
                                   ("rust-serde-json" ,rust-serde-json-1))))))

(define-public rust-cgmath-0.9
  (package
    (inherit rust-cgmath-0.18)
    (name "rust-cgmath")
    (version "0.9.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cgmath" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1l33l5wzqx46v0h2hp2rddr6fk8sxfrz0d22gf8llxa36ypgpkji"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-num" ,rust-num-0.1)
                       ("rust-rand" ,rust-rand-0.3)
                       ("rust-rustc-serialize" ,rust-rustc-serialize-0.3))
       #:cargo-development-inputs (("rust-glium" ,rust-glium-0.14))))))

(define-public rust-cgmath-0.7
  (package
    (inherit rust-cgmath-0.18)
    (name "rust-cgmath")
    (version "0.7.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cgmath" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1xmrjrxi584y4hzq8714c2ckkh96c16gb7x68swzyx3fnqz3psbm"))))
    (arguments
     `(#:cargo-inputs (("rust-num" ,rust-num-0.1)
                       ("rust-rand" ,rust-rand-0.3)
                       ("rust-rustc-serialize" ,rust-rustc-serialize-0.3))))))

(define-public rust-crevice-0.14
  (package
    (name "rust-crevice")
    (version "0.14.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "crevice" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0ajhl877x3s7yq3vl80z64yyx0pnpi95nqbzhjsmv239i4xpg7fv"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-cgmath" ,rust-cgmath-0.18)
                       ("rust-crevice-derive" ,rust-crevice-derive-0.10)
                       ("rust-glam" ,rust-glam-0.24)
                       ("rust-nalgebra" ,rust-nalgebra-0.32))
       #:cargo-development-inputs (("rust-insta" ,rust-insta-1))))
    (home-page "https://github.com/LPGhatguy/crevice")
    (synopsis
     "Rust crate to generate GLSL structs with explicitly-initialized padding bytes")
    (description
     "Crevice creates GLSL-compatible versions of types through the power of
derive macros. Generated structures provide an as_bytes method to
allow safely packing data into buffers for uploading.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-crevice-derive-0.10
  (package
    (name "rust-crevice-derive")
    (version "0.10.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "crevice-derive" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0vmjb14w7dawjh6fdggiafajv9193jlr1plss0miwj9yxdyc0813"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/LPGhatguy/crevice")
    (synopsis "Derive crate for the 'crevice' crate")
    (description
     "This package provides custom derive support for the 'crevice' crate.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-cuda-std-0.2
  (package
    (name "rust-cuda-std")
    (version "0.2.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cuda_std" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "174237dj152dvndvykcn17nz2d0kdzsyyxnb6fsdz3i7xa8lfcgn"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;Failed doc tests due to missing attribute 'kernel'
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-cuda-std-macros" ,rust-cuda-std-macros-0.2)
                       ("rust-half" ,rust-half-1)
                       ("rust-paste" ,rust-paste-1) ;rust-paste-1.0.5 over upstream
                       ("rust-vek" ,rust-vek-0.15))))
    (home-page "https://github.com/Rust-GPU/Rust-CUDA")
    (synopsis "Standard library for CUDA with rustc_codegen_nvvm")
    (description
     "An ecosystem of libraries and tools for writing and executing
extremely fast GPU code fully in Rust.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-cuda-std-macros-0.2
  (package
    (name "rust-cuda-std-macros")
    (version "0.2.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cuda_std_macros" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0hlakxn9pz8233pwsh44j8gzqzf078a3lnnq3v2cadmb4c4l1mlz"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/Rust-GPU/Rust-CUDA")
    (synopsis "Macros for cuda_std")
    (description "This crate provides macros for the cuda_std crate.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-cust-0.3
  (package
    (name "rust-cust")
    (version "0.3.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cust" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0sflxlz54a44h725q25b9zv0pl2vnhh42wxrhcjg2yg124cwfv0d"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1.2)
                       ("rust-cust-core" ,rust-cust-core-0.1)
                       ("rust-cust-derive" ,rust-cust-derive-0.2)
                       ("rust-find-cuda-helper" ,rust-find-cuda-helper-0.2)
                       ("rust-glam" ,rust-glam-0.20))))
    (home-page "https://github.com/Rust-GPU/Rust-CUDA")
    (synopsis "High level bindings to the CUDA Driver API")
    (description
     "This package provides high level bindings to the CUDA Driver API for
Rust-CUDA.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-cust-0.2
  (package
    (inherit rust-cust-0.3)
    (name "rust-cust")
    (version "0.2.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cust" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1jb7l9qq65jc9rqwwygaxg4b304nn3i8igic905cbbrnw1ns8iz9"))))))

(define-public rust-cust-core-0.1
  (package
    (name "rust-cust-core")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cust_core" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "01jzjwywsngqm8d1vxk3zr9klvidab6iis1myg5r1y5q5ik7k7q3"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;fails doc tests due to undeclared 'cust' module
       #:cargo-inputs (("rust-cust-derive" ,rust-cust-derive-0.2)
                       ("rust-glam" ,rust-glam-0.20)
                       ("rust-half" ,rust-half-1.8)
                       ("rust-mint" ,rust-mint-0.5)
                       ("rust-num-complex" ,rust-num-complex-0.4)
                       ("rust-vek" ,rust-vek-0.15))))
    (home-page "https://github.com/Rust-GPU/Rust-CUDA")
    (synopsis "Core library for cust that can be shared across CPU and GPU")
    (description
     "An ecosystem of libraries and tools for writing and executing extremely fast GPU code fully in Rust.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-cust-derive-0.2
  (package
    (name "rust-cust-derive")
    (version "0.2.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cust_derive" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0rigqx5d1941cbpbd76i455ifh4yzz6fcga2na9fv6k2zsavr8z8"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/Rust-GPU/Rust-CUDA")
    (synopsis "Derive macros for cust_core")
    (description "Derive macros for cust_core.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-cust-derive-0.1
  (package
    (name "rust-cust-derive")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cust_derive" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1ckxjfdlwhmdyf5s2v10cslpb6wri9xl8nk3qirz8rsn5x1hn61v"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/Rust-GPU/Rust-CUDA")
    (synopsis "Derive macros for cust_core")
    (description "Derive macros for cust_core.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-cust-raw-0.11
  (package
    (name "rust-cust-raw")
    (version "0.11.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cust_raw" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1y1b82gf0fmaqxhvzjd3cxgd54vvbj3vji68pcl9ijqjvrm0vx7v"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t ;Requires find_cuda_helper.
       #:cargo-inputs (("rust-find-cuda-helper" ,rust-find-cuda-helper-0.2))))
    (home-page "https://github.com/Rust-GPU/Rust-CUDA")
    (synopsis "Low level bindings to the CUDA Driver API")
    (description "Low level bindings to the CUDA Driver API for Rust-CUDA.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-d3d12-0.6
  (package
    (name "rust-d3d12")
    (version "0.6.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "d3d12" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1nirigg48lvilgbwgbk89xrf2k1ak60wgqy0xslx8ywfb8pxxw6q"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t ;Only for windows
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-libloading" ,rust-libloading-0.7)
                       ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/gfx-rs/d3d12-rs")
    (synopsis "Low level D3D12 API wrapper")
    (description "This package provides a low level D3D12 API wrapper.")
    (license (list license:expat license:asl2.0))))

(define-public rust-ddsfile-0.5
  (package
    (name "rust-ddsfile")
    (version "0.5.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ddsfile" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "11rf0j02xp7nc8wpq74m1q1zxhazjxbc794dvrfxnh1ggjbcskjr"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-byteorder" ,rust-byteorder-1)
                       ("rust-enum-primitive" ,rust-enum-primitive-0.1))))
    (home-page "https://github.com/SiegeEngine/ddsfile")
    (synopsis "DirectDraw Surface file format parser/composer")
    (description
     "This package provides the @code{DirectDraw} Surface file format
parser/composer.")
    (license license:expat)))

(define-public rust-distill-0.0
  (package
    (name "rust-distill")
    (version "0.0.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "distill" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "15y2q4w4ryanbysr7l98jcgc99f1m5sn2h01sxsn283zgsac7n4n"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-distill-core" ,rust-distill-core-0.0)
                       ("rust-distill-daemon" ,rust-distill-daemon-0.0)
                       ("rust-distill-importer" ,rust-distill-importer-0.0)
                       ("rust-distill-loader" ,rust-distill-loader-0.0))
       #:cargo-development-inputs (("rust-futures" ,rust-futures-0.3)
                                   ("rust-serde" ,rust-serde-1)
                                   ("rust-serial-test" ,rust-serial-test-0.5)
                                   ("rust-tokio" ,rust-tokio-1)
                                   ("rust-uuid" ,rust-uuid-0.8))))
    (home-page "https://github.com/amethyst/distill")
    (synopsis "Asset framework for game engines & editor suites")
    (description
     "This package provides an asset framework for game engines & editor
suites.")
    (license (list license:expat license:asl2.0))))

(define-public rust-distill-core-0.0
  (package
    (name "rust-distill-core")
    (version "0.0.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "distill-core" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1vkds01w3lk0mxckr07rhh037kiw4p08c1kzv2pk8i7ragi2hcp4"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-dunce" ,rust-dunce-1)
                       ("rust-futures-core" ,rust-futures-core-0.3)
                       ("rust-path-clean" ,rust-path-clean-0.1)
                       ("rust-path-slash" ,rust-path-slash-0.1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-type-uuid" ,rust-type-uuid-0.1)
                       ("rust-uuid" ,rust-uuid-0.8))
       #:cargo-development-inputs (("rust-bincode" ,rust-bincode-1)
                                   ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/amethyst/distill")
    (synopsis "Core types and utils for @code{distill}")
    (description
     "This package provides core types and utils for @code{distill}.")
    (license (list license:expat license:asl2.0))))

(define-public rust-distill-daemon-0.0
  (package
    (name "rust-distill-daemon")
    (version "0.0.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "distill-daemon" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1kdblm4yvw9k974ww1ibhysyb8cidd3yxh12449vzzyw4g1bla14"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-async-channel" ,rust-async-channel-1)
                       ("rust-async-lock" ,rust-async-lock-2)
                       ("rust-bincode" ,rust-bincode-1)
                       ("rust-capnp" ,rust-capnp-0.14)
                       ("rust-capnp-rpc" ,rust-capnp-rpc-0.14)
                       ("rust-chrono" ,rust-chrono-0.4)
                       ("rust-distill-core" ,rust-distill-core-0.0)
                       ("rust-distill-downstream-lmdb-rkv" ,rust-distill-downstream-lmdb-rkv-0.11)
                       ("rust-distill-importer" ,rust-distill-importer-0.0)
                       ("rust-distill-loader" ,rust-distill-loader-0.0)
                       ("rust-distill-schema" ,rust-distill-schema-0.0)
                       ("rust-dunce" ,rust-dunce-1)
                       ("rust-erased-serde" ,rust-erased-serde-0.3)
                       ("rust-event-listener" ,rust-event-listener-2)
                       ("rust-fern" ,rust-fern-0.6)
                       ("rust-futures" ,rust-futures-0.3)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-notify" ,rust-notify-4)
                       ("rust-num-cpus" ,rust-num-cpus-1)
                       ("rust-path-clean" ,rust-path-clean-0.1)
                       ("rust-path-slash" ,rust-path-slash-0.1)
                       ("rust-pin-project" ,rust-pin-project-1)
                       ("rust-rayon" ,rust-rayon-1)
                       ("rust-ron" ,rust-ron-0.6)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-derive" ,rust-serde-derive-1)
                       ("rust-tokio" ,rust-tokio-1)
                       ("rust-tokio-util" ,rust-tokio-util-0.6)
                       ("rust-uuid" ,rust-uuid-0.8))
       #:cargo-development-inputs (("rust-tempfile" ,rust-tempfile-3))))
    (home-page "https://github.com/amethyst/distill")
    (synopsis "Daemon component of the asset pipeline `distill`")
    (description
     "This package provides the daemon component of the asset pipeline
`distill`.")
    (license (list license:expat license:asl2.0))))

(define-public rust-distill-downstream-lmdb-rkv-0.11
  (package
    (name "rust-distill-downstream-lmdb-rkv")
    (version "0.11.0-windows-fix")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "distill-downstream-lmdb-rkv" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1j8aqdkh26vzq3ldk2zms9r0k4azrmcrzpymfcil8gxpfcfd48gm"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-distill-downstream-lmdb-sys" ,rust-distill-downstream-lmdb-sys-0.8)
                       ("rust-libc" ,rust-libc-0.2))
       #:cargo-development-inputs (("rust-byteorder" ,rust-byteorder-1)
                                   ("rust-rand" ,rust-rand-0.4)
                                   ("rust-tempdir" ,rust-tempdir-0.3))))
    (home-page "https://github.com/mozilla/lmdb-rs.git")
    (synopsis "Idiomatic and safe LMDB wrapper")
    (description "This package provides an idiomatic and safe LMDB wrapper.")
    (license license:asl2.0)))

(define-public rust-distill-downstream-lmdb-sys-0.8
  (package
    (name "rust-distill-downstream-lmdb-sys")
    (version "0.8.0-windows-fix")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "distill-downstream-lmdb-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "10vcysl9dkgd8nm26mf2wxjvlzjfk6mhzymvfwzlw26x9pp1cs8m"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/danburkert/lmdb-rs.git")
    (synopsis "Rust bindings for liblmdb")
    (description "This package provides Rust bindings for liblmdb.")
    (license license:asl2.0)))

(define-public rust-distill-importer-0.0
  (package
    (name "rust-distill-importer")
    (version "0.0.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "distill-importer" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "055n3jawwg22188mmh1mbrx2mm6d75s09r3lbmqz0rwsm6xjmb0y"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-distill-core" ,rust-distill-core-0.0)
                       ("rust-distill-serde-importable-derive" ,rust-distill-serde-importable-derive-0.0)
                       ("rust-erased-serde" ,rust-erased-serde-0.3)
                       ("rust-futures" ,rust-futures-0.3)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-ron" ,rust-ron-0.6)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-typetag" ,rust-typetag-0.1)
                       ("rust-uuid" ,rust-uuid-0.8))))
    (home-page "https://github.com/amethyst/distill")
    (synopsis "Importer component of the asset pipeline @code{distill}")
    (description
     "This is the importer component of the asset pipeline @code{distill}.")
    (license (list license:expat license:asl2.0))))

(define-public rust-distill-loader-0.0
  (package
    (name "rust-distill-loader")
    (version "0.0.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "distill-loader" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0bsi6n1afkjbjb1q83h52gpqd4b2fzv27mzz7zhljr7ld91sj2gc"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-capnp" ,rust-capnp-0.14)
                       ("rust-capnp-rpc" ,rust-capnp-rpc-0.14)
                       ("rust-crossbeam-channel" ,rust-crossbeam-channel-0.5)
                       ("rust-dashmap" ,rust-dashmap-4)
                       ("rust-distill-core" ,rust-distill-core-0.0)
                       ("rust-distill-schema" ,rust-distill-schema-0.0)
                       ("rust-futures-channel" ,rust-futures-channel-0.3)
                       ("rust-futures-core" ,rust-futures-core-0.3)
                       ("rust-futures-util" ,rust-futures-util-0.3)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-memmap" ,rust-memmap-0.7)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-thread-local" ,rust-thread-local-1)
                       ("rust-tokio" ,rust-tokio-1)
                       ("rust-tokio-util" ,rust-tokio-util-0.6)
                       ("rust-uuid" ,rust-uuid-0.8))))
    (home-page "https://github.com/amethyst/distill")
    (synopsis "Loader component of the asset pipeline `distill`")
    (description
     "This package provides the loader component of the asset pipeline `distill`.")
    (license (list license:expat license:asl2.0))))

(define-public rust-distill-schema-0.0
  (package
    (name "rust-distill-schema")
    (version "0.0.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "distill-schema" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0q86i38qshx282jp1mdwxjh6jwgz2fxp5hlh33kr7bjb6j884m26"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-capnp" ,rust-capnp-0.14)
                       ("rust-distill-core" ,rust-distill-core-0.0))))
    (home-page "https://github.com/amethyst/distill")
    (synopsis "RPC schema definitions for the asset pipeline `distill`")
    (description
     "This package provides RPC schema definitions for the asset pipeline
`distill`.")
    (license (list license:expat license:asl2.0))))

(define-public rust-distill-serde-importable-derive-0.0
  (package
    (name "rust-distill-serde-importable-derive")
    (version "0.0.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "distill-serde-importable-derive" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1b36bl1jhikjnnqf453wh3r2ncpk5k7sz90gwfwzfjpl8ibkgj6j"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/amethyst/distill")
    (synopsis "Proc macro for SerdeImportables in `distill`")
    (description
     "This package provides the Proc macro for @code{SerdeImportables} in
`distill`.")
    (license (list license:expat license:asl2.0))))

(define-public rust-draw-state-0.6
  (package
    (inherit rust-draw-state-0.8)
    (name "rust-draw-state")
    (version "0.6.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "draw-state" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "11c1ivczgxmivrbx451jg6hnc4nhganrvl6md17wh7kwigdgr5hm"))))
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.6))))))

(define-public rust-euclid-0.19
  (package
    (inherit rust-euclid-0.22)
    (name "rust-euclid")
    (version "0.19.9")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "euclid" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1qah8h2z3dj2gb325f2qqcm30p4w7b8qsbadz6jyfxwl3di9jssr"))))
    (arguments
     `(#:cargo-inputs (("rust-euclid-macros" ,rust-euclid-macros-0.1)
                       ("rust-mint" ,rust-mint-0.5)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-rand" ,rust-rand-0.4)
                                   ("rust-serde-test" ,rust-serde-test-1))))))

(define-public rust-euclid-macros-0.1
  (package
    (name "rust-euclid-macros")
    (version "0.1.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "euclid-macros" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "05dy60mxw2yg26m1ssqd5v7an0wly97rn0r3b8f7l0x5iv0q9jzx"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-0.4)
                       ("rust-quote" ,rust-quote-0.6)
                       ("rust-syn" ,rust-syn-0.15))))
    (home-page "https://github.com/servo/euclid")
    (synopsis "Euclid implementation detail")
    (description "This package provides Euclid macros")
    (license (list license:expat license:asl2.0))))

(define-public rust-fast-image-resize-2
  (package
    (name "rust-fast-image-resize")
    (version "2.7.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "fast-image-resize" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0nnm59h7dl2bpi5k2wcd7zz14nl00sa33jiipbjbn48f0i09ly6c"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-thiserror" ,rust-thiserror-1))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.4)
                                   ("rust-image" ,rust-image-0.24)
                                   ("rust-itertools" ,rust-itertools-0.10)
                                   ("rust-nix" ,rust-nix-0.26)
                                   ("rust-png" ,rust-png-0.17)
                                   ("rust-resize" ,rust-resize-0.7)
                                   ("rust-rgb" ,rust-rgb-0.8)
                                   ("rust-serde" ,rust-serde-1)
                                   ("rust-serde-json" ,rust-serde-json-1)
                                   ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "https://github.com/cykooz/fast_image_resize")
    (synopsis
     "Library for fast image resizing with using of SIMD instructions")
    (description
     "This package provides a library for fast image resizing with using of
SIMD instructions.")
    (license (list license:expat license:asl2.0))))

(define-public rust-find-cuda-helper-0.2
  (package
    (name "rust-find-cuda-helper")
    (version "0.2.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "find_cuda_helper" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1bdxg8bmcqvnxb43y6bn2xnhszyi9bm0kndagp3iml1xb5ffdygr"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t ;Circular dependency on cust_core v0.2 on build
       #:cargo-inputs (("rust-glob" ,rust-glob-0.3)
                       ("rust-bitflags" ,rust-bitflags-1.2)
                       ("rust-cust-derive" ,rust-cust-derive-0.1)
                       ("rust-cust-raw" ,rust-cust-raw-0.11)
                       ("rust-cust" ,rust-cust-0.2)
                       ("rust-vek" ,rust-vek-0.15))))
    (home-page "https://github.com/Rust-GPU/Rust-CUDA")
    (synopsis "Helper crate for searching for CUDA libraries")
    (description "Helper crate for searching for CUDA libraries.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-genmesh-0.4
  (package
    (inherit rust-genmesh-0.6)
    (name "rust-genmesh")
    (version "0.4.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "genmesh" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "06yvllg11qa58jbfq7wrplmdh4lv4ji5y8l6m0q09fq7w0cacx54"))))
    (arguments
     `(#:cargo-inputs (("rust-cgmath" ,rust-cgmath-0.14))))))

(define-public rust-gfx-0.17
  (package
    (inherit rust-gfx-0.18)
    (name "rust-gfx")
    (version "0.17.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gfx" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0d28vs7fzj6dk16vq7s2632681zak2qgslrllx156927yz0y0z3x"))))
    (arguments
     `(#:cargo-inputs (("rust-derivative" ,rust-derivative-1)
                       ("rust-draw-state" ,rust-draw-state-0.8)
                       ("rust-gfx-core" ,rust-gfx-core-0.8)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-mint" ,rust-mint-0.5))))))

(define-public rust-gfx-core-0.8
  (package
    (inherit rust-gfx-core-0.9)
    (name "rust-gfx-core")
    (version "0.8.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gfx-core" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1bih32skldwlp8mq2x0263g7sw0k2m3lf6nsqdb0kwk1gs1k4jf7"))))
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-derivative" ,rust-derivative-1)
                       ("rust-draw-state" ,rust-draw-state-0.8)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-mint" ,rust-mint-0.5)
                       ("rust-serde" ,rust-serde-1))))))

(define-public rust-gfx-core-0.6
  (package
    (inherit rust-gfx-core-0.9)
    (name "rust-gfx-core")
    (version "0.6.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gfx-core" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "10ysd2wf95a36zvxvpl8k49jvm9v6m6l87n4af40imkw7zvp24kh"))))
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.7)
                       ("rust-draw-state" ,rust-draw-state-0.6)
                       ("rust-log" ,rust-log-0.3))))))

(define-public rust-gfx-device-dx11-0.7
  (package
    (name "rust-gfx-device-dx11")
    (version "0.7.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gfx-device-dx11" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1zw3ccir9xyi4wsgkakb1d53mdibgyy08plw8sxflzblqlnf465r"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-gfx-core" ,rust-gfx-core-0.8)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/gfx-rs/gfx")
    (synopsis "DirectX-11 backend for gfx-rs")
    (description
     "This package provides the @code{DirectX-11} backend for gfx-rs.")
    (license license:asl2.0)))

(define-public rust-gfx-device-gl-0.16
  (package
    (name "rust-gfx-device-gl")
    (version "0.16.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gfx_device_gl" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1g5yg19jvxdmviljyakhd6253bnb2qg7v8iscf48ihc0ldgki70h"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-gfx-core" ,rust-gfx-core-0.9)
                       ("rust-gfx-gl" ,rust-gfx-gl-0.6)
                       ("rust-log" ,rust-log-0.4))))
    (home-page "https://github.com/gfx-rs/gfx")
    (synopsis "OpenGL backend for gfx-rs")
    (description "This package provides the OpenGL backend for gfx.")
    (license license:asl2.0)))

(define-public rust-gfx-device-gl-0.15
  (package
    (inherit rust-gfx-device-gl-0.16)
    (name "rust-gfx-device-gl")
    (version "0.15.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gfx-device-gl" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "10m9h603978cw131vylz649xg9rz4rnb3vgm3rx1iqwsmdvcryfy"))))
    (arguments
     `(#:cargo-inputs (("rust-gfx-core" ,rust-gfx-core-0.8)
                       ("rust-gfx-gl" ,rust-gfx-gl-0.5)
                       ("rust-log" ,rust-log-0.4))))))

(define-public rust-gfx-device-metal-0.3
  (package
    (name "rust-gfx-device-metal")
    (version "0.3.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gfx-device-metal" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "147nap58khr6r3n51yavfs8wk0mq38xazwgfd0mp6j2lsrqjqys0"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bit-set" ,rust-bit-set-0.4)
                       ("rust-cocoa" ,rust-cocoa-0.9)
                       ("rust-gfx-core" ,rust-gfx-core-0.8)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-metal-rs" ,rust-metal-rs-0.4)
                       ("rust-objc" ,rust-objc-0.2)
                       ("rust-objc-foundation" ,rust-objc-foundation-0.1))))
    (home-page "https://github.com/gfx-rs/gfx")
    (synopsis "Metal backend for gfx-rs")
    (description "This package provides the Metal backend for gfx-rs.")
    (license license:asl2.0)))

(define-public rust-gfx-device-vulkan-0.1
  (package
    (name "rust-gfx-device-vulkan")
    (version "0.1.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gfx-device-vulkan" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0kn4i9vffcyj0friynmj3bzb025f832ppsxzji7kzqx5s78k9qb4"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-gfx-core" ,rust-gfx-core-0.6)
                       ("rust-log" ,rust-log-0.3)
                       ("rust-shared-library" ,rust-shared-library-0.1)
                       ("rust-spirv-utils" ,rust-spirv-utils-0.2)
                       ("rust-vk" ,rust-vk-0.0)
                       ("rust-vk-sys" ,rust-vk-sys-0.2)
                       ("rust-winit" ,rust-winit-0.5))))
    (home-page "https://github.com/gfx-rs/gfx")
    (synopsis "Vulkan API backend for gfx-rs")
    (description "This package provides the Vulkan API backend for gfx-rs.")
    (license license:asl2.0)))

(define-public rust-gfx-gl-0.6
  (package
    (name "rust-gfx-gl")
    (version "0.6.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gfx_gl" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0ppzj4bgjawdqz3fvnscqk8lnmgh95pwzh0v96vwy809cxj83lzj"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-gl-generator" ,rust-gl-generator-0.14))))
    (home-page "https://github.com/gfx-rs/gfx_gl.git")
    (synopsis "OpenGL bindings for gfx, based on gl-rs")
    (description
     "This package provides OpenGL bindings for gfx, based on gl-rs.")
    (license license:asl2.0)))

(define-public rust-gfx-gl-0.5
  (package
    (inherit rust-gfx-gl-0.6)
    (name "rust-gfx-gl")
    (version "0.5.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gfx-gl" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1jzfgrmvb86vx3xmsxndsdpm0ngh5d82bpgrvnkja43ciw7r52iy"))))
    (arguments
     `(#:cargo-inputs (("rust-gl-generator" ,rust-gl-generator-0.9))))))

(define-public rust-gfx-window-glutin-0.28
  (package
    (name "rust-gfx-window-glutin")
    (version "0.28.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gfx-window-glutin" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0zyvfpbrgpg201690zhdv636bhp4f513m8pwc7fc1lm478578qlp"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-gfx-core" ,rust-gfx-core-0.8)
                       ("rust-gfx-device-gl" ,rust-gfx-device-gl-0.15)
                       ("rust-glutin" ,rust-glutin-0.19))))
    (home-page "https://github.com/gfx-rs/gfx")
    (synopsis "Glutin window for gfx-rs")
    (description "This package provides the Glutin window for gfx-rs.")
    (license license:asl2.0)))

(define-public rust-gfx-glyph-0.13
  (package
    (name "rust-gfx-glyph")
    (version "0.13.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gfx-glyph" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1qb15rv8ag27hwdl5c4y441vjra52gzavf3w62q19i7v24jbajzj"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-backtrace" ,rust-backtrace-0.3)
                       ("rust-gfx" ,rust-gfx-0.17)
                       ("rust-gfx-core" ,rust-gfx-core-0.8)
                       ("rust-glyph-brush" ,rust-glyph-brush-0.4)
                       ("rust-log" ,rust-log-0.4))
       #:cargo-development-inputs (("rust-approx" ,rust-approx-0.3)
                                   ("rust-cgmath" ,rust-cgmath-0.17)
                                   ("rust-env-logger" ,rust-env-logger-0.6)
                                   ("rust-gfx-device-gl" ,rust-gfx-device-gl-0.15)
                                   ("rust-gfx-window-glutin" ,rust-gfx-window-glutin-0.28)
                                   ("rust-glutin" ,rust-glutin-0.19)
                                   ("rust-spin-sleep" ,rust-spin-sleep-0.3))))
    (home-page "https://github.com/alexheretic/glyph-brush")
    (synopsis "Fast GPU cached text rendering using gfx-rs & ab_glyph")
    (description
     "This package provides fast GPU cached text rendering using gfx-rs &
ab_glyph.")
    (license license:asl2.0)))

(define-public rust-gfx-macros-0.2
  (package
    (name "rust-gfx-macros")
    (version "0.2.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gfx-macros" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "07rh9lh6d89p3lwvasqzg2jsnmf9f7ykazj3fdpb41a77wfrx3yk"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-quote" ,rust-quote-0.3)
                       ("rust-syn" ,rust-syn-0.11))))
    (home-page "https://github.com/gfx-rs/gfx")
    (synopsis "Custom derive macros for gfx-rs")
    (description "This package provides custom derive macros for gfx-rs.")
    (license license:asl2.0)))

(define-public rust-gfx-window-dxgi-0.17
  (package
    (name "rust-gfx-window-dxgi")
    (version "0.17.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gfx-window-dxgi" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0p379jni10qg39a6r8ymk1hdm5l5w9vhjq7glb27gkd97q9y4rc3"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-gfx-core" ,rust-gfx-core-0.8)
                       ("rust-gfx-device-dx11" ,rust-gfx-device-dx11-0.7)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-winapi" ,rust-winapi-0.3)
                       ("rust-winit" ,rust-winit-0.18))))
    (home-page "https://github.com/gfx-rs/gfx")
    (synopsis "DXGI window for gfx-rs")
    (description "This package provides DXGI window for gfx-rs.")
    (license license:asl2.0)))

(define-public rust-gfx-window-glutin-0.27
  (package
    (name "rust-gfx-window-glutin")
    (version "0.27.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gfx-window-glutin" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "00vhm133cd23a19v5b1ca3szj9apqdjci91arpvldn5i8zammi8c"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-gfx-core" ,rust-gfx-core-0.8)
                       ("rust-gfx-device-gl" ,rust-gfx-device-gl-0.15)
                       ("rust-glutin" ,rust-glutin-0.19))))
    (home-page "https://github.com/gfx-rs/gfx")
    (synopsis "Glutin window for gfx-rs")
    (description "This package provides the glutin window for gfx-rs.")
    (license license:asl2.0)))

(define-public rust-gfx-window-metal-0.6
  (package
    (name "rust-gfx-window-metal")
    (version "0.6.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gfx-window-metal" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0wyx425vyyl0n04rh84dvlzg7x493wq851i5v4x03cjqqxvg4jd3"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cocoa" ,rust-cocoa-0.9)
                       ("rust-gfx-core" ,rust-gfx-core-0.8)
                       ("rust-gfx-device-metal" ,rust-gfx-device-metal-0.3)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-metal-rs" ,rust-metal-rs-0.4)
                       ("rust-objc" ,rust-objc-0.2)
                       ("rust-winit" ,rust-winit-0.12))))
    (home-page "https://github.com/gfx-rs/gfx")
    (synopsis "Metal window for gfx-rs")
    (description "This package provides the Metal window for gfx-rs.")
    (license license:asl2.0)))

(define-public rust-gfx-window-vulkan-0.1
  (package
    (name "rust-gfx-window-vulkan")
    (version "0.1.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gfx-window-vulkan" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "11a6hh04s2dsdww31979srs75ad7wqv3gl35gk3xxs450jkb02d0"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-gfx-core" ,rust-gfx-core-0.6)
                       ("rust-gfx-device-vulkan" ,rust-gfx-device-vulkan-0.1)
                       ("rust-kernel32-sys" ,rust-kernel32-sys-0.2)
                       ("rust-vk-sys" ,rust-vk-sys-0.2)
                       ("rust-winit" ,rust-winit-0.5))))
    (home-page "https://github.com/gfx-rs/gfx")
    (synopsis "Vulkan window for gfx-rs")
    (description "This package provides a Vulkan window for gfx-rs.")
    (license license:asl2.0)))

(define-public rust-ggez-0.7
  (package
    (name "rust-ggez")
    (version "0.7.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ggez" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "09l5yyimmz8j4cj5925miic787xzw9bax0ara8ghmnd34pmy1mf1"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;Could not find test files.
       #:cargo-inputs (("rust-approx" ,rust-approx-0.5)
                       ("rust-bitflags" ,rust-bitflags-2)
                       ("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-directories" ,rust-directories-3)
                       ("rust-gfx" ,rust-gfx-0.18)
                       ("rust-gfx-core" ,rust-gfx-core-0.9)
                       ("rust-gfx-device-gl" ,rust-gfx-device-gl-0.16)
                       ("rust-gilrs" ,rust-gilrs-0.9)
                       ("rust-glam" ,rust-glam-0.20)
                       ("rust-glutin" ,rust-glutin-0.27)
                       ("rust-glyph-brush" ,rust-glyph-brush-0.7)
                       ("rust-image" ,rust-image-0.24)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-lyon" ,rust-lyon-0.17)
                       ("rust-mint" ,rust-mint-0.5)
                       ("rust-old-school-gfx-glutin-ext" ,rust-old-school-gfx-glutin-ext-0.27)
                       ("rust-rodio" ,rust-rodio-0.16)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-derive" ,rust-serde-derive-1)
                       ("rust-smart-default" ,rust-smart-default-0.6)
                       ("rust-toml" ,rust-toml-0.5)
                       ("rust-winit" ,rust-winit-0.25)
                       ("rust-zip" ,rust-zip-0.6)
                       ("rust-skeptic" ,rust-skeptic-0.13))
       #:cargo-development-inputs (("rust-argh" ,rust-argh-0.1)
                                   ("rust-chrono" ,rust-chrono-0.4)
                                   ("rust-fern" ,rust-fern-0.6)
                                   ("rust-getrandom" ,rust-getrandom-0.2)
                                   ("rust-keyframe" ,rust-keyframe-1)
                                   ("rust-keyframe-derive" ,rust-keyframe-derive-1)
                                   ("rust-num-derive" ,rust-num-derive-0.3)
                                   ("rust-num-traits" ,rust-num-traits-0.2)
                                   ("rust-oorandom" ,rust-oorandom-11.1)
                                   ("rust-rand" ,rust-rand-0.8)
                                   ("rust-skeptic" ,rust-skeptic-0.13))))
    (inputs (list alsa-lib eudev))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/ggez/ggez")
    (synopsis "@code{ggez} is a Rust library to create a Good Game Easily")
    (description
     "@code{ggez} is a lightweight cross-platform game framework for making 2D games
with minimum friction. It aims to implement an API based on (a
Rustified version of) the LÖVE game framework.")
    (license license:expat)))

(define-public rust-gif-0.9
  (package
    (inherit rust-gif-0.11)
    (name "rust-gif")
    (version "0.9.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gif" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0py271291bkg5k1wqgawv0l4zjqsv1rnsx943gskpnr3p92ikr72"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-color-quant" ,rust-color-quant-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-lzw" ,rust-lzw-0.10))
       #:cargo-development-inputs (("rust-glob" ,rust-glob-0.2))))))

(define-public rust-gif-0.7
  (package
    (inherit rust-gif-0.11)
    (name "rust-gif")
    (version "0.7.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gif" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1wqds4da9zpa62lcbj1fmgqq9fwsy6fkwrkfykhd022j9jxksh25"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-color-quant" ,rust-color-quant-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-lzw" ,rust-lzw-0.9))
       #:cargo-development-inputs (("rust-glob" ,rust-glob-0.3))))))

(define-public rust-gif-0.5
  (package
    (inherit rust-gif-0.11)
    (name "rust-gif")
    (version "0.5.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gif" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "13jf70z71x2kx4hwn8vwzjhr4fh84rhwy5vichs2nbi8pir51zll"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-color-quant" ,rust-color-quant-1)
                       ("rust-libc" ,rust-libc-0.1)
                       ("rust-lzw" ,rust-lzw-0.8))
       #:cargo-development-inputs (("rust-glob" ,rust-glob-0.3))))))

(define-public rust-glam-0.24
  (package
    (name "rust-glam")
    (version "0.24.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glam" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "09h2al85ci7g153v6cs7dh50i7i2fp4ks59wrir3hkl482v8q8a2"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-approx" ,rust-approx-0.5)
                       ("rust-bytecheck" ,rust-bytecheck-0.6)
                       ("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-mint" ,rust-mint-0.5)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-rkyv" ,rust-rkyv-0.7)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.4)
                                   ("rust-rand-xoshiro" ,rust-rand-xoshiro-0.6)
                                   ("rust-serde-json" ,rust-serde-json-1)
                                   ("rust-wasm-bindgen-test" ,rust-wasm-bindgen-test-0.3))))
    (home-page "https://github.com/bitshifter/glam-rs")
    (synopsis "3D math library for games and graphics")
    (description
     "This package provides a simple and fast 3D math library for games and
graphics.")
    (license (list license:expat license:asl2.0))))

(define-public rust-glam-0.23
  (package
    (inherit rust-glam-0.24)
    (name "rust-glam")
    (version "0.23.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glam" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "076mdgmy38lhlvgng74iy2sr3iih58gx5qcz20g0hmamv6dgsjlf"))))
    (arguments
     `(#:cargo-inputs (("rust-approx" ,rust-approx-0.5)
                       ("rust-bytecheck" ,rust-bytecheck-0.6)
                       ("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-mint" ,rust-mint-0.5)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-rkyv" ,rust-rkyv-0.7)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.4)
                                   ("rust-rand-xoshiro" ,rust-rand-xoshiro-0.6)
                                   ("rust-serde-json" ,rust-serde-json-1)
                                   ("rust-wasm-bindgen-test" ,rust-wasm-bindgen-test-0.3))))))

(define-public rust-glam-0.22
  (package
    (inherit rust-glam-0.24)
    (name "rust-glam")
    (version "0.22.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glam" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0x57gyrxyfs409b3f5i64yy2pbcgkr2qkq8v3a0mmm8vdkargx8j"))))
    (arguments
     `(#:cargo-inputs (("rust-approx" ,rust-approx-0.5)
                       ("rust-bytecheck" ,rust-bytecheck-0.6)
                       ("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-mint" ,rust-mint-0.5)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-rkyv" ,rust-rkyv-0.7)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.4)
                                   ("rust-rand-xoshiro" ,rust-rand-xoshiro-0.6)
                                   ("rust-serde-json" ,rust-serde-json-1)
                                   ("rust-wasm-bindgen-test" ,rust-wasm-bindgen-test-0.3))))))

(define-public rust-glam-0.21
  (package
    (inherit rust-glam-0.24)
    (name "rust-glam")
    (version "0.21.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glam" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "05c8r639hz3ydlhbhv6d9z6193iivi8276zz2fq3hqw6ci8am3si"))))
    (arguments
     `(#:cargo-inputs (("rust-approx" ,rust-approx-0.5)
                       ("rust-bytecheck" ,rust-bytecheck-0.6)
                       ("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-mint" ,rust-mint-0.5)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-rkyv" ,rust-rkyv-0.7)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.4)
                                   ("rust-rand-xoshiro" ,rust-rand-xoshiro-0.6)
                                   ("rust-serde-json" ,rust-serde-json-1)
                                   ("rust-wasm-bindgen-test" ,rust-wasm-bindgen-test-0.3))))))

(define-public rust-glam-0.20
  (package
    (inherit rust-glam-0.24)
    (name "rust-glam")
    (version "0.20.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glam" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0pv7n634vw2hn9fmi915pzgangyljgsp4m5505c3zq2bfiz9agpl"))))
    (arguments
     `(#:cargo-inputs (("rust-approx" ,rust-approx-0.5)
                       ("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-mint" ,rust-mint-0.5)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.4)
                                   ("rust-rand-xoshiro" ,rust-rand-xoshiro-0.6)
                                   ("rust-serde-json" ,rust-serde-json-1)
                                   ("rust-wasm-bindgen-test" ,rust-wasm-bindgen-test-0.3))))))

(define-public rust-glam-0.19
  (package
    (inherit rust-glam-0.24)
    (name "rust-glam")
    (version "0.19.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glam" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1jjia0nva5p7ca3dma8jhzxd4dmczgbhn3b338ffh5hmg7k0k19b"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-approx" ,rust-approx-0.5)
                       ("rust-bytecheck" ,rust-bytecheck-0.6)
                       ("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-mint" ,rust-mint-0.5)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-rkyv" ,rust-rkyv-0.7)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.4)
                                   ("rust-rand-xoshiro" ,rust-rand-xoshiro-0.6)
                                   ("rust-serde-json" ,rust-serde-json-1)
                                   ("rust-wasm-bindgen-test" ,rust-wasm-bindgen-test-0.3))))))

(define-public rust-glam-0.18
  (package
    (inherit rust-glam-0.24)
    (name "rust-glam")
    (version "0.18.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glam" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0d7f7f7cdbahxk677k6l8h7r4bxxni24nzdndwr8wyx71d4kwnjj"))))
    (arguments
     `(#:cargo-inputs (("rust-approx" ,rust-approx-0.5)
                       ("rust-bytecheck" ,rust-bytecheck-0.6)
                       ("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-mint" ,rust-mint-0.5)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-rkyv" ,rust-rkyv-0.7)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.4)
                                   ("rust-rand-xoshiro" ,rust-rand-xoshiro-0.6)
                                   ("rust-serde-json" ,rust-serde-json-1)
                                   ("rust-wasm-bindgen-test" ,rust-wasm-bindgen-test-0.3))))))

(define-public rust-glam-0.8
  (package
    (inherit rust-glam-0.24)
    (name "rust-glam")
    (version "0.8.7")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glam" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "084vqw764aq2f34a13pcrl5x4yfkr2s2a2p2bd4sqw00n6sp41dh"))))
    (arguments
     `(#:cargo-inputs (("rust-mint" ,rust-mint-0.5)
                       ("rust-rand" ,rust-rand-0.7)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.3)
                                   ("rust-rand-xoshiro" ,rust-rand-xoshiro-0.4)
                                   ("rust-serde-json" ,rust-serde-json-1))))))

(define-public rust-gl-0.12
  (package
    (inherit rust-gl-0.14)
    (name "rust-gl")
    (version "0.12.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gl" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "135v8ld0545qwd6igi07600p6wra2nnjhqxwaj29f2sjjdsq27dx"))))
    (arguments
     `(#:cargo-inputs (("rust-gl-generator" ,rust-gl-generator-0.11))
       #:cargo-development-inputs (("rust-glutin" ,rust-glutin-0.19))))))

(define-public rust-gl-generator-0.10
  (package
    (inherit rust-gl-generator-0.14)
    (name "rust-gl-generator")
    (version "0.10.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gl-generator" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0146yd4i9wbgfrhnkc04w7n7civbanznc0q87skp6v7p7hbszzx0"))))
    (arguments
     `(#:cargo-inputs (("rust-khronos-api" ,rust-khronos-api-3)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-xml-rs" ,rust-xml-rs-0.8))))))

(define-public rust-gl-generator-0.9
  (package
    (inherit rust-gl-generator-0.14)
    (name "rust-gl-generator")
    (version "0.9.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gl-generator" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "02lx6zfvpszp43161645hvj06smfbi9dgmjqm9xmlnyqrdq52ybs"))))
    (arguments
     `(#:cargo-inputs (("rust-khronos-api" ,rust-khronos-api-2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-xml-rs" ,rust-xml-rs-0.7))))))

(define-public rust-gl-generator-0.5
  (package
    (inherit rust-gl-generator-0.14)
    (name "rust-gl-generator")
    (version "0.5.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gl-generator" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0756xj1inkh0j5mcigsyyl5amhg954hkd13av4d9wbnmlcmvzb77"))))
    (arguments
     `(#:cargo-inputs (("rust-khronos-api" ,rust-khronos-api-1)
                       ("rust-log" ,rust-log-0.3)
                       ("rust-xml-rs" ,rust-xml-rs-0.6))))))

(define-public rust-gl-generator-0.4
  (package
    (inherit rust-gl-generator-0.14)
    (name "rust-gl-generator")
    (version "0.4.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gl-generator" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "048hnacz40sabbq50nccjf5qdiq9hw1wq0sbbmkkzpksdhnmp41p"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-khronos-api" ,rust-khronos-api-1)
                       ("rust-log" ,rust-log-0.3)
                       ("rust-xml-rs" ,rust-xml-rs-0.2))))))

(define-public rust-gl-generator-0.3
  (package
    (inherit rust-gl-generator-0.14)
    (name "rust-gl-generator")
    (version "0.3.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gl-generator" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1y3kq1rcd60ax9xx0bwrrsrbpbpwij470q9plz8006yd7xxz8rw4"))))
    (arguments
     `(#:skip-build? #t ;could not fetch dependencies
       #:cargo-inputs (("rust-khronos-api" ,rust-khronos-api-0)
                       ("rust-log" ,rust-log-0.3)
                       ("rust-xml-rs" ,rust-xml-rs-0.1))))))

(define-public rust-gleam-0.2
  (package
    (inherit rust-gleam-0.6)
    (name "rust-gleam")
    (version "0.2.32")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gleam" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "009d8rgxygh7sjpzl5kk7jklkqzvgs76gb5bqn0a0a6mg3jy144m"))))
    (arguments
     `(#:skip-build? #t ;type not implemented error
       #:cargo-inputs (("rust-gl-generator" ,rust-gl-generator-0.5)
                       ("rust-pkg-config" ,rust-pkg-config-0.3))))))

(define-public rust-glium-0.32
  (package
    (name "rust-glium")
    (version "0.32.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glium" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1gwknlxblg5hlwjg91y8pp5slr9dnca7cs1x3nwi8q5qxhl6fxnj"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;Incampatible event loop integration test
       #:cargo-inputs (("rust-backtrace" ,rust-backtrace-0.3)
                       ("rust-fnv" ,rust-fnv-1)
                       ("rust-gl-generator" ,rust-gl-generator-0.14)
                       ("rust-glutin" ,rust-glutin-0.29)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-memoffset" ,rust-memoffset-0.6)
                       ("rust-smallvec" ,rust-smallvec-1)
                       ("rust-takeable-option" ,rust-takeable-option-0.5))
       #:cargo-development-inputs (("rust-cgmath" ,rust-cgmath-0.18)
                                   ("rust-genmesh" ,rust-genmesh-0.6)
                                   ("rust-image" ,rust-image-0.24)
                                   ("rust-libc" ,rust-libc-0.2)
                                   ("rust-obj" ,rust-obj-0.10)
                                   ("rust-ouroboros" ,rust-ouroboros-0.15)
                                   ("rust-rand" ,rust-rand-0.8))))
    (inputs (list expat fontconfig freetype))
    (native-inputs (list cmake pkg-config))
    (home-page "https://github.com/glium/glium")
    (synopsis "Elegant and safe OpenGL wrapper")
    (description
     "Glium is an intermediate layer between OpenGL and your
application. You still need to manually handle the graphics pipeline,
but without having to use OpenGL's old and error-prone API.")
    (license license:asl2.0)))

(define-public rust-glium-0.16
  (package
    (inherit rust-glium-0.32)
    (name "rust-glium")
    (version "0.16.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glium" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "04bg2y2c117152z9dgrkzm157bf1mkmndw202559a9azhpvqnikw"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-backtrace" ,rust-backtrace-0.2)
                       ("rust-fnv" ,rust-fnv-1)
                       ("rust-gl-generator" ,rust-gl-generator-0.5)
                       ("rust-glutin" ,rust-glutin-0.7)
                       ("rust-lazy-static" ,rust-lazy-static-0.2)
                       ("rust-smallvec" ,rust-smallvec-0.1))
       #:cargo-development-inputs (("rust-cgmath" ,rust-cgmath-0.9)
                                   ("rust-genmesh" ,rust-genmesh-0.4)
                                   ("rust-image" ,rust-image-0.12)
                                   ("rust-obj" ,rust-obj-0.5)
                                   ("rust-rand" ,rust-rand-0.3))))))

(define-public rust-glium-0.14
  (package
    (inherit rust-glium-0.32)
    (name "rust-glium")
    (version "0.14.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glium" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1ww6hfx31v70jss3f374sm0ji7wr3cv98ppcwbfsvrk98ghwxr1z"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-backtrace" ,rust-backtrace-0.1)
                       ("rust-gl-generator" ,rust-gl-generator-0.5)
                       ("rust-glutin" ,rust-glutin-0.5)
                       ("rust-lazy-static" ,rust-lazy-static-0.1)
                       ("rust-smallvec" ,rust-smallvec-0.1))
       #:cargo-development-inputs (("rust-cgmath" ,rust-cgmath-0.7)
                                   ("rust-clock-ticks" ,rust-clock-ticks-0.1)
                                   ("rust-genmesh" ,rust-genmesh-0.4)
                                   ("rust-image" ,rust-image-0.7)
                                   ("rust-obj" ,rust-obj-0.5)
                                   ("rust-rand" ,rust-rand-0.3))))))

(define-public rust-glium-0.13
  (package
    (inherit rust-glium-0.32)
    (name "rust-glium")
    (version "0.13.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glium" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1f8m64vzswkxxxm0sbyxrhcrcnhwhdwbzjlpqvhfw94x90cqif2g"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-backtrace" ,rust-backtrace-0.1)
                       ("rust-gl-generator" ,rust-gl-generator-0.3)
                       ("rust-glutin" ,rust-glutin-0.4)
                       ("rust-khronos-api" ,rust-khronos-api-0)
                       ("rust-lazy-static" ,rust-lazy-static-0.1)
                       ("rust-smallvec" ,rust-smallvec-0.1))
       #:cargo-development-inputs (("rust-cgmath" ,rust-cgmath-0.7)
                                   ("rust-clock-ticks" ,rust-clock-ticks-0.1)
                                   ("rust-genmesh" ,rust-genmesh-0.4)
                                   ("rust-image" ,rust-image-0.4)
                                   ("rust-obj" ,rust-obj-0.5)
                                   ("rust-rand" ,rust-rand-0.3))))))

(define-public rust-glow-0.12
  (package
    (name "rust-glow")
    (version "0.12.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glow" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0a1p6c9nff09m4gn0xnnschcpjq35y7c12w69ar8l2mnwj0fa3ya"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-js-sys" ,rust-js-sys-0.3)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-slotmap" ,rust-slotmap-1)
                       ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
                       ("rust-web-sys" ,rust-web-sys-0.3))))
    (home-page "https://github.com/grovesNL/glow.git")
    (synopsis "A set of bindings to run GL (Open GL, OpenGL ES, and WebGL)")
    (description
     "GL on Whatever: a set of bindings to run GL (Open GL, @code{OpenGL} ES, and
@code{WebGL)} anywhere, and avoid target-specific code.")
    (license (list license:expat license:asl2.0 license:zlib))))

(define-public rust-glsl-layout-0.1
  (package
    (name "rust-glsl-layout")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glsl-layout" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "16ba8bxfsgzcfvlplr4h31k44dvlhj51qpqv6wy2vl2jk4jvrw34"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-cgmath" ,rust-cgmath-0.16)
                       ("rust-gfx-core" ,rust-gfx-core-0.8)
                       ("rust-glsl-layout-derive" ,rust-glsl-layout-derive-0.1))))
    (home-page "https://github.com/rustgd/glsl-layout")
    (synopsis
     "Provides data types and traits to build structures ready to upload
into UBO")
    (description
     "This package provides data types and traits to build structures ready
to upload into UBO.")
    (license (list license:expat license:asl2.0))))

(define-public rust-glsl-layout-derive-0.1
  (package
    (name "rust-glsl-layout-derive")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glsl-layout-derive" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1jn41i088lx6vng3phj514xfq5z86mwf2dim0iryi3c2m6x0d62v"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-quote" ,rust-quote-0.5)
                       ("rust-syn" ,rust-syn-0.13))))
    (home-page "https://github.com/rustgd/glsl-layout")
    (synopsis "Custom derive for `glsl-layout` crate.")
    (description "Custom derive for `glsl-layout` crate.")
    (license (list license:expat license:asl2.0))))

(define-public rust-gltf-0.11
  (package
    (name "rust-gltf")
    (version "0.11.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gltf" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0anwxl16cspnhr2jrms7mfdy5jixh8a5wd27v1vlsw974dv8bq94"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-base64" ,rust-base64-0.6)
                       ("rust-byteorder" ,rust-byteorder-1)
                       ("rust-cgmath" ,rust-cgmath-0.15)
                       ("rust-gltf-json" ,rust-gltf-json-0.11)
                       ("rust-image" ,rust-image-0.19)
                       ("rust-lazy-static" ,rust-lazy-static-0.2))
       #:cargo-development-inputs (("rust-approx" ,rust-approx-0.1))))
    (home-page "https://github.com/gltf-rs/gltf")
    (synopsis "glTF 2.0 loader")
    (description "This package provides the @code{glTF} 2.0 loader.")
    (license (list license:expat license:asl2.0))))

(define-public rust-gltf-derive-0.11
  (package
    (name "rust-gltf-derive")
    (version "0.11.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gltf-derive" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1i32r9hfnxb1i7sfb84gsir9saxxwf09xf3dllh1zq6l72n6m5lx"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-inflections" ,rust-inflections-1)
                       ("rust-quote" ,rust-quote-0.3)
                       ("rust-syn" ,rust-syn-0.11))))
    (home-page "https://github.com/gltf-rs/gltf")
    (synopsis "Internal macros for the gltf crate")
    (description "This package provides internal macros for the gltf crate.")
    (license (list license:expat license:asl2.0))))

(define-public rust-gltf-json-0.11
  (package
    (name "rust-gltf-json")
    (version "0.11.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gltf-json" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "03v3yy1pp0i34q12362bf3gfajycy8w16yx6v5x23rgnni8d7wwz"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-gltf-derive" ,rust-gltf-derive-0.11)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-derive" ,rust-serde-derive-1)
                       ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/gltf-rs/gltf")
    (synopsis "JSON parsing for the gltf crate")
    (description "This package provides JSON parsing for the gltf crate.")
    (license (list license:expat license:asl2.0))))

(define-public rust-glutin-0.29
  (package
    (name "rust-glutin")
    (version "0.29.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glutin" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "04y7s5m74j9gns5bdja0alkm0m0b727vf9k7rw6g5jpxjk99lk24"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-cgl" ,rust-cgl-0.3)
                       ("rust-cocoa" ,rust-cocoa-0.24)
                       ("rust-core-foundation" ,rust-core-foundation-0.9)
                       ("rust-glutin-egl-sys" ,rust-glutin-egl-sys-0.1)
                       ("rust-glutin-gles2-sys" ,rust-glutin-gles2-sys-0.1)
                       ("rust-glutin-glx-sys" ,rust-glutin-glx-sys-0.1)
                       ("rust-glutin-wgl-sys" ,rust-glutin-wgl-sys-0.1)
                       ("rust-libloading" ,rust-libloading-0.7)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-objc" ,rust-objc-0.2)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-osmesa-sys" ,rust-osmesa-sys-0.1)
                       ("rust-parking-lot" ,rust-parking-lot-0.12)
                       ("rust-raw-window-handle" ,rust-raw-window-handle-0.5)
                       ("rust-wayland-client" ,rust-wayland-client-0.29)
                       ("rust-wayland-egl" ,rust-wayland-egl-0.29)
                       ("rust-winapi" ,rust-winapi-0.3)
                       ("rust-winit" ,rust-winit-0.27))))
    (inputs (list expat fontconfig freetype))
    (native-inputs (list pkg-config cmake))
    (home-page "https://github.com/rust-windowing/glutin")
    (synopsis "Cross-platform OpenGL context provider.")
    (description
     "This package provides a cross-platform @code{OpenGL} context provider.")
    (license license:asl2.0)))

(define-public rust-glutin-0.27
  (package
    (name "rust-glutin")
    (version "0.27.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glutin" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1w58vcni5sq0h03s5a9rmj2rsraqj3693rgbd2bdjmdqw796qbbn"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-android-glue" ,rust-android-glue-0.2)
                       ("rust-cgl" ,rust-cgl-0.3)
                       ("rust-cocoa" ,rust-cocoa-0.24)
                       ("rust-core-foundation" ,rust-core-foundation-0.9)
                       ("rust-glutin-egl-sys" ,rust-glutin-egl-sys-0.1)
                       ("rust-glutin-emscripten-sys" ,rust-glutin-emscripten-sys-0.1)
                       ("rust-glutin-gles2-sys" ,rust-glutin-gles2-sys-0.1)
                       ("rust-glutin-glx-sys" ,rust-glutin-glx-sys-0.1)
                       ("rust-glutin-wgl-sys" ,rust-glutin-wgl-sys-0.1)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-libloading" ,rust-libloading-0.7)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-objc" ,rust-objc-0.2)
                       ("rust-osmesa-sys" ,rust-osmesa-sys-0.1)
                       ("rust-parking-lot" ,rust-parking-lot-0.11)
                       ("rust-wayland-commons" ,rust-wayland-commons-0.28)
                       ("rust-wayland-egl" ,rust-wayland-egl-0.28)
                       ("rust-wayland-scanner" ,rust-wayland-scanner-0.28)
                       ("rust-wayland-sys" ,rust-wayland-sys-0.28)
                       ("rust-winapi" ,rust-winapi-0.3)
                       ("rust-winit" ,rust-winit-0.25))))
    (home-page "https://github.com/tomaka/glutin")
    (synopsis "Cross-platform OpenGL context provider")
    (description "This package provides an OpenGL context provider.")
    (license license:asl2.0)))

(define-public rust-glutin-0.19
  (package
    (inherit rust-glutin-0.30)
    (name "rust-glutin")
    (version "0.19.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glutin" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1hzw4kmqmng773rp1sd3gsfk7mpz4ah11nxj0iv25fxdb3d6wp2k"))))
    (arguments
     `(#:cargo-inputs (("rust-android-glue" ,rust-android-glue-0.2)
                       ("rust-cgl" ,rust-cgl-0.2)
                       ("rust-cocoa" ,rust-cocoa-0.18)
                       ("rust-core-foundation" ,rust-core-foundation-0.6)
                       ("rust-core-graphics" ,rust-core-graphics-0.17)
                       ("rust-gl-generator" ,rust-gl-generator-0.10)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-objc" ,rust-objc-0.2)
                       ("rust-osmesa-sys" ,rust-osmesa-sys-0.1)
                       ("rust-shared-library" ,rust-shared-library-0.1)
                       ("rust-wayland-client" ,rust-wayland-client-0.21)
                       ("rust-winapi" ,rust-winapi-0.3)
                       ("rust-winit" ,rust-winit-0.18)
                       ("rust-x11-dl" ,rust-x11-dl-2))))))

(define-public rust-glutin-0.7
  (package
    (inherit rust-glutin-0.30)
    (name "rust-glutin")
    (version "0.7.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glutin" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0aziw2fzl0i384jzahdwayc1a6jvxdwbhpd2nicp4qk3hfdcr58z"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-android-glue" ,rust-android-glue-0.2)
                       ("rust-cgl" ,rust-cgl-0.1)
                       ("rust-cocoa" ,rust-cocoa-0.3)
                       ("rust-core-foundation" ,rust-core-foundation-0.2)
                       ("rust-core-graphics" ,rust-core-graphics-0.3)
                       ("rust-dwmapi-sys" ,rust-dwmapi-sys-0.1)
                       ("rust-gdi32-sys" ,rust-gdi32-sys-0.1)
                       ("rust-gl-generator" ,rust-gl-generator-0.5)
                       ("rust-kernel32-sys" ,rust-kernel32-sys-0.2)
                       ("rust-lazy-static" ,rust-lazy-static-0.2)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-objc" ,rust-objc-0.2)
                       ("rust-osmesa-sys" ,rust-osmesa-sys-0.1)
                       ("rust-shared-library" ,rust-shared-library-0.1)
                       ("rust-shell32-sys" ,rust-shell32-sys-0.1)
                       ("rust-user32-sys" ,rust-user32-sys-0.1)
                       ("rust-wayland-client" ,rust-wayland-client-0.7)
                       ("rust-winapi" ,rust-winapi-0.2)
                       ("rust-winit" ,rust-winit-0.5)
                       ("rust-x11-dl" ,rust-x11-dl-2))))))

(define-public rust-glutin-0.5
  (package
    (inherit rust-glutin-0.30)
    (name "rust-glutin")
    (version "0.5.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glutin" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "118ysr78ggwr04xs203pnxpln1fnyxksma08imr1c007h9vinsjl"))))
    (arguments
     `(#:skip-build? #t ;custom build command failed
       #:cargo-inputs (("rust-android-glue" ,rust-android-glue-0.1)
                       ("rust-cgl" ,rust-cgl-0.1)
                       ("rust-cocoa" ,rust-cocoa-0.3)
                       ("rust-core-foundation" ,rust-core-foundation-0.9)
                       ("rust-core-graphics" ,rust-core-graphics-0.3)
                       ("rust-dwmapi-sys" ,rust-dwmapi-sys-0.1)
                       ("rust-gdi32-sys" ,rust-gdi32-sys-0.1)
                       ("rust-gl-generator" ,rust-gl-generator-0.5)
                       ("rust-kernel32-sys" ,rust-kernel32-sys-0.2)
                       ("rust-lazy-static" ,rust-lazy-static-0.2)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-objc" ,rust-objc-0.2)
                       ("rust-osmesa-sys" ,rust-osmesa-sys-0.1)
                       ("rust-shared-library" ,rust-shared-library-0.1)
                       ("rust-shell32-sys" ,rust-shell32-sys-0.1)
                       ("rust-user32-sys" ,rust-user32-sys-0.1)
                       ("rust-wayland-client" ,rust-wayland-client-0.5)
                       ("rust-wayland-kbd" ,rust-wayland-kbd-0.3)
                       ("rust-wayland-window" ,rust-wayland-window-0.2)
                       ("rust-winapi" ,rust-winapi-0.2)
                       ("rust-x11-dl" ,rust-x11-dl-2))))))

(define-public rust-glutin-0.4
  (package
    (inherit rust-glutin-0.30)
    (name "rust-glutin")
    (version "0.4.9")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glutin" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "10d259jqbsalalnbag5mkqpqswyl424k666sqjzxbjlj2s5in7x7"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-android-glue" ,rust-android-glue-0.1)
                       ("rust-cgl" ,rust-cgl-0.1)
                       ("rust-cocoa" ,rust-cocoa-0.2)
                       ("rust-core-foundation" ,rust-core-foundation-0.9)
                       ("rust-core-graphics" ,rust-core-graphics-0.22)
                       ("rust-dwmapi-sys" ,rust-dwmapi-sys-0.1)
                       ("rust-gdi32-sys" ,rust-gdi32-sys-0.1)
                       ("rust-gl-generator" ,rust-gl-generator-0.4)
                       ("rust-kernel32-sys" ,rust-kernel32-sys-0.2)
                       ("rust-khronos-api" ,rust-khronos-api-1)
                       ("rust-lazy-static" ,rust-lazy-static-0.1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-objc" ,rust-objc-0.1)
                       ("rust-osmesa-sys" ,rust-osmesa-sys-0.0.5)
                       ("rust-shared-library" ,rust-shared-library-0.1)
                       ("rust-shell32-sys" ,rust-shell32-sys-0.1)
                       ("rust-user32-sys" ,rust-user32-sys-0.1)
                       ("rust-wayland-client" ,rust-wayland-client-0.5)
                       ("rust-wayland-kbd" ,rust-wayland-kbd-0.3)
                       ("rust-wayland-window" ,rust-wayland-window-0.2)
                       ("rust-winapi" ,rust-winapi-0.2)
                       ("rust-x11-dl" ,rust-x11-dl-2.3))))))

(define-public rust-glutin-egl-sys-0.1
  (package
    (inherit rust-glutin-egl-sys-0.4)
    (name "rust-glutin-egl-sys")
    (version "0.1.6")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glutin-egl-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0g81bz7ppvaksvwcw1jg553g8b2shvmnfm9ms6hixwvinj20z438"))))
    (arguments
     `(#:cargo-inputs (("rust-winapi" ,rust-winapi-0.3)
                       ("rust-gl-generator" ,rust-gl-generator-0.14))))))

(define-public rust-glutin-glx-sys-0.1
  (package
    (inherit rust-glutin-glx-sys-0.4)
    (name "rust-glutin-glx-sys")
    (version "0.1.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glutin_glx_sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0s14s3v2dddkx141w2x65s8ik54mrn432hisbc65i62hhrshagfr"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-gl-generator" ,rust-gl-generator-0.14)
                       ("rust-x11-dl" ,rust-x11-dl-2))))))

(define-public rust-glutin-winit-0.3
  (package
    (name "rust-glutin-winit")
    (version "0.3.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glutin-winit" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "194gb38qqi3119fldk1vw9qv7mxwfcvw15wgzq5q6qj0q0zqg6k2"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-glutin" ,rust-glutin-0.30)
                       ("rust-raw-window-handle" ,rust-raw-window-handle-0.5)
                       ("rust-winit" ,rust-winit-0.28)
                       ("rust-cfg-aliases" ,rust-cfg-aliases-0.1))))
    (inputs (list wayland libxkbcommon))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/rust-windowing/glutin")
    (synopsis "Glutin bootstrapping helpers with winit")
    (description
     "The crate provides cross-platform glutin Display bootstrapping with winit.
This crate is also a reference on how to do the bootstrapping of
glutin when used with a cross-platform windowing library.")
    (license license:expat)))

(define-public rust-glyph-brush-0.7
  (package
    (name "rust-glyph-brush")
    (version "0.7.7")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glyph_brush" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "13cp8brq843105w9f00831lv681hnk18k9643vab38187w9gvpjf"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-glyph-brush-draw-cache" ,rust-glyph-brush-draw-cache-0.1)
                       ("rust-glpyh-brush-layout" ,rust-glyph-brush-layout-0.2)
                       ("rust-ordered-float" ,rust-ordered-float-3)
                       ("rust-rustc-hash" ,rust-rustc-hash-1)
                       ("rust-twox-hash" ,rust-twox-hash-1))
       #:cargo-development-inputs (("rust-approx" ,rust-approx-0.5)
                                   ("rust-criterion" ,rust-criterion-0.4)
                                   ("rust-env-logger" ,rust-env-logger-0.10)
                                   ("rust-gl" ,rust-gl-0.14)
                                   ("rust-glutin" ,rust-glutin-0.30)
                                   ("rust-glutin-winit" ,rust-glutin-winit-0.3)
                                   ("rust-once-cell" ,rust-once-cell-1)
                                   ("rust-raw-window-handle" ,rust-raw-window-handle-0.5)
                                   ("rust-spin-sleep" ,rust-spin-sleep-1)
                                   ("rust-winit" ,rust-winit-0.28))))
    (home-page "https://github.com/alexheretic/glyph-brush")
    (synopsis "Fast cached text render library using ab_glyph")
    (description
     "Fast caching text render library using ab_glyph. Provides render API
agnostic rasterization & draw caching logic.")
    (license license:asl2.0)))

(define-public rust-glyph-brush-0.4
  (package
    (inherit rust-glyph-brush-0.7)
    (name "rust-glyph-brush")
    (version "0.4.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glyph-brush" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1129mbsq3vxxv153dqgbz2qm78zv4i16gpccaci79n90xpqfp55y"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-glyph-brush-layout" ,rust-glyph-brush-layout-0.1)
                       ("rust-hashbrown" ,rust-hashbrown-0.3)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-ordered-float" ,rust-ordered-float-1)
                       ("rust-rusttype" ,rust-rusttype-0.7)
                       ("rust-twox-hash" ,rust-twox-hash-1))
       #:cargo-development-inputs (("rust-env-logger" ,rust-env-logger-0.6)
                                   ("rust-gl" ,rust-gl-0.12)
                                   ("rust-glutin" ,rust-glutin-0.21)
                                   ("rust-lazy-static" ,rust-lazy-static-1)
                                   ("rust-spin-sleep" ,rust-spin-sleep-0.3))))))

(define-public rust-glyph-brush-draw-cache-0.1
  (package
    (name "rust-glyph-brush-draw-cache")
    (version "0.1.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glyph_brush_draw_cache" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0446qaw3rxr3jkashdw2x9kmdqiybdbqnb0yl84rx27nj19nf430"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-ab-glyph" ,rust-ab-glyph-0.2)
                       ("rust-crossbeam-channel" ,rust-crossbeam-channel-0.5)
                       ("rust-crossbeam-deque" ,rust-crossbeam-deque-0.8)
                       ("rust-linked-hash-map" ,rust-linked-hash-map-0.5)
                       ("rust-rayon" ,rust-rayon-1)
                       ("rust-rustc-hash" ,rust-rustc-hash-1))
       #:cargo-development-inputs (("rust-approx" ,rust-approx-0.5)
                                   ("rust-criterion" ,rust-criterion-0.3)
                                   ("rust-glyph-brush-layout" ,rust-glyph-brush-layout-0.2)
                                   ("rust-once-cell" ,rust-once-cell-1))))
    (home-page "https://github.com/alexheretic/glyph-brush")
    (synopsis "Texture draw cache for ab_glyph")
    (description "Rasterization cache for ab_glyph used in glyph_brush.")
    (license license:asl2.0)))

(define-public rust-glyph-brush-layout-0.2
  (package
    (name "rust-glyph-brush-layout")
    (version "0.2.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glyph_brush_layout" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0f3scdx5kqjndnwvamfjca6dl8fs6np0jl2wd71mmjh09wrw4cnc"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-ab-glyph" ,rust-ab-glyph-0.2)
                       ("rust-approx" ,rust-approx-0.5)
                       ("rust-xi-unicode" ,rust-xi-unicode-0.3))
       #:cargo-development-inputs (("rust-once-cell" ,rust-once-cell-1)
                                   ("rust-ordered-float" ,rust-ordered-float-2.10))))
    (home-page "https://github.com/alexheretic/glyph-brush")
    (synopsis "Text layout for ab_glyph")
    (description "Text layout for ab_glyph.")
    (license license:asl2.0)))

(define-public rust-glyph-brush-layout-0.1
  (package
    (inherit rust-glyph-brush-layout-0.2)
    (name "rust-glyph-brush-layout")
    (version "0.1.9")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "glyph-brush-layout" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1ir4xi5b0s0yykxm26d7mvspa8dlqay1q91fnfv73p7if32ssw4b"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-approx" ,rust-approx-0.3)
                       ("rust-rusttype" ,rust-rusttype-0.8)
                       ("rust-xi-unicode" ,rust-xi-unicode-0.2))
       #:cargo-development-inputs (("rust-once-cell" ,rust-once-cell-1)
                                   ("rust-ordered-float" ,rust-ordered-float-1))))))

(define-public rust-gpu-alloc-0.5
  (package
    (name "rust-gpu-alloc")
    (version "0.5.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gpu-alloc" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0qkzv19k8ls20nx13qw63gfy9jc4gbxzcc50gr2h90mk57yamgi2"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-gpu-alloc-types" ,rust-gpu-alloc-types-0.2)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-tracing" ,rust-tracing-0.1))))
    (home-page "https://github.com/zakarumych/gpu-alloc")
    (synopsis "Implementation agnostic memory allocator for Vulkan like APIs")
    (description
     "This package provies an implementation agnostic memory allocator for
Vulkan like APIs")
    (license (list license:expat license:asl2.0))))

(define-public rust-gpu-allocator-0.22
  (package
    (name "rust-gpu-allocator")
    (version "0.22.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gpu-allocator" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1s13qi1yvp8gan95q7r3dxbhdnq503v5laz4zjnnyb0ww7igk5ff"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;Requries d3d12 from windows!
       #:cargo-inputs (("rust-ash" ,rust-ash-0.37)
                       ("rust-backtrace" ,rust-backtrace-0.3)
                       ("rust-imgui" ,rust-imgui-0.10)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-winapi" ,rust-winapi-0.3)
                       ("rust-windows" ,rust-windows-0.44))
       #:cargo-development-inputs (("rust-ash" ,rust-ash-0.37)
                                   ("rust-ash-window" ,rust-ash-window-0.12)
                                   ("rust-env-logger" ,rust-env-logger-0.10)
                                   ("rust-raw-window-handle" ,rust-raw-window-handle-0.5)
                                   ("rust-winapi" ,rust-winapi-0.3)
                                   ("rust-windows" ,rust-windows-0.44)
                                   ("rust-winit" ,rust-winit-0.27))))
    (inputs (list fontconfig))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/Traverse-Research/gpu-allocator")
    (synopsis "Memory allocator for GPU memory in Vulkan and DirectX 12")
    (description
     "This package provides a memory allocator for GPU memory in Vulkan and
@code{DirectX} 12")
    (license (list license:expat license:asl2.0))))

(define-public rust-gpu-allocator-0.8
  (package
    (inherit rust-gpu-allocator-0.22)
    (name "rust-gpu-allocator")
    (version "0.8.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gpu-allocator" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "16hrbidmrbvd63vmfpi359pmj2wrcsfmal42mgfzdww0w2pms4f6"))))
    (arguments
     `(#:tests? #f ;could not read example test file
       #:cargo-inputs (("rust-ash" ,rust-ash-0.32)
                       ("rust-backtrace" ,rust-backtrace-0.3)
                       ("rust-imgui" ,rust-imgui-0.7)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-thiserror" ,rust-thiserror-1))
       #:cargo-development-inputs (("rust-ash-window" ,rust-ash-window-0.6)
                                   ("rust-winit" ,rust-winit-0.25))))))

(define-public rust-gpu-alloc-types-0.2
  (package
    (name "rust-gpu-alloc-types")
    (version "0.2.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gpu-alloc-types" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "19b7zqg0va1lqcr7sj2z66cvasgg1p8imv7aninz5my9dc6lv02l"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-1))))
    (home-page "https://github.com/zakarumych/gpu-alloc")
    (synopsis "Core types of gpu-alloc crate")
    (description "Core types of gpu-alloc crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-gpu-descriptor-0.2
  (package
    (name "rust-gpu-descriptor")
    (version "0.2.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gpu-descriptor" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0ynmd3phm3kj448xyq570zjzh860157a7305cpli9nqbpbhh430b"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-gpu-descriptor-types" ,rust-gpu-descriptor-types-0.1)
                       ("rust-hashbrown" ,rust-hashbrown-0.12)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-tracing" ,rust-tracing-0.1))))
    (home-page "https://github.com/zakarumych/gpu-descriptor")
    (synopsis
     "Implementation agnostic descriptor allocator for Vulkan like APIs")
    (description
     "This package provides an implementation agnostic descriptor allocator
for Vulkan like APIs.")
    (license (list license:expat license:asl2.0))))

(define-public rust-gpu-descriptor-types-0.1
  (package
    (name "rust-gpu-descriptor-types")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gpu-descriptor-types" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "09i17z9943npl59lqy2y9h9562ri98xdxyccyvz6ilaswmvkcgin"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-1))))
    (home-page "https://github.com/zakarumych/gpu-descriptor")
    (synopsis "Core types of gpu-descriptor crate")
    (description
     "This package provides core types of @code{gpu-descriptor} crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-hassle-rs-0.9
  (package
    (name "rust-hassle-rs")
    (version "0.9.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "hassle-rs" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "11gvc8z6m8s4rfry2fsj7lh75n53yfrjr12kzi2p7336i5hiqq4h"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;Windows required!
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-com-rs" ,rust-com-rs-0.2)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-libloading" ,rust-libloading-0.7)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-widestring" ,rust-widestring-0.5)
                       ("rust-winapi" ,rust-winapi-0.3))
       #:cargo-development-inputs (("rust-rspirv" ,rust-rspirv-0.11))))
    (home-page "https://github.com/Traverse-Research/hassle-rs")
    (synopsis "HLSL compiler library")
    (description "This crate provides an FFI layer and idiomatic rust
wrappers for the new DXC hlsl compiler and validator.")
    (license license:expat)))

(define-public rust-image-0.19
  (package
    (inherit rust-image-0.24)
    (name "rust-image")
    (version "0.19.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "image" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0sksvf3i9cnl1bbvhkf080xl7bw65dijmbg8pn4h1qq4my8zgpzb"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-byteorder" ,rust-byteorder-1)
                       ("rust-gif" ,rust-gif-0.10)
                       ("rust-jpeg-decoder" ,rust-jpeg-decoder-0.1)
                       ("rust-lzw" ,rust-lzw-0.10)
                       ("rust-num-derive" ,rust-num-derive-0.2)
                       ("rust-num-iter" ,rust-num-iter-0.1)
                       ("rust-num-rational" ,rust-num-rational-0.1)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-png" ,rust-png-0.12)
                       ("rust-scoped-threadpool" ,rust-scoped-threadpool-0.1))
       #:cargo-development-inputs (("rust-glob" ,rust-glob-0.2)
                                   ("rust-num-complex" ,rust-num-complex-0.1)
                                   ("rust-quickcheck" ,rust-quickcheck-0.6))))))

(define-public rust-image-0.12
  (package
    (inherit rust-image-0.24)
    (name "rust-image")
    (version "0.12.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "image" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0n11f0j56qkghg2x3grdi48zw5x8lgqj6kisy8bgwjc2fpdicn6r"))))
    (arguments
     `(#:cargo-inputs (("rust-byteorder" ,rust-byteorder-1)
                       ("rust-enum-primitive" ,rust-enum-primitive-0.1)
                       ("rust-gif" ,rust-gif-0.9)
                       ("rust-jpeg-decoder" ,rust-jpeg-decoder-0.1)
                       ("rust-num-iter" ,rust-num-iter-0.1)
                       ("rust-num-rational" ,rust-num-rational-0.1)
                       ("rust-num-traits" ,rust-num-traits-0.1)
                       ("rust-png" ,rust-png-0.6)
                       ("rust-scoped-threadpool" ,rust-scoped-threadpool-0.1))
       #:cargo-development-inputs (("rust-glob" ,rust-glob-0.2)
                                   ("rust-num-complex" ,rust-num-complex-0.1))))))

(define-public rust-image-0.7
  (package
    (inherit rust-image-0.24)
    (name "rust-image")
    (version "0.7.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "image" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "06mx8xf25j6py0g3i3h8q8ar8m6zwl1lqk9xqmj61vx0z2ygr6s3"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-byteorder" ,rust-byteorder-0.4)
                       ("rust-enum-primitive" ,rust-enum-primitive-0.1)
                       ("rust-gif" ,rust-gif-0.7)
                       ("rust-glob" ,rust-glob-0.2)
                       ("rust-jpeg-decoder" ,rust-jpeg-decoder-0.1)
                       ("rust-num" ,rust-num-0.1)
                       ("rust-png" ,rust-png-0.4))))))

(define-public rust-image-0.4
  (package
    (inherit rust-image-0.24)
    (name "rust-image")
    (version "0.4.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "image" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1q0sxpdnrh7z03d3nf2wypmg1cmx0s484a0radaz4aypq2yq2127"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-byteorder" ,rust-byteorder-0.3)
                       ("rust-enum-primitive" ,rust-enum-primitive-0.0.1)
                       ("rust-gif" ,rust-gif-0.5)
                       ("rust-glob" ,rust-glob-0.2)
                       ("rust-num" ,rust-num-0.1)
                       ("rust-png" ,rust-png-0.3))))))

(define-public rust-imgui-0.10
  (package
    (name "rust-imgui")
    (version "0.10.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "imgui" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1g11074q3s23igmhqfdgs1mb54lpvn7gdab38scrqz94j351ksic"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-cfg-if" ,rust-cfg-if-1)
                       ("rust-imgui-sys" ,rust-imgui-sys-0.10)
                       ("rust-mint" ,rust-mint-0.5)
                       ("rust-parking-lot" ,rust-parking-lot-0.12))
       #:cargo-development-inputs (("rust-memoffset" ,rust-memoffset-0.6))))
    (home-page "https://github.com/imgui-rs/imgui-rs")
    (synopsis "High-level Rust bindings to dear imgui")
    (description
     "This package provides high-level Rust bindings to dear imgui.")
    (license (list license:expat license:asl2.0))))

(define-public rust-imgui-0.7
  (package
    (inherit rust-imgui-0.10)
    (name "rust-imgui")
    (version "0.7.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "imgui" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1ci2wz04j71b6yp5y0ar21jhcw11rvqwldynqlhn72166dpczkr4"))))
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-imgui-sys" ,rust-imgui-sys-0.7)
                       ("rust-parking-lot" ,rust-parking-lot-0.11))
       #:cargo-development-inputs (("rust-memoffset" ,rust-memoffset-0.6))))))

(define-public rust-imgui-glium-renderer-0.10
  (package
    (name "rust-imgui-glium-renderer")
    (version "0.10.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "imgui-glium-renderer" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1b7mg8pmgxf3nczscmrqkwp0gd5awkzpx837f6pja8ghv4yqh99y"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;Unmet requirement in glutin
       #:cargo-inputs (("rust-glium" ,rust-glium-0.32)
                       ("rust-imgui" ,rust-imgui-0.10))))
    (home-page "https://github.com/imgui-rs/imgui-rs")
    (synopsis "Glium renderer for the imgui crate")
    (description "This package provides a glium renderer for the imgui crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-imgui-sys-0.10
  (package
    (name "rust-imgui-sys")
    (version "0.10.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "imgui-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1nqa4msiqip1vs8ciw4avlq6xzmk328571zrcnqr7hmq8fzylb31"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-cfg-if" ,rust-cfg-if-1)
                       ("rust-chlorine" ,rust-chlorine-1)
                       ("rust-mint" ,rust-mint-0.5)
                       ("rust-pkg-config" ,rust-pkg-config-0.3)
                       ("rust-vcpkg" ,rust-vcpkg-0.2))))
    (home-page "https://github.com/imgui-rs/imgui-rs")
    (synopsis "Raw FFI bindings to dear imgui")
    (description "This package provides Raw FFI bindings to dear imgui.")
    (license (list license:expat license:asl2.0))))

(define-public rust-imgui-sys-0.7
  (package
    (inherit rust-imgui-sys-0.10)
    (name "rust-imgui-sys")
    (version "0.7.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "imgui-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0c6kv0299hliqhyjyg7vs08wrpq8kpqqqiligssh5gvqdfz01jl5"))))
    (arguments
     `(#:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-chlorine" ,rust-chlorine-1))))))

(define-public rust-imgui-winit-support-0.10
  (package
    (name "rust-imgui-winit-support")
    (version "0.10.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "imgui-winit-support" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0fjgb3xkxmdbzxfa9gjbfnl8mhaqppgvagpiis1jm1dmj8h5zpjz"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t ;Please select a feature roadblock. (x11 or wayland) (FIX)
       #:cargo-inputs (("rust-imgui" ,rust-imgui-0.10)
                       ("rust-winit" ,rust-winit-0.27))))
    (home-page "https://github.com/imgui-rs/imgui-rs")
    (synopsis "winit support code for the imgui crate")
    (description
     "This package provides winit support code for the imgui crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-keyframe-1
  (package
    (name "rust-keyframe")
    (version "1.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "keyframe" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1afr5ffns3k79xaqnw6rw3qn8sngwly6gxfnjn8d060mk3vqnw30"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;Ambiguous crate reference in tests
       #:cargo-inputs (("rust-mint" ,rust-mint-0.5)
                       ("rust-num-traits" ,rust-num-traits-0.2))
       #:cargo-development-inputs (("rust-ggez" ,rust-ggez-0.7)
                                   ("rust-num-derive" ,rust-num-derive-0.3))))
    (inputs (list alsa-lib eudev))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/hannesmann/keyframe")
    (synopsis "Simple library for animation in Rust")
    (description
     "The animation library includes easing functions such as user-defined
Bézier curves, keyframable curves, animation sequences and mint
integration for 2D, 3D, and 4D support with points, rectangles,
colors, etc.")
    (license license:expat)))

(define-public rust-keyframe-derive-1
  (package
    (name "rust-keyframe-derive")
    (version "1.0.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "keyframe-derive" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "047nk1kciky3ykdxgs0qgrrvq5zb7yagqdkkk9jlr45r9lfijz9p"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/HannesMann/keyframe")
    (synopsis "Implements #[derive(CanTween)] for keyframe")
    (description
     "This package implements @code{#[derive(CanTween)]} for keyframe.")
    (license license:expat)))

(define-public rust-khronos-egl-4
  (package
    (name "rust-khronos-egl")
    (version "4.1.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "khronos-egl" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1qqm42ix278w72snhss8sw8kq4y8c11z4h4xrdqvikhb3nym48wc"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;Requires bad imports
       #:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-libloading" ,rust-libloading-0.7)
                       ("rust-pkg-config" ,rust-pkg-config-0.3))
       #:cargo-development-inputs (("rust-gl" ,rust-gl-0.14)
                                   ("rust-wayland-client" ,rust-wayland-client-0.28)
                                   ("rust-wayland-egl" ,rust-wayland-egl-0.28)
                                   ("rust-wayland-protocols" ,rust-wayland-protocols-0.28))))
    (inputs (list wayland))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/timothee-haudebourg/khronos-egl")
    (synopsis "Rust bindings for EGL")
    (description "This package provides Rust bindings for EGL.")
    (license (list license:expat license:asl2.0))))

(define-public rust-legion-0.4
  (package
    (name "rust-legion")
    (version "0.4.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "legion" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "07wvkmnjb2kh8fbr406hw2pn69z53r387inisqmsp9ch8sxm7zbb"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-atomic-refcell" ,rust-atomic-refcell-0.1)
                       ("rust-bit-set" ,rust-bit-set-0.5)
                       ("rust-crossbeam-channel" ,rust-crossbeam-channel-0.5)
                       ("rust-downcast-rs" ,rust-downcast-rs-1)
                       ("rust-erased-serde" ,rust-erased-serde-0.3)
                       ("rust-itertools" ,rust-itertools-0.10)
                       ("rust-legion-codegen" ,rust-legion-codegen-0.4)
                       ("rust-parking-lot" ,rust-parking-lot-0.11)
                       ("rust-paste" ,rust-paste-1)
                       ("rust-rayon" ,rust-rayon-1)
                       ("rust-scoped-tls-hkt" ,rust-scoped-tls-hkt-0.1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-smallvec" ,rust-smallvec-1)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-uuid" ,rust-uuid-0.8))
       #:cargo-development-inputs (("rust-bincode" ,rust-bincode-1)
                                   ("rust-cgmath" ,rust-cgmath-0.18)
                                   ("rust-criterion" ,rust-criterion-0.3)
                                   ("rust-serde-json" ,rust-serde-json-1)
                                   ("rust-trybuild" ,rust-trybuild-1))))
    (home-page "https://github.com/amethyst/legion")
    (synopsis "High performance entity component system (ECS) library")
    (description
     "This package provides a high performance entity component system (ECS)
library.")
    (license license:expat)))

(define-public rust-legion-codegen-0.4
  (package
    (name "rust-legion-codegen")
    (version "0.4.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "legion-codegen" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "06xfhwaqdmd5qwng5ghnd0ycnd9ilxgxjd8304i2ayqx6rxavmca"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-1)
                       ("rust-thiserror" ,rust-thiserror-1))))
    (home-page "https://github.com/TomGillen/legion")
    (synopsis "#[system] proc macro for the legion library")
    (description
     "This package provides a @code{#[system]} proc macro for the legion
library.")
    (license license:expat)))

(define-public rust-lyon-0.17
  (package
    (name "rust-lyon")
    (version "0.17.10")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "lyon" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1fscdqi51ggkmhgbn956l7yppn4jrbsiw1mj7l7vhbrybvni01fg"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-lyon-algorithms" ,rust-lyon-algorithms-0.17)
                       ("rust-lyon-tessellation" ,rust-lyon-tessellation-0.17)
                       ("rust-lyon-extra" ,rust-lyon-extra-0.17)
                       ("rust-lyon-svg" ,rust-lyon-svg-0.17)
                       ("rust-lyon-tess2" ,rust-lyon-tess2-0.17))))
    (home-page "https://github.com/nical/lyon")
    (synopsis "2D Graphics rendering on the GPU using tessellation")
    (description
     "This crate provides 2D Graphics rendering on the GPU using tessellation.")
    (license (list license:expat license:asl2.0))))

(define-public rust-lyon-algorithms-0.17
  (package
    (name "rust-lyon-algorithms")
    (version "0.17.7")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "lyon_algorithms" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0ji4i9698kdqb02rhm8dk5rsz2q6z1lh0p707m6xi80vahbgfdw0"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-lyon-path" ,rust-lyon-path-0.17)
                       ("rust-sid" ,rust-sid-0.6)
                       ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/nical/lyon")
    (synopsis "2D Path manipulation/transformation algorithms.")
    (description
     "This crate provides 2D Path manipulation/transformation algorithms.")
    (license (list license:expat license:asl2.0))))

(define-public rust-lyon-extra-0.17
  (package
    (name "rust-lyon-extra")
    (version "0.17.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "lyon_extra" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "10n8h48a363qa66byqfkg164542p97v5rrnb33gqhggah739nwm0"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-lyon-path" ,rust-lyon-path-0.17)
                       ("rust-lyon-svg" ,rust-lyon-svg-0.17))))
    (home-page "https://github.com/nical/lyon")
    (synopsis "Various optional utilities for the lyon crate.")
    (description
     "This crate provides various optional utilities for the lyon crate.")
    (license (list license:expat license:asl2.0))))

(define-public rust-lyon-geom-1
  (package
    (name "rust-lyon-geom")
    (version "1.0.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "lyon-geom" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1bg87w2281nf8hflm62q5fbk2zda7fh3g5b923mq4whll3q1zpvl"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-arrayvec" ,rust-arrayvec-0.7)
                       ("rust-euclid" ,rust-euclid-0.22)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/nical/lyon")
    (synopsis
     "2D quadratic and cubic bézier arcs and line segment math on top of euclid")
    (description
     "lyon_geom can be used as a standalone crate or as part of @code{lyon}
via the @code{lyon::geom} module.")
    (license (list license:expat license:asl2.0))))

(define-public rust-lyon-geom-0.12
  (package
    (inherit rust-lyon-geom-1)
    (name "rust-lyon-geom")
    (version "0.12.7")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "lyon-geom" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0q4bahfivc11jzwyh4vs6cjwq4y1dl4plfilqaf9mgj33lgvzfdx"))))
    (arguments
     `(#:cargo-inputs (("rust-arrayvec" ,rust-arrayvec-0.4)
                       ("rust-euclid" ,rust-euclid-0.19)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-serde" ,rust-serde-1))))))

(define-public rust-lyon-path-0.12
  (package
    (inherit rust-lyon-path-0.17)
    (name "rust-lyon-path")
    (version "0.12.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "lyon-path" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0hs8aja19v9ga2mb9sh1d9s6gc3v0gzgf0mnc0cs3k5p8q3qxp79"))))
    (arguments
     `(#:cargo-inputs (("rust-lyon-geom" ,rust-lyon-geom-0.12)
                       ("rust-serde" ,rust-serde-1))))))

(define-public rust-lyon-tess2-0.17
  (package
    (name "rust-lyon-tess2")
    (version "0.17.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "lyon_tess2" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0m7j2qz2q9sgs367jxh1gq188k9h815pfkn1w9nwwcm8wn1iw8mz"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-lyon-tessellation" ,rust-lyon-tessellation-0.17)
                       ("rust-tess2-sys" ,rust-tess2-sys-0.0.1)
                       ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/nical/lyon")
    (synopsis "An additional path tessellator for lyon using libtess2.")
    (description
     "This crate provides an additional path tessellator for lyon using
libtess2.")
    (license (list license:expat license:asl2.0))))

(define-public rust-lyon-tessellation-0.17
  (package
    (name "rust-lyon-tessellation")
    (version "0.17.10")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "lyon_tessellation" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0q2pccjszda26asylnvhwsil8wvsprfjazrqdzj4i033s26y0c3j"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-float-next-after" ,rust-float-next-after-0.1)
                       ("rust-lyon-path" ,rust-lyon-path-0.17)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-lyon-extra" ,rust-lyon-extra-0.17))))
    (home-page "https://github.com/nical/lyon")
    (synopsis "2D Path manipulation/transformation algorithms.")
    (description
     "This crate provides 2D Path manipulation/transformation algorithms.")
    (license (list license:expat license:asl2.0))))

(define-public rust-metal-0.24
  (package
    (name "rust-metal")
    (version "0.24.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "metal" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0q6h0a20003db7h77xbv37dwwpc2wx6lsfvs08nli0b73xfka4fy"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t ;Metal is Apple dependent.
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-block" ,rust-block-0.1)
                       ("rust-core-graphics-types" ,rust-core-graphics-types-0.1)
                       ("rust-dispatch" ,rust-dispatch-0.2)
                       ("rust-foreign-types" ,rust-foreign-types-0.3)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-objc" ,rust-objc-0.2))
       #:cargo-development-inputs (("rust-cocoa" ,rust-cocoa-0.24)
                                   ("rust-cty" ,rust-cty-0.2)
                                   ("rust-png" ,rust-png-0.16)
                                   ("rust-sema" ,rust-sema-0.1)
                                   ("rust-winit" ,rust-winit-0.24))))
    (home-page "https://github.com/gfx-rs/metal-rs")
    (synopsis "Rust bindings for Metal")
    (description "This package provides rust bindings for Metal")
    (license (list license:expat license:asl2.0))))

(define-public rust-metal-0.21
  (package
    (inherit rust-metal-0.24)
    (name "rust-metal")
    (version "0.21.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "metal" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0v52sv6m0k41a7kqqwihq3pagwjv0npmscqzv73j9pha8qcxg625"))))
    (arguments
     `(#:skip-build? #t ;Metal is Apple dependent.
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-block" ,rust-block-0.1)
                       ("rust-cocoa-foundation" ,rust-cocoa-foundation-0.1)
                       ("rust-dispatch" ,rust-dispatch-0.2)
                       ("rust-foreign-types" ,rust-foreign-types-0.3)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-objc" ,rust-objc-0.2))
       #:cargo-development-inputs (("rust-cocoa" ,rust-cocoa-0.24)
                                   ("rust-cty" ,rust-cty-0.2)
                                   ("rust-png" ,rust-png-0.16)
                                   ("rust-sema" ,rust-sema-0.1)
                                   ("rust-winit" ,rust-winit-0.22))))))

(define-public rust-metal-rs-0.4
  (package
    (inherit rust-metal-0.24)
    (name "rust-metal-rs")
    (version "0.4.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "metal-rs" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1gri9pxwcwlcydaczkgssz58idhg58sahjp0mjr8y1qs6zvxasb8"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.9)
                       ("rust-block" ,rust-block-0.1)
                       ("rust-cocoa" ,rust-cocoa-0.9)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-objc" ,rust-objc-0.2)
                       ("rust-objc-foundation" ,rust-objc-foundation-0.1))
       #:cargo-development-inputs (("rust-sema" ,rust-sema-0.1)
                                   ("rust-winit" ,rust-winit-0.7))))))

(define-public rust-mikktspace-0.1
  (package
    (name "rust-mikktspace")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "mikktspace" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1aim03rs5rq0xqsz4p2ji85sq6hmhlw1hqm47x27p3rz3si1jnsw"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1))
       #:cargo-development-inputs (("rust-cgmath" ,rust-cgmath-0.15))))
    (home-page "https://github.com/gltf-rs/mikktspace")
    (synopsis "Mikkelsen tangent space algorithm")
    (description
     "This package provides the Mikkelsen tangent space algorithm.")
    (license (list license:expat license:asl2.0))))

(define-public rust-minterpolate-0.4
  (package
    (name "rust-minterpolate")
    (version "0.4.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "minterpolate" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1vll6l5z3q7nsyig1dsq1hsi2kmbfpi5yhdahgvcpyd2air1clcx"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-mint" ,rust-mint-0.5)
                       ("rust-num" ,rust-num-0.4)
                       ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/rustgd/minterpolate")
    (synopsis "Data set interpolation for mint primitives and raw arrays")
    (description
     "This package provides data set interpolation for mint primitives and
raw arrays.")
    (license (list license:expat license:asl2.0))))

(define-public rust-mp4parse-0.8
  (package
    (name "rust-mp4parse")
    (version "0.8.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "mp4parse" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1pqsv1zm5x9nnkjrv25qv2yg6ba4dny6bsy6cfdzrdm8kwg2r54r"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;The final overflow_protection test fails
       #:cargo-inputs (("rust-bitreader" ,rust-bitreader-0.3)
                       ("rust-byteorder" ,rust-byteorder-1)
                       ("rust-num-traits" ,rust-num-traits-0.1)
                       ("rust-abort-on-panic" ,rust-abort-on-panic-1)
                       ("rust-afl" ,rust-afl-0.1)
                       ("rust-afl-plugin" ,rust-afl-plugin-0.1))
       #:cargo-development-inputs (("rust-test-assembler" ,rust-test-assembler-0.1))))
    (home-page "https://github.com/mozilla/mp4parse-rust")
    (synopsis "Parser for ISO base media file format (mp4)")
    (description "Parser for ISO base media file format (mp4)")
    (license license:mpl2.0)))

(define-public rust-naga-0.11
  (package
    (name "rust-naga")
    (version "0.11.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "naga" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0w9jhnd2ikwdd1apmsrs12l4wpxpmzhzs0kp14hh3mdppill4gbc"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-arbitrary" ,rust-arbitrary-1)
                       ("rust-bit-set" ,rust-bit-set-0.5)
                       ("rust-bitflags" ,rust-bitflags-1)
                       ("rust-codespan-reporting" ,rust-codespan-reporting-0.11)
                       ("rust-hexf-parse" ,rust-hexf-parse-0.2)
                       ("rust-indexmap" ,rust-indexmap-1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-petgraph" ,rust-petgraph-0.6)
                       ("rust-pp-rs" ,rust-pp-rs-0.2)
                       ("rust-rustc-hash" ,rust-rustc-hash-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-spirv" ,rust-spirv-0.2)
                       ("rust-termcolor" ,rust-termcolor-1)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-unicode-xid" ,rust-unicode-xid-0.2))
       #:cargo-development-inputs (("rust-bincode" ,rust-bincode-1)
                                   ("rust-criterion" ,rust-criterion-0.3)
                                   ("rust-diff" ,rust-diff-0.1)
                                   ("rust-env-logger" ,rust-env-logger-0.9)
                                   ("rust-ron" ,rust-ron-0.7)
                                   ("rust-rspirv" ,rust-rspirv-0.11)
                                   ("rust-serde" ,rust-serde-1)
                                   ("rust-spirv" ,rust-spirv-0.2))))
    (home-page "https://github.com/gfx-rs/naga")
    (synopsis "Shader translation infrastructure")
    (description "The shader translation library for the needs of wgpu.")
    (license (list license:expat license:asl2.0))))

(define-public rust-noise-0.7
  (package
    (name "rust-noise")
    (version "0.7.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "noise" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0hsbw9gpsz8w9msvyvddygagd9wj93hqpg5pxz388laxfkb1s1c2"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-image" ,rust-image-0.23)
                       ("rust-rand" ,rust-rand-0.7)
                       ("rust-rand-xorshift" ,rust-rand-xorshift-0.2))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.3))))
    (home-page "https://github.com/razaekel/noise-rs")
    (synopsis "Procedural noise generation library")
    (description
     "This package provides a procedural noise generation library.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-old-school-gfx-glutin-ext-0.27
  (package
    (name "rust-old-school-gfx-glutin-ext")
    (version "0.27.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "old_school_gfx_glutin_ext" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1cs6027m3laf2kvffm5xrjb3qxdp76zwrw0wbaymhcajhp8bs9hh"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-gfx-core" ,rust-gfx-core-0.9)
                       ("rust-gfx-device-gl" ,rust-gfx-device-gl-0.16)
                       ("rust-glutin" ,rust-glutin-0.27)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-gfx" ,rust-gfx-0.18))))
    (home-page "https://github.com/alexheretic/old-school-gfx-glutin-ext")
    (synopsis "Extensions for glutin to initialize & update old school gfx")
    (description
     "Extensions for glutin to initialize & update old school gfx. An
alternative to gfx_window_glutin.")
    (license license:asl2.0)))

(define-public rust-osmesa-sys-0.0.6
  (package
    (inherit rust-osmesa-sys-0.1)
    (name "rust-osmesa-sys")
    (version "0.0.6")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "osmesa-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0fgvw9pshzr0i174qc4lj112fnys6xi442n78palxgjsr9b5hna6"))))
    (arguments
     `(#:cargo-inputs (("rust-libc" ,rust-libc-0.1)
                       ("rust-shared-library" ,rust-shared-library-0.1))))))

(define-public rust-osmesa-sys-0.0.5
  (package
    (inherit rust-osmesa-sys-0.1)
    (name "rust-osmesa-sys")
    (version "0.0.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "osmesa-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "179nqpaxaz1x48gs365v5cmwm76849n2rpw0q92ms9gsf26jsp72"))))
    (arguments
     `(#:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-shared-library" ,rust-shared-library-0.1))))))

(define-public rust-png-0.6
  (package
    (inherit rust-png-0.17)
    (name "rust-png")
    (version "0.6.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "png" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "10icb27dh1jm7qdx8jcldbhapp1w7rwcydcrrrlbbvaplplp7drw"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.7)
                       ("rust-deflate" ,rust-deflate-0.7)
                       ("rust-inflate" ,rust-inflate-0.1)
                       ("rust-num-iter" ,rust-num-iter-0.1))
       #:cargo-development-inputs (("rust-getopts" ,rust-getopts-0.2)
                                   ("rust-glium" ,rust-glium-0.14)
                                   ("rust-glob" ,rust-glob-0.2)
                                   ("rust-term" ,rust-term-0.4))))))

(define-public rust-png-0.4
  (package
    (inherit rust-png-0.17)
    (name "rust-png")
    (version "0.4.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "png" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0jknxh2i0cpi3chj890v5m5dpfjc3hlhx6cc15srp0a6c38491k6"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.3)
                       ("rust-flate2" ,rust-flate2-0.2)
                       ("rust-inflate" ,rust-inflate-0.1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-num-iter" ,rust-num-iter-0.1))
       #:cargo-development-inputs (("rust-getopts" ,rust-getopts-0.2)
                                   ("rust-glium" ,rust-glium-0.13)
                                   ("rust-glob" ,rust-glob-0.2)
                                   ("rust-term" ,rust-term-0.2))))))

(define-public rust-png-0.3
  (package
    (inherit rust-png-0.17)
    (name "rust-png")
    (version "0.3.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "png" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1lgx1qpbyk6yqci2fl587d9pdk9qyvxhvb2m2nm8im3xpdrgpdnw"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-2)
                       ("rust-flate2" ,rust-flate2-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-num" ,rust-num-0.4))
       #:cargo-development-inputs (("rust-getopts" ,rust-getopts-0.2)
                                   ("rust-glium" ,rust-glium-0.31)
                                   ("rust-glob" ,rust-glob-0.3)
                                   ("rust-term" ,rust-term-0.2))))))

(define-public rust-pp-rs-0.2
  (package
    (name "rust-pp-rs")
    (version "0.2.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "pp-rs" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1vkd9lgwf5rxy7qgzl8mka7vnghaq6nnn0nmg7mycl72ysvqnidv"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-unicode-xid" ,rust-unicode-xid-0.2))))
    (home-page "https://github.com/Kangz/glslpp-rs")
    (synopsis "Shader preprocessor")
    (description "A shader preprocessor and lexer in Rust.")
    (license license:bsd-3)))

(define-public rust-profiling-1
  (package
    (name "rust-profiling")
    (version "1.0.11")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "profiling" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "16qky51f83gks48rf4y4zafhng9c5q0cq9l8r1c9g36rb44zz7gq"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-optick" ,rust-optick-1)
                       ("rust-profiling-procmacros" ,rust-profiling-procmacros-1)
                       ("rust-puffin" ,rust-puffin-0.16)
                       ("rust-superluminal-perf" ,rust-superluminal-perf-0.1)
                       ("rust-tracing" ,rust-tracing-0.1)
                       ("rust-tracy-client" ,rust-tracy-client-0.16))
       #:cargo-development-inputs (("rust-bincode" ,rust-bincode-1)
                                   ("rust-env-logger" ,rust-env-logger-0.6)
                                   ("rust-glam" ,rust-glam-0.8)
                                   ("rust-imgui" ,rust-imgui-0.10)
                                   ("rust-imgui-winit-support" ,rust-imgui-winit-support-0.10)
                                   ("rust-lazy-static" ,rust-lazy-static-1)
                                   ("rust-log" ,rust-log-0.4)
                                   ("rust-puffin-imgui" ,rust-puffin-imgui-0.22)
                                   ("rust-rafx" ,rust-rafx-0.0.14)
                                   ("rust-tracing-subscriber" ,rust-tracing-subscriber-0.2)
                                   ("rust-tracing-tracy" ,rust-tracing-tracy-0.4)
                                   ("rust-winit" ,rust-winit-0.27))))
    (home-page "https://github.com/aclysma/profiling")
    (synopsis "Very thin abstraction over other profiler crates")
    (description
     "This crate provides a very thin abstraction over other profiler
crates.")
    (license (list license:expat license:asl2.0))))

(define-public rust-profiling-procmacros-1
  (package
    (name "rust-profiling-procmacros")
    (version "1.0.11")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "profiling-procmacros" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0gylp366pw3v56ccm9q7cz9q9z2snsgif8kl94kq1zmrnr2nl5gb"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-2))))
    (home-page "https://github.com/aclysma/profiling")
    (synopsis "Very thin abstraction over other profiler crates")
    (description
     "Provides a very thin abstraction over instrumented profiling crates
like @code{puffin}, @code{optick}, @code{tracy}, and
@code{superluminal-perf}.")
    (license (list license:expat license:asl2.0))))

(define-public rust-puffin-imgui-0.22
  (package
    (name "rust-puffin-imgui")
    (version "0.22.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "puffin-imgui" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1f7byw16rhgni7vkdc6kz7bbpcswlj5pnil8b6ffyyl8c2s7562x"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-imgui" ,rust-imgui-0.10)
                       ("rust-mint" ,rust-mint-0.5)
                       ("rust-natord" ,rust-natord-1)
                       ("rust-puffin" ,rust-puffin-0.16)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-glium" ,rust-glium-0.32)
                                   ("rust-imgui-glium-renderer" ,rust-imgui-glium-renderer-0.10)
                                   ("rust-imgui-winit-support" ,rust-imgui-winit-support-0.10))))
    (inputs (list expat freetype fontconfig))
    (native-inputs (list cmake pkg-config))
    (home-page "https://github.com/EmbarkStudios/puffin")
    (synopsis "ImGui GUI bindings for the Puffin profiler")
    (description "@code{ImGui} GUI bindings for the Puffin profiler")
    (license (list license:expat license:asl2.0))))

(define-public rust-rafx-0.0.14
  (package
    (name "rust-rafx")
    (version "0.0.14")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "rafx" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0dy9rxxz6y5kcy5parb8415xi5piajqqq6r04df4kvzq6sy6jbib"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-rafx-api" ,rust-rafx-api-0.0.14)
                       ("rust-rafx-assets" ,rust-rafx-assets-0.0.14)
                       ("rust-rafx-base" ,rust-rafx-base-0.0.14)
                       ("rust-rafx-framework" ,rust-rafx-framework-0.0.14)
                       ("rust-rafx-renderer" ,rust-rafx-renderer-0.0.14)
                       ("rust-rafx-visibility" ,rust-rafx-visibility-0.0.14))
       #:cargo-development-inputs (("rust-ash" ,rust-ash-0.32)
                                   ("rust-bincode" ,rust-bincode-1)
                                   ("rust-env-logger" ,rust-env-logger-0.6)
                                   ("rust-glam" ,rust-glam-0.13)
                                   ("rust-legion" ,rust-legion-0.4)
                                   ("rust-log" ,rust-log-0.4)
                                   ("rust-profiling" ,rust-profiling-1)
                                   ("rust-sdl2" ,rust-sdl2-0.35))))
    (home-page "https://github.com/aclysma/rafx")
    (synopsis "Rendering framework built on an extensible asset pipeline")
    (description
     "This package provides a rendering framework built on an extensible
asset pipeline.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rafx-api-0.0.14
  (package
    (name "rust-rafx-api")
    (version "0.0.14")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "rafx-api" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0cghw52n1kimm9dvmxabk36209arggl3syg76284czvb4ikh31li"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-ash" ,rust-ash-0.32)
                       ("rust-ash-window" ,rust-ash-window-0.6)
                       ("rust-backtrace" ,rust-backtrace-0.3)
                       ("rust-bitflags" ,rust-bitflags-1)
                       ("rust-block" ,rust-block-0.1)
                       ("rust-cocoa" ,rust-cocoa-0.24)
                       ("rust-cocoa-foundation" ,rust-cocoa-foundation-0.1)
                       ("rust-core-foundation" ,rust-core-foundation-0.9)
                       ("rust-crossbeam-channel" ,rust-crossbeam-channel-0.5)
                       ("rust-dispatch" ,rust-dispatch-0.2)
                       ("rust-fnv" ,rust-fnv-1)
                       ("rust-foreign-types-shared" ,rust-foreign-types-shared-0.1)
                       ("rust-gpu-allocator" ,rust-gpu-allocator-0.8)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-metal" ,rust-metal-0.21)
                       ("rust-objc" ,rust-objc-0.2)
                       ("rust-objc" ,rust-objc-0.2)
                       ("rust-profiling" ,rust-profiling-1)
                       ("rust-rafx-base" ,rust-rafx-base-0.0.14)
                       ("rust-raw-window-handle" ,rust-raw-window-handle-0.3)
                       ("rust-raw-window-metal" ,rust-raw-window-metal-0.1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-bytes" ,rust-serde-bytes-0.11)
                       ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
                       ("rust-web-sys" ,rust-web-sys-0.3)
                       ("rust-winapi" ,rust-winapi-0.3)
                       ("rust-x11" ,rust-x11-2))))
    (home-page "https://github.com/aclysma/rafx")
    (synopsis "API crate for rafx")
    (description "This is the api crate for rafx.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rafx-assets-0.0.14
  (package
    (name "rust-rafx-assets")
    (version "0.0.14")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "rafx-assets" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0wqxy2fps3rv99bjgglq9g1fapw8i5r1z2a5m98yg40j84kjwhy2"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-arrayvec" ,rust-arrayvec-0.5)
                       ("rust-basis-universal" ,rust-basis-universal-0.1)
                       ("rust-bincode" ,rust-bincode-1)
                       ("rust-crossbeam-channel" ,rust-crossbeam-channel-0.5)
                       ("rust-distill" ,rust-distill-0.0)
                       ("rust-downcast-rs" ,rust-downcast-rs-1)
                       ("rust-fnv" ,rust-fnv-1)
                       ("rust-futures-lite" ,rust-futures-lite-1)
                       ("rust-image" ,rust-image-0.23)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-profiling" ,rust-profiling-1)
                       ("rust-rafx-api" ,rust-rafx-api-0.0.14)
                       ("rust-rafx-base" ,rust-rafx-base-0.0.14)
                       ("rust-rafx-framework" ,rust-rafx-framework-0.0.14)
                       ("rust-ron" ,rust-ron-0.6)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-bytes" ,rust-serde-bytes-0.11)
                       ("rust-type-uuid" ,rust-type-uuid-0.1)
                       ("rust-uuid" ,rust-uuid-0.8))
       #:cargo-development-inputs (("rust-env-logger" ,rust-env-logger-0.6))))
    (home-page "https://github.com/aclysma/rafx")
    (synopsis "Assets crate for rafx")
    (description "This is the assets crate for rafx.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rafx-base-0.0.14
  (package
    (name "rust-rafx-base")
    (version "0.0.14")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "rafx-base" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0r01k7sh4h4l6cgi885ijdxww9aacxhgpki7r123xnk9sary5hx0"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-crossbeam-channel" ,rust-crossbeam-channel-0.5)
                       ("rust-downcast-rs" ,rust-downcast-rs-1)
                       ("rust-fnv" ,rust-fnv-1)
                       ("rust-instant" ,rust-instant-0.1)
                       ("rust-log" ,rust-log-0.4))))
    (home-page "https://github.com/aclysma/rafx")
    (synopsis "Base crate for rafx")
    (description "This is the base crate for rafx.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rafx-framework-0.0.14
  (package
    (name "rust-rafx-framework")
    (version "0.0.14")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "rafx-framework" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0ippq5z8fc6dlskzsvbw5ck2sdvag8nvgv3x39zv5k2b1wxqvkss"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-arrayvec" ,rust-arrayvec-0.5)
                       ("rust-bitflags" ,rust-bitflags-1)
                       ("rust-crossbeam-channel" ,rust-crossbeam-channel-0.5)
                       ("rust-downcast-rs" ,rust-downcast-rs-1)
                       ("rust-fnv" ,rust-fnv-1)
                       ("rust-glam" ,rust-glam-0.13)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-parking-lot" ,rust-parking-lot-0.11)
                       ("rust-profiling" ,rust-profiling-1)
                       ("rust-rafx-api" ,rust-rafx-api-0.0.14)
                       ("rust-rafx-base" ,rust-rafx-base-0.0.14)
                       ("rust-rafx-visibility" ,rust-rafx-visibility-0.0.14)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-bytes" ,rust-serde-bytes-0.11)
                       ("rust-slotmap" ,rust-slotmap-1))))
    (home-page "https://github.com/aclysma/rafx")
    (synopsis "Framework crate for rafx")
    (description "This is the framework crate for rafx.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rafx-renderer-0.0.14
  (package
    (name "rust-rafx-renderer")
    (version "0.0.14")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "rafx-renderer" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1nprgvcj2hjfm2ayrr5vwddn6v6yy5iyrfdvsgl7kyiza3kc9f5r"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-crossbeam-channel" ,rust-crossbeam-channel-0.5)
                       ("rust-fnv" ,rust-fnv-1)
                       ("rust-glam" ,rust-glam-0.13)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-profiling" ,rust-profiling-1)
                       ("rust-rafx-api" ,rust-rafx-api-0.0.14)
                       ("rust-rafx-assets" ,rust-rafx-assets-0.0.14)
                       ("rust-rafx-base" ,rust-rafx-base-0.0.14)
                       ("rust-rafx-framework" ,rust-rafx-framework-0.0.14)
                       ("rust-rafx-visibility" ,rust-rafx-visibility-0.0.14))))
    (home-page "https://github.com/aclysma/rafx")
    (synopsis "Renderer crate for rafx")
    (description "This is the renderer crate for rafx.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rafx-visibility-0.0.14
  (package
    (name "rust-rafx-visibility")
    (version "0.0.14")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "rafx-visibility" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0a1j4bgk63rxnv4z7jqnm4fi1h6p220b08qrdz7blyda3kbm9ahw"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-crossbeam-channel" ,rust-crossbeam-channel-0.5)
                       ("rust-glam" ,rust-glam-0.13)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-parking-lot" ,rust-parking-lot-0.11)
                       ("rust-profiling" ,rust-profiling-1)
                       ("rust-rustc-hash" ,rust-rustc-hash-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-slotmap" ,rust-slotmap-1))))
    (home-page "https://github.com/aclysma/rafx")
    (synopsis "Visibility crate for rafx")
    (description "This is the visibility crate for rafx.")
    (license (list license:expat license:asl2.0))))

(define-public rust-raqote-0.8
  (package
    (name "rust-raqote")
    (version "0.8.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "raqote" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0aq45973hnj05929mb1vppq19cpz5fql2hh8jkg5i9pf4lcdrfs8"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-euclid" ,rust-euclid-0.22)
                       ("rust-font-kit" ,rust-font-kit-0.11)
                       ("rust-lyon-geom" ,rust-lyon-geom-1)
                       ("rust-pathfinder-geometry" ,rust-pathfinder-geometry-0.5)
                       ("rust-png" ,rust-png-0.17)
                       ("rust-sw-composite" ,rust-sw-composite-0.7)
                       ("rust-typed-arena" ,rust-typed-arena-2))))
    (inputs (list fontconfig))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/jrmuizel/raqote")
    (synopsis "2D graphics library")
    (description
     "Raqote is a small, simple, fast software 2D graphics library.")
    (license license:bsd-3)))

(define-public rust-raw-window-metal-0.3
  (package
    (name "rust-raw-window-metal")
    (version "0.3.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "raw-window-metal" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1p6lxc62mp78w1j7l9r8b33lrh7qhgvzwg44vrrw57q8616hk4gd"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-cocoa" ,rust-cocoa-0.24)
                       ("rust-core-graphics" ,rust-core-graphics-0.22)
                       ("rust-objc" ,rust-objc-0.2)
                       ("rust-raw-window-handle" ,rust-raw-window-handle-0.5))))
    (home-page "https://github.com/norse-rs/raw-window-metal")
    (synopsis "Interop library between Metal and raw-window-handle")
    (description
     "This package provies an Interop library between Metal and raw-window-handle.")
    (license (list license:expat license:asl2.0))))

(define-public rust-raw-window-metal-0.1
  (package
    (inherit rust-raw-window-metal-0.3)
    (name "rust-raw-window-metal")
    (version "0.1.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "raw-window-metal" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "16r8kzmg1i43xa8lv8arb5a5p2q5ls5i8wmrg4ainzzgrp8ixlic"))))
    (arguments
     `(#:cargo-inputs (("rust-cocoa" ,rust-cocoa-0.24)
                       ("rust-core-graphics" ,rust-core-graphics-0.22)
                       ("rust-objc" ,rust-objc-0.2)
                       ("rust-raw-window-handle" ,rust-raw-window-handle-0.3))))
    (inputs (list libobjc2))))

(define-public rust-resize-0.7
  (package
    (name "rust-resize")
    (version "0.7.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "resize" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0hdd5r2m1700y6r88v5hq3q28xixrsbfhbzqz26409jyy3zvvrw7"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-fallible-collections" ,rust-fallible-collections-0.4)
                       ("rust-rgb" ,rust-rgb-0.8))
       #:cargo-development-inputs (("rust-png" ,rust-png-0.17))))
    (home-page "https://github.com/PistonDevelopers/resize")
    (synopsis "Simple image resampling library in pure Rust")
    (description
     "This package provides a simple image resampling library in pure Rust.")
    (license license:expat)))

(define-public rust-rspirv-0.11
  (package
    (name "rust-rspirv")
    (version "0.11.0+1.5.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "rspirv" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1fl3qpfqap32n6mxiwz9dgjpqvap2wsjkhv5fc9f96nab4xrj0qm"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-clippy" ,rust-clippy-0.0.153)
                       ("rust-fxhash" ,rust-fxhash-0.2)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-spirv" ,rust-spirv-0.2))
       #:cargo-development-inputs (("rust-assert-matches" ,rust-assert-matches-1))))
    (home-page "https://github.com/gfx-rs/rspirv")
    (synopsis "Rust library APIs for SPIR-V module manipulation")
    (description
     "The core crate of the rspirv project providing APIs for processing
SPIR-V modules.")
    (license license:asl2.0)))

(define-public rust-sdl2-0.31
  (package
    (inherit rust-sdl2-0.35)
    (name "rust-sdl2")
    (version "0.31.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "sdl2" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0q5s7vay0x7x53vlxg6i21hy89z9z6p71khcp49hgcjllfc2lk57"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.7)
                       ("rust-c-vec" ,rust-c-vec-1)
                       ("rust-lazy-static" ,rust-lazy-static-0.2)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-num" ,rust-num-0.1)
                       ("rust-rand" ,rust-rand-0.3)
                       ("rust-sdl2-sys" ,rust-sdl2-sys-0.31))))))

(define-public rust-sdl2-sys-0.31
  (package
    (inherit rust-sdl2-sys-0.35)
    (name "rust-sdl2-sys")
    (version "0.31.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "sdl2-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1dvs3dwixc8bl3amhid8261894z64bmyw4lnj35k0fp3lvl3qm2w"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bindgen" ,rust-bindgen-0.31)
                       ("rust-cfg-if" ,rust-cfg-if-0.1)
                       ("rust-cmake" ,rust-cmake-0.1)
                       ("rust-flate2" ,rust-flate2-0.2)
                       ("rust-pkg-config" ,rust-pkg-config-0.3)
                       ("rust-reqwest" ,rust-reqwest-0.7)
                       ("rust-tar" ,rust-tar-0.4))))))

(define-public rust-smithay-client-toolkit-0.17
  (package
    (name "rust-smithay-client-toolkit")
    (version "0.17.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "smithay-client-toolkit" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1rsxylblbhg8qjvq7q4phcqm70rm2i7sz2mqcj90frxvi4ynqiz1"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-calloop" ,rust-calloop-0.10)
                       ("rust-dlib" ,rust-dlib-0.5)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-memmap2" ,rust-memmap2-0.5)
                       ("rust-nix" ,rust-nix-0.26)
                       ("rust-pkg-config" ,rust-pkg-config-0.3)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-wayland-backend" ,rust-wayland-backend-0.1)
                       ("rust-wayland-client" ,rust-wayland-client-0.30)
                       ("rust-wayland-cursor" ,rust-wayland-cursor-0.30)
                       ("rust-wayland-protocols" ,rust-wayland-protocols-0.30)
                       ("rust-wayland-protocols-wlr" ,rust-wayland-protocols-wlr-0.1)
                       ("rust-wayland-scanner" ,rust-wayland-scanner-0.30)
                       ("rust-xkbcommon" ,rust-xkbcommon-0.5))
       #:cargo-development-inputs (("rust-bytemuck" ,rust-bytemuck-1)
                                   ("rust-env-logger" ,rust-env-logger-0.9)
                                   ("rust-font-kit" ,rust-font-kit-0.11)
                                   ("rust-image" ,rust-image-0.23)
                                   ("rust-pollster" ,rust-pollster-0.2)
                                   ("rust-raqote" ,rust-raqote-0.8)
                                   ("rust-raw-window-handle" ,rust-raw-window-handle-0.4)
                                   ("rust-wgpu" ,rust-wgpu-0.15))))
    (inputs (list libxkbcommon))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/smithay/client-toolkit")
    (synopsis "Toolkit for making client Wayland applications")
    (description "This package provides a toolkit for making client Wayland
applications.")
    (license license:expat)))

(define-public rust-smithay-client-toolkit-0.12.3
  (package
    (inherit rust-smithay-client-toolkit-0.17)
    (name "rust-smithay-client-toolkit")
    (version "0.12.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "smithay-client-toolkit" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "100bm0wk7agfk1dmfnqfjq55kn53srkyc3yq7vx9bb6ksmpwfl27"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-andrew" ,rust-andrew-0.3)
                       ("rust-bitflags" ,rust-bitflags-1)
                       ("rust-calloop" ,rust-calloop-0.6)
                       ("rust-dlib" ,rust-dlib-0.4)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-memmap2" ,rust-memmap2-0.1)
                       ("rust-nix" ,rust-nix-0.18)
                       ("rust-wayland-client" ,rust-wayland-client-0.28)
                       ("rust-wayland-cursor" ,rust-wayland-cursor-0.28)
                       ("rust-wayland-protocols" ,rust-wayland-protocols-0.28))))))

(define-public rust-specs-0.14
  (package
    (name "rust-specs")
    (version "0.14.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "specs" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1w5xyhqzqg7kqixhgh7f6cs4hq2clbn61jhylxxslf23v8x62rfy"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-crossbeam" ,rust-crossbeam-0.4)
                       ("rust-derivative" ,rust-derivative-1)
                       ("rust-fnv" ,rust-fnv-1)
                       ("rust-futures" ,rust-futures-0.1)
                       ("rust-hibitset" ,rust-hibitset-0.5)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-mopa" ,rust-mopa-0.2)
                       ("rust-nonzero-signed" ,rust-nonzero-signed-1)
                       ("rust-rayon" ,rust-rayon-1)
                       ("rust-rudy" ,rust-rudy-0.1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-shred" ,rust-shred-0.7)
                       ("rust-shred-derive" ,rust-shred-derive-0.5)
                       ("rust-shrev" ,rust-shrev-1)
                       ("rust-tuple-utils" ,rust-tuple-utils-0.2))
       #:cargo-development-inputs (("rust-cgmath" ,rust-cgmath-0.16)
                                   ("rust-criterion" ,rust-criterion-0.2)
                                   ("rust-rand" ,rust-rand-0.5)
                                   ("rust-ron" ,rust-ron-0.4)
                                   ("rust-serde-json" ,rust-serde-json-1)
                                   ("rust-specs-derive" ,rust-specs-derive-0.4))))
    (home-page "https://specs.amethyst.rs")
    (synopsis "Specs is an Entity-Component-System library written in Rust")
    (description
     "Specs is an Entity-Component System written in Rust. Unlike most other
ECS libraries out there, it providesm more parallelism and flexibility.")
    (license (list license:expat license:asl2.0))))

(define-public rust-specs-derive-0.4
  (package
    (name "rust-specs-derive")
    (version "0.4.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "specs-derive" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "049m23figxid6vmd3h28m5fqs5fk36gcs8j2xh7iklpkc29y08ry"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/slide-rs/specs/tree/master/specs-derive")
    (synopsis "Custom derive macro for Specs components")
    (description "Custom derive macro for Specs components")
    (license (list license:expat license:asl2.0))))

(define-public rust-specs-hierarchy-0.3
  (package
    (name "rust-specs-hierarchy")
    (version "0.3.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "specs-hierarchy" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0l32j4ka25q193ry5h62v19i3ba3122wpc4ss4y7b16fyi8k67cc"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-hibitset" ,rust-hibitset-0.5)
                       ("rust-shred" ,rust-shred-0.7)
                       ("rust-shred-derive" ,rust-shred-derive-0.5)
                       ("rust-shrev" ,rust-shrev-1)
                       ("rust-specs" ,rust-specs-0.14))))
    (home-page "https://github.com/rustgd/specs-hierarchy.git")
    (synopsis "Scene graph type hierarchy abstraction for use with specs")
    (description
     "This package provides scene graph type hierarchy abstraction for use
with specs.")
    (license (list license:expat license:asl2.0))))

(define-public rust-spirv-0.2
  (package
    (name "rust-spirv")
    (version "0.2.0+1.5.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "spirv" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0c7qjinqpwcfxk00qx0j46z7i31lnzg2qnnar3gz3crxzqwglsr4"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/gfx-rs/rspirv")
    (synopsis "Rust definition of SPIR-V structs and enums")
    (description "The headers crate for the rspirv project which provides Rust
definitions of SPIR-V structs, enums, and constants.")
    (license license:asl2.0)))

(define-public rust-spirv-utils-0.2
  (package
    (name "rust-spirv-utils")
    (version "0.2.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "spirv-utils" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1cxr7g3anxl4jlafcq2m1acjmaxms9q9q6apbblxy0mb75klkfw3"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-lalrpop-util" ,rust-lalrpop-util-0.11))))
    (home-page "https://github.com/Aatch/spirv-utils")
    (synopsis "SPIR-V Utilities library")
    (description "This package provides the SPIR-V utilities library.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-sw-composite-0.7
  (package
    (name "rust-sw-composite")
    (version "0.7.16")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "sw-composite" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1m3i4vdmixzcsnbffiws8jimsxdq1n3346kkmmha1bxljmwgpj4s"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/jrmuizel/sw-composite/")
    (synopsis "Collection of software compositing routines")
    (description
     "This package provides a collection of software compositing routines.")
    (license license:bsd-3)))

(define-public rust-tess2-sys-0.0.1
  (package
    (name "rust-tess2-sys")
    (version "0.0.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "tess2-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1n8nj0g7g926mrwghk9l25x2icgcw227z3v6fczawivvql579446"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-gcc" ,rust-gcc-0.3))))
    (home-page "https://github.com/semtexzv/tess2-sys")
    (synopsis "FFI definitions and build script for libtess2")
    (description
     "This crate provides an additional path tessellator for lyon using
libtess2.")
    (license license:asl2.0)))

(define-public rust-tiny-skia-0.7
  (package
    (inherit rust-tiny-skia-0.8)
    (name "rust-tiny-skia")
    (version "0.7.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "tiny-skia" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "10mzi6x57w6s0k812zww1kx3dl8yprh1q64x9dpb35dqkdb809k4"))))
    (arguments
     `(#:cargo-inputs (("rust-arrayref" ,rust-arrayref-0.3)
                       ("rust-arrayvec" ,rust-arrayvec-0.5)
                       ("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-cfg-if" ,rust-cfg-if-1)
                       ("rust-png" ,rust-png-0.17)
                       ("rust-safe-arch" ,rust-safe-arch-0.5)
                       ("rust-tiny-skia-path" ,rust-tiny-skia-path-0.7))))))

(define-public rust-tiny-skia-path-0.7
  (package
    (inherit rust-tiny-skia-path-0.8)
    (name "rust-tiny-skia-path")
    (version "0.7.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "tiny-skia-path" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0v7sf1g9avk91npxk41byswpm5msz8yh3jv7adc3vr1f1hpx6561"))))
    (arguments
     `(#:cargo-inputs (("rust-arrayref" ,rust-arrayref-0.3)
                       ("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-libm" ,rust-libm-0.2))))))

(define-public rust-vek-0.16
  (package
    (name "rust-vek")
    (version "0.16.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "vek" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0mr1ma0c9a5l9knm3d2ik45k9nj8g7h7g6nmjv2zmh9v7w25hlf8"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-approx" ,rust-approx-0.5)
                       ("rust-num-integer" ,rust-num-integer-0.1)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-az" ,rust-az-1)
                       ("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-image" ,rust-image-0.24)
                       ("rust-mint" ,rust-mint-0.5)
                       ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/yoanlcq/vek")
    (synopsis "Generic 2D-3D math swiss army knife for game engines, with SIMD
support and focus on convenience")
    (description
     "Generic 2D-3D math swiss army knife for game engines, with SIMD
support and focus on convenience.")
    (license (list license:expat license:asl2.0))))

(define-public rust-vek-0.15
  (package
    (inherit rust-vek-0.16)
    (name "rust-vek")
    (version "0.15.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "vek" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0fsw7v50y9kh57jjjb96hdp38w5qmgg08l2cn2jvm741zvv34qj0"))))
    (arguments
     `(#:cargo-inputs (("rust-approx" ,rust-approx-0.5)
                       ("rust-num-integer" ,rust-num-integer-0.1)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-az" ,rust-az-1)
                       ("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-image" ,rust-image-0.24)
                       ("rust-mint" ,rust-mint-0.5)
                       ("rust-serde" ,rust-serde-1))))))

(define-public rust-vk-0.0
  (package
    (name "rust-vk")
    (version "0.0.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "vk" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0mmw60z2daj91w4n7p5n7l584254zjjyq6qvhr9zfqd4669rygxh"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/bjz/gl-rs/vk/")
    (synopsis "Vulkan bindings (placeholder)")
    (description "This package provides Vulkan bindings.")
    (license license:asl2.0)))

(define-public rust-vk-sys-0.2
  (package
    (name "rust-vk-sys")
    (version "0.2.7")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "vk-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "19621y08k4dbq3y110ma9s5xgwkzrvf4syx9wspwsq1mx5pkxhb8"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/vulkano-rs/vulkano")
    (synopsis "Bindings for the Vulkan graphics API")
    (description "This package provides bindings for the Vulkan graphics API.")
    (license (list license:expat license:asl2.0))))

(define-public rust-wavefront-obj-5
  (package
    (name "rust-wavefront-obj")
    (version "5.1.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wavefront-obj" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1zq6jimnmjagyh0pbkbwajdx324myh8vxnhc029n9b2sbjvcpj9k"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/PistonDevelopers/wavefront_obj")
    (synopsis "A parser for the Wavefront .obj file format")
    (description
     "This package provides a parser for the Wavefront .obj file format.")
    (license license:expat)))

(define-public rust-wayland-backend-0.1
  (package
    (name "rust-wayland-backend")
    (version "0.1.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-backend" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1rnzr14l654506alzyvphlcifb8sd4macnw4c5i0ymabvm926prv"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;bad circular dependency on wayland_scanner
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-downcast-rs" ,rust-downcast-rs-1)
                       ("rust-io-lifetimes" ,rust-io-lifetimes-1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-nix" ,rust-nix-0.26)
                       ("rust-raw-window-handle" ,rust-raw-window-handle-0.5)
                       ("rust-scoped-tls" ,rust-scoped-tls-1)
                       ("rust-smallvec" ,rust-smallvec-1)
                       ("rust-wayland-sys" ,rust-wayland-sys-0.31))
       #:cargo-development-inputs (("rust-concat-idents" ,rust-concat-idents-1)
                                   ("rust-env-logger" ,rust-env-logger-0.9))))
    (home-page "https://github.com/smithay/wayland-rs")
    (synopsis "Low-level bindings to the Wayland protocol")
    (description "Low-level bindings to the Wayland protocol")
    (license license:expat)))

(define-public rust-wayland-client-0.30
  (package
    (name "rust-wayland-client")
    (version "0.30.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-client" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1j3as2g1znrs2lpkksqcvx8pag85yiwwbcv6wb3lyrqgfxa9d728"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;Circular dependency on wayland_protocols
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-calloop" ,rust-calloop-0.10)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-nix" ,rust-nix-0.26)
                       ("rust-wayland-backend" ,rust-wayland-backend-0.1)
                       ("rust-wayland-scanner" ,rust-wayland-scanner-0.30))
       #:cargo-development-inputs (("rust-futures-channel" ,rust-futures-channel-0.3)
                                   ("rust-futures-util" ,rust-futures-util-0.3)
                                   ("rust-tempfile" ,rust-tempfile-3))))
    (inputs (list rust-bitflags-1
                  rust-downcast-rs-1
                  rust-libc-0.2
                  rust-nix-0.24
                  rust-scoped-tls-1
                  rust-wayland-commons-0.29
                  rust-wayland-scanner-0.30
                  rust-wayland-sys-0.31))
    (home-page "https://github.com/smithay/wayland-rs")
    (synopsis
     "Rust bindings to the standard C implementation of the wayland protocol")
    (description
     "This package provides Rust bindings to the standard C implementation of
the wayland protocol, client side.")
    (license license:expat)))

(define-public rust-wayland-client-0.28
  (package
    (inherit rust-wayland-client-0.29)
    (name "rust-wayland-client")
    (version "0.28.6")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-client" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0m831sj4w5k0j9167f2dy3815k73g153j09271cz20p5a0ik7az3"))))
    (arguments
     `(#:tests? #f ;Can't find crates
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-downcast-rs" ,rust-downcast-rs-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-nix" ,rust-nix-0.20)
                       ("rust-scoped-tls" ,rust-scoped-tls-1)
                       ("rust-wayland-commons" ,rust-wayland-commons-0.28)
                       ("rust-wayland-scanner" ,rust-wayland-scanner-0.28)
                       ("rust-wayland-sys" ,rust-wayland-sys-0.28))
       #:cargo-development-inputs (("rust-tempfile" ,rust-tempfile-3))))))

(define-public rust-wayland-client-0.12
  (package
    (inherit rust-wayland-client-0.29)
    (name "rust-wayland-client")
    (version "0.12.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-client" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1j7vy1mvzbdm3i94c7s20nx3cvyb7l8gwi3r1n9y8zhi8gwsv41b"))))
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-token-store" ,rust-token-store-0.1)
                       ("rust-wayland-scanner" ,rust-wayland-scanner-0.12)
                       ("rust-wayland-sys" ,rust-wayland-sys-0.12))
       #:cargo-development-inputs (("rust-byteorder" ,rust-byteorder-1)
                                   ("rust-tempfile" ,rust-tempfile-2))))))

(define-public rust-wayland-client-0.9
  (package
    (inherit rust-wayland-client-0.30)
    (name "rust-wayland-client")
    (version "0.9.10")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-client" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "17asrid8w996pzxrsc28194j8s5w2xqsm809cr4smv9x1y4g444v"))))
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.9)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-wayland-scanner" ,rust-wayland-scanner-0.9)
                       ("rust-wayland-sys" ,rust-wayland-sys-0.9))
       #:cargo-development-inputs (("rust-byteorder" ,rust-byteorder-0.5)
                                   ("rust-tempfile" ,rust-tempfile-2))))))

(define-public rust-wayland-client-0.8
  (package
    (inherit rust-wayland-client-0.30)
    (name "rust-wayland-client")
    (version "0.8.7")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-client" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0ibp3ajf2g2av74c68032fc15gpqdcc6vkkkc4wvacrhwwpnxc48"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.7)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-wayland-scanner" ,rust-wayland-scanner-0.8)
                       ("rust-wayland-sys" ,rust-wayland-sys-0.8))
       #:cargo-development-inputs (("rust-byteorder" ,rust-byteorder-0.5)
                                   ("rust-tempfile" ,rust-tempfile-2))))))

(define-public rust-wayland-client-0.7
  (package
    (inherit rust-wayland-client-0.30)
    (name "rust-wayland-client")
    (version "0.7.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-client" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1d1p2j7qqxi7rk6zshk9yd1z83amf98rnsdcy7jfr5vcdj3vkcm4"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.7)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-wayland-scanner" ,rust-wayland-scanner-0.7)
                       ("rust-wayland-sys" ,rust-wayland-sys-0.7))
       #:cargo-development-inputs (("rust-byteorder" ,rust-byteorder-0.5)
                                   ("rust-tempfile" ,rust-tempfile-2))))))

(define-public rust-wayland-client-0.5
  (package
    (inherit rust-wayland-client-0.30)
    (name "rust-wayland-client")
    (version "0.5.12")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-client" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "17jmi893jr34s8w175rnljwqi7vxc8d0wls0inhc0p3v2m60klyf"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.7)
                       ("rust-crossbeam" ,rust-crossbeam-0.2)
                       ("rust-dlib" ,rust-dlib-0.3)
                       ("rust-lazy-static" ,rust-lazy-static-0.2)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-wayland-scanner" ,rust-wayland-scanner-0.5)
                       ("rust-wayland-sys" ,rust-wayland-sys-0.5))
       #:cargo-development-inputs (("rust-byteorder" ,rust-byteorder-0.5)
                                   ("rust-tempfile" ,rust-tempfile-2))))))

(define-public rust-wayland-commons-0.28
  (package
    (inherit rust-wayland-commons-0.29)
    (name "rust-wayland-commons")
    (version "0.28.6")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-commons" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1npvcrwh8chjcji73c24hlp05zbv6dxv24bylb8bn4bhgja1f652"))))
    (arguments
     `(#:cargo-inputs (("rust-nix" ,rust-nix-0.20)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-smallvec" ,rust-smallvec-1)
                       ("rust-wayland-sys" ,rust-wayland-sys-0.28))))))

(define-public rust-wayland-cursor-0.30
  (package
    (name "rust-wayland-cursor")
    (version "0.30.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-cursor" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1al7qkdr0nr8nsddcv61zhj4gw3bxp9n48s4n03qns2bbc6kl31d"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-nix" ,rust-nix-0.26)
                       ("rust-wayland-client" ,rust-wayland-client-0.30)
                       ("rust-xcursor" ,rust-xcursor-0.3))))
    (home-page "https://github.com/smithay/wayland-rs")
    (synopsis "Bindings to libwayland-cursor.")
    (description
     "This crate provides helpers to load the system provided cursor images
and load them into WlBuffers as well as obtain the necessary metadata
to properly display animated cursors.")
    (license license:expat)))

(define-public rust-wayland-protocols-0.30
  (package
    (name "rust-wayland-protocols")
    (version "0.30.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-protocols" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0kcvvli38gdjb9c7dpa2s0ix4nnqfq7n2bbc39370kx9bhg10a1v"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-wayland-backend" ,rust-wayland-backend-0.1)
                       ("rust-wayland-client" ,rust-wayland-client-0.30)
                       ("rust-wayland-scanner" ,rust-wayland-scanner-0.30)
                       ("rust-wayland-server" ,rust-wayland-server-0.30))))
    (home-page "https://github.com/smithay/wayland-rs")
    (synopsis "Generated API for the officials wayland protocol extensions")
    (description
     "This package provides a generated API for the officials Wayland
protocol extensions.")
    (license license:expat)))

(define-public rust-wayland-protocols-0.12
  (package
    (inherit rust-wayland-protocols-0.30)
    (name "rust-wayland-protocols")
    (version "0.12.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-protocols" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0dsmbq0339fb7d2ylik0ajdv9kpzmcxfmj9pni6r77f75zfl4ngv"))))
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-wayland-client" ,rust-wayland-client-0.12)
                       ("rust-wayland-scanner" ,rust-wayland-scanner-0.12)
                       ("rust-wayland-server" ,rust-wayland-server-0.12)
                       ("rust-wayland-sys" ,rust-wayland-sys-0.12))))))

(define-public rust-wayland-protocols-0.9
  (package
    (inherit rust-wayland-protocols-0.30)
    (name "rust-wayland-protocols")
    (version "0.9.10")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-protocols" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1ijaacd47x8hfvadcpihcxl4f215gg9jzdzhip7xzbxnzydmp300"))))
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.9)
                       ("rust-wayland-client" ,rust-wayland-client-0.9)
                       ("rust-wayland-scanner" ,rust-wayland-scanner-0.9)
                       ("rust-wayland-server" ,rust-wayland-server-0.9)
                       ("rust-wayland-sys" ,rust-wayland-sys-0.9))))))

(define-public rust-wayland-protocols-wlr-0.1
  (package
    (name "rust-wayland-protocols-wlr")
    (version "0.1.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-protocols-wlr" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "12jqi7n77l8a13hc5w5fkdgs4kdjk9i6nvl74njsdr106c4r3sgw"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-wayland-backend" ,rust-wayland-backend-0.1)
                       ("rust-wayland-client" ,rust-wayland-client-0.30)
                       ("rust-wayland-protocols" ,rust-wayland-protocols-0.30)
                       ("rust-wayland-scanner" ,rust-wayland-scanner-0.30)
                       ("rust-wayland-server" ,rust-wayland-server-0.30))))
    (home-page "https://github.com/smithay/wayland-rs")
    (synopsis "Generated API for the WLR wayland protocol extensions")
    (description
     "This package provides generated API for the WLR wayland protocol
extensions.")
    (license license:expat)))

(define-public rust-wayland-scanner-0.30
  (package
    (name "rust-wayland-scanner")
    (version "0.30.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-scanner" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "03ikmfwacsgbym2y4raf05knl1qjlgg81sy0174jxhzvayr77f5r"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;'Rustfmt failed'
       #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quick-xml" ,rust-quick-xml-0.28)
                       ("rust-quote" ,rust-quote-1))
       #:cargo-development-inputs (("rust-similar" ,rust-similar-2))))
    (home-page "https://github.com/smithay/wayland-rs")
    (synopsis "Generate Rust APIs from XML Wayland protocol files")
    (description
     "Wayland Scanner generates Rust APIs from XML Wayland protocol files.
It is intended for use with wayland-sys.  You should only need this crate if
you are working on custom Wayland protocol extensions.  Look at the
wayland-client crate for usable bindings.")
    (license license:expat)))

(define-public rust-wayland-scanner-0.28
  (package
    (inherit rust-wayland-scanner-0.30)
    (name "rust-wayland-scanner")
    (version "0.28.6")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-scanner" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1w839jsh7nrni4f2x5bkapf98w7kddxyqmpks4rf67dnvsr3x4nf"))))
    (arguments
     `(#:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-xml-rs" ,rust-xml-rs-0.8))))))

(define-public rust-wayland-scanner-0.12
  (package
    (inherit rust-wayland-scanner-0.30)
    (name "rust-wayland-scanner")
    (version "0.12.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-scanner" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1b885raifbi9wzjjxfb7lk8f29f31y24vrkd8cyjqvqyc9dabzyw"))))
    (arguments
     `(#:cargo-inputs (("rust-xml-rs" ,rust-xml-rs-0.7))))))

(define-public rust-wayland-scanner-0.9
  (package
    (inherit rust-wayland-scanner-0.30)
    (name "rust-wayland-scanner")
    (version "0.9.10")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-scanner" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0s979b6rz1ihk9k23d526i4l7raclawjycckg2mf8vmp68hjc838"))))
    (arguments
     `(#:cargo-inputs (("rust-xml-rs" ,rust-xml-rs-0.6))))))

(define-public rust-wayland-scanner-0.8
  (package
    (inherit rust-wayland-scanner-0.30)
    (name "rust-wayland-scanner")
    (version "0.8.7")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-scanner" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1gy05niclp9hhpkzk4b86zys98drrhkf1naimkv7ryb3xw02n09a"))))
    (arguments
     `(#:cargo-inputs (("rust-xml-rs" ,rust-xml-rs-0.3))))))

(define-public rust-wayland-scanner-0.7
  (package
    (inherit rust-wayland-scanner-0.30)
    (name "rust-wayland-scanner")
    (version "0.7.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-scanner" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1zzcpqgq04xwrshly4da2a19z3l3r82sk5h561qfqfbmdf33iz91"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-xml-rs" ,rust-xml-rs-0.3))))))

(define-public rust-wayland-scanner-0.5
  (package
    (inherit rust-wayland-scanner-0.30)
    (name "rust-wayland-scanner")
    (version "0.5.11")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-scanner" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0rjg6qs9a9j9895djjp9f2d20kizh0fm2947msmwpbvb1lvnj62s"))))
    (arguments
     `(#:cargo-inputs (("rust-xml-rs" ,rust-xml-rs-0.3))))))

(define-public rust-wayland-server-0.30
  (package
    (name "rust-wayland-server")
    (version "0.30.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-server" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0fnjhhcbnwgyplawc02v3b6nkxnhzl2981yiyzzlj7gyjs0c4hww"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-downcast-rs" ,rust-downcast-rs-1)
                       ("rust-io-lifetimes" ,rust-io-lifetimes-1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-nix" ,rust-nix-0.26)
                       ("rust-wayland-backend" ,rust-wayland-backend-0.1)
                       ("rust-wayland-scanner" ,rust-wayland-scanner-0.30))))
    (home-page "https://github.com/smithay/wayland-rs")
    (synopsis
     "Bindings to the standard C implementation of the wayland protocol, server side.")
    (description
     "This package provides Rust bindings to the standard C implementation of
the wayland protocol, server side.")
    (license license:expat)))

(define-public rust-wayland-server-0.12
  (package
    (inherit rust-wayland-server-0.30)
    (name "rust-wayland-server")
    (version "0.12.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-server" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "17i7yshcqa6hbq4a5y3baf7ahn85dyxjhfy4yyfcsxfhpmbx5yn7"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-nix" ,rust-nix-0.9)
                       ("rust-token-store" ,rust-token-store-0.1)
                       ("rust-wayland-scanner" ,rust-wayland-scanner-0.12)
                       ("rust-wayland-sys" ,rust-wayland-sys-0.12))))))

(define-public rust-wayland-server-0.9
  (package
    (inherit rust-wayland-server-0.30)
    (name "rust-wayland-server")
    (version "0.9.10")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-server" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1w9dzxafh729r7r2wmal2ib1j41m5kqdvvhz1fvi25ay7i3lvm8v"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.9)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-nix" ,rust-nix-0.8)
                       ("rust-wayland-scanner" ,rust-wayland-scanner-0.9)
                       ("rust-wayland-sys" ,rust-wayland-sys-0.9))))))

(define-public rust-wayland-sys-0.31
  (package
    (name "rust-wayland-sys")
    (version "0.31.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1bxpwamgagpxa8p9m798gd3g6rwj2m4sbdvc49zx05jjzzmci80m"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-dlib" ,rust-dlib-0.5)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-memoffset" ,rust-memoffset-0.9)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-pkg-config" ,rust-pkg-config-0.3))
       #:phases (modify-phases %standard-phases
                  (add-after 'unpack 'patch-libraries
                    (lambda* (#:key inputs #:allow-other-keys)
                      (let ((libwayland (dirname (search-input-file inputs
                                                  "lib/libwayland-client.so"))))
                        (substitute* (find-files "src" "\\.rs$")
                          (("libwayland.*\\.so" shared-lib)
                           (string-append libwayland "/" shared-lib)))))))))
    (propagated-inputs (list wayland))
    (home-page "https://github.com/smithay/wayland-rs")
    (synopsis "FFI bindings to the various @file{libwayland-*.so} libraries")
    (description
     "This package provides FFI bindings to the various
@file{libwayland-*.so} libraries.  You should only need this crate if
you are working on custom Wayland protocol extensions.  Look at the
crate @code{rust-wayland-client} for usable bindings.")
    (license license:expat)))

(define-public rust-wayland-sys-0.28
  (package
    (inherit rust-wayland-sys-0.30)
    (name "rust-wayland-sys")
    (version "0.28.6")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1f7yy3c6h270xd4wk2nsrr9433gmkg29d5rfxndvzznpmslzqhfq"))))
    (arguments
     `(#:cargo-inputs (("rust-dlib" ,rust-dlib-0.5)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-memoffset" ,rust-memoffset-0.6)
                       ("rust-pkg-config" ,rust-pkg-config-0.3))
       #:phases (modify-phases %standard-phases
                  (add-after 'unpack 'patch-libraries
                    (lambda* (#:key inputs #:allow-other-keys)
                      (let ((libwayland (dirname (search-input-file inputs
                                                  "lib/libwayland-client.so"))))
                        (substitute* (find-files "src" "\\.rs$")
                          (("libwayland.*\\.so" shared-lib)
                           (string-append libwayland "/" shared-lib)))))))))))

(define-public rust-wayland-sys-0.12
  (package
    (inherit rust-wayland-sys-0.30)
    (name "rust-wayland-sys")
    (version "0.12.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1cqd7flbkivcqfd09h6axsbybrk6jv5yh2m13j03wiiw0s1jyyip"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-dlib" ,rust-dlib-0.4)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-libc" ,rust-libc-0.2))))))

(define-public rust-wayland-sys-0.9
  (package
    (inherit rust-wayland-sys-0.30)
    (name "rust-wayland-sys")
    (version "0.9.10")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0ivw4iy04lx5gir4hxid8f4lnwhf5a6i8jawiapai2cjpnfwlcxl"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-dlib" ,rust-dlib-0.3)
                       ("rust-lazy-static" ,rust-lazy-static-0.2)
                       ("rust-libc" ,rust-libc-0.2))))))

(define-public rust-wayland-sys-0.8
  (package
    (inherit rust-wayland-sys-0.30)
    (name "rust-wayland-sys")
    (version "0.8.7")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1da1sdak5afjj4zrwbvpncj0k6jb4vyflw9mfmbjx7ja3gn0pkwy"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-dlib" ,rust-dlib-0.3)
                       ("rust-lazy-static" ,rust-lazy-static-0.2)
                       ("rust-libc" ,rust-libc-0.2))))))

(define-public rust-wayland-sys-0.7
  (package
    (inherit rust-wayland-sys-0.30)
    (name "rust-wayland-sys")
    (version "0.7.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "17y5ik5bb7gi84ndl8cpjlwdhrcjy3q5ib0bkb2cjgfs9785fhk0"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-dlib" ,rust-dlib-0.3)
                       ("rust-lazy-static" ,rust-lazy-static-0.2)
                       ("rust-libc" ,rust-libc-0.2))))))

(define-public rust-wayland-sys-0.5
  (package
    (inherit rust-wayland-sys-0.30)
    (name "rust-wayland-sys")
    (version "0.5.11")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0fl01v8wghplps6ba23zryz89dgidfvz3sl2bwhl8rg5bpzgfcwn"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-dlib" ,rust-dlib-0.3)
                       ("rust-lazy-static" ,rust-lazy-static-0.1)
                       ("rust-libc" ,rust-libc-0.2))))))

(define-public rust-wayland-window-0.13
  (package
    (name "rust-wayland-window")
    (version "0.13.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-window" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1ca1q7arm5ryf3d7fqn3nsf493s5p6dgpcihdb89rpphhhg47gz5"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-memmap" ,rust-memmap-0.6)
                       ("rust-tempfile" ,rust-tempfile-3)
                       ("rust-wayland-client" ,rust-wayland-client-0.12)
                       ("rust-wayland-protocols" ,rust-wayland-protocols-0.12))
       #:cargo-development-inputs (("rust-byteorder" ,rust-byteorder-1)
                                   ("rust-wayland-client" ,rust-wayland-client-0.12))))
    (home-page "https://github.com/Smithay/wayland-window")
    (synopsis "A minimalistic window-decorations library built on top of
wayland-client")
    (description
     "This package provides a minimalistic window-decorations library built
on top of wayland-client.")
    (license license:expat)))

(define-public rust-wayland-window-0.7
  (package
    (inherit rust-wayland-window-0.13)
    (name "rust-wayland-window")
    (version "0.7.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-window" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "06j5f0v065p8ap8mw4a6k95x1dyidygk3fa6dhqghchi1kdih6hg"))))
    (arguments
     `(#:cargo-inputs (("rust-byteorder" ,rust-byteorder-1)
                       ("rust-tempfile" ,rust-tempfile-2)
                       ("rust-wayland-client" ,rust-wayland-client-0.9)
                       ("rust-wayland-protocols" ,rust-wayland-protocols-0.9))
       #:cargo-development-inputs (("rust-wayland-client" ,rust-wayland-client-0.9))))))

(define-public rust-wayland-window-0.4
  (package
    (inherit rust-wayland-window-0.13)
    (name "rust-wayland-window")
    (version "0.4.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-window" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1vv9cdx5m6ar64lyqmk68z4m4f1j03f9kpj1nv98fcgr1qspj5ap"))))
    (arguments
     `(#:cargo-inputs (("rust-byteorder" ,rust-byteorder-1)
                       ("rust-tempfile" ,rust-tempfile-2)
                       ("rust-wayland-client" ,rust-wayland-client-0.8))
       #:cargo-development-inputs (("rust-wayland-client" ,rust-wayland-client-0.8))))))

(define-public rust-wayland-window-0.2
  (package
    (inherit rust-wayland-window-0.13)
    (name "rust-wayland-window")
    (version "0.2.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-window" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0k48mmkmpkj3jaj60j85raigh0ngk1yzp2fq48ac5jb3m39nk6rh"))))
    (arguments
     `(#:cargo-inputs (("rust-byteorder" ,rust-byteorder-0.5)
                       ("rust-tempfile" ,rust-tempfile-2)
                       ("rust-wayland-client" ,rust-wayland-client-0.5))
       #:cargo-development-inputs (("rust-wayland-client" ,rust-wayland-client-0.5))))))

(define-public rust-winit-0.27
  (package
    (inherit rust-winit-0.28)
    (name "rust-winit")
    (version "0.27.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "winit" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0z35jymza04gjniq0mmydq3m1mrr9pqfcwcldj4zvcl6pmpnsydv"))))
    (arguments
     `(#:skip-build? #t ;windows only!
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-cocoa" ,rust-cocoa-0.24)
                       ("rust-core-foundation" ,rust-core-foundation-0.9)
                       ("rust-core-graphics" ,rust-core-graphics-0.22)
                       ("rust-dispatch" ,rust-dispatch-0.2)
                       ("rust-instant" ,rust-instant-0.1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-mint" ,rust-mint-0.5)
                       ("rust-mio" ,rust-mio-0.8)
                       ("rust-ndk" ,rust-ndk-0.7)
                       ("rust-ndk-glue" ,rust-ndk-glue-0.7)
                       ("rust-objc" ,rust-objc-0.2)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-parking-lot" ,rust-parking-lot-0.12)
                       ("rust-parking-lot" ,rust-parking-lot-0.12)
                       ("rust-percent-encoding" ,rust-percent-encoding-2)
                       ("rust-raw-window-handle" ,rust-raw-window-handle-0.4)
                       ("rust-raw-window-handle" ,rust-raw-window-handle-0.5)
                       ("rust-sctk-adwaita" ,rust-sctk-adwaita-0.4)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-smithay-client-toolkit" ,rust-smithay-client-toolkit-0.16)
                       ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
                       ("rust-wayland-client" ,rust-wayland-client-0.29)
                       ("rust-wayland-protocols" ,rust-wayland-protocols-0.29)
                       ("rust-web-sys" ,rust-web-sys-0.3)
                       ("rust-windows-sys" ,rust-windows-sys-0.36)
                       ("rust-x11-dl" ,rust-x11-dl-2))
       #:cargo-development-inputs (("rust-console-log" ,rust-console-log-0.2)
                                   ("rust-image" ,rust-image-0.24)
                                   ("rust-simple-logger" ,rust-simple-logger-2))))))

(define-public rust-winit-0.25
  (package
    (inherit rust-winit-0.28)
    (name "rust-winit")
    (version "0.25.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "winit" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1y62hqgb93yz02yxx54cmk5mj8agc0zpdxry8yz8cpjdb6a0fqbr"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-cocoa" ,rust-cocoa-0.24)
                       ("rust-core-foundation" ,rust-core-foundation-0.9)
                       ("rust-core-graphics" ,rust-core-graphics-0.22)
                       ("rust-core-video-sys" ,rust-core-video-sys-0.1)
                       ("rust-dispatch" ,rust-dispatch-0.2)
                       ("rust-instant" ,rust-instant-0.1)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-mio" ,rust-mio-0.6)
                       ("rust-mio-extras" ,rust-mio-extras-2)
                       ("rust-mio-misc" ,rust-mio-misc-1)
                       ("rust-ndk" ,rust-ndk-0.3)
                       ("rust-ndk-glue" ,rust-ndk-glue-0.3)
                       ("rust-ndk-sys" ,rust-ndk-sys-0.2)
                       ("rust-objc" ,rust-objc-0.2)
                       ("rust-parking-lot" ,rust-parking-lot-0.11)
                       ("rust-percent-encoding" ,rust-percent-encoding-2)
                       ("rust-raw-window-handle" ,rust-raw-window-handle-0.3)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-smithay-client-toolkit" ,rust-smithay-client-toolkit-0.12.3)
                       ("rust-stdweb" ,rust-stdweb-0.4)
                       ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
                       ("rust-web-sys" ,rust-web-sys-0.3)
                       ("rust-winapi" ,rust-winapi-0.3)
                       ("rust-x11-dl" ,rust-x11-dl-2))
       #:cargo-development-inputs (("rust-console-log" ,rust-console-log-0.2)
                                   ("rust-simple-logger" ,rust-simple-logger-1))))
    (inputs (list rust-wayland-client-0.28))))

(define-public rust-winit-0.22
  (package
    (inherit rust-winit-0.28)
    (name "rust-winit")
    (version "0.22.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "winit" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0qkqiv7h4ai6hy2ghs9lpd6vzihgx36wl5nfx8l7hqmnvpvwnk0y"))))
    (arguments
     `(#:skip-build? #t ;Requires Windows libraries.
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-cocoa" ,rust-cocoa-0.20)
                       ("rust-core-foundation" ,rust-core-foundation-0.7)
                       ("rust-core-graphics" ,rust-core-graphics-0.19)
                       ("rust-core-video-sys" ,rust-core-video-sys-0.1)
                       ("rust-dispatch" ,rust-dispatch-0.2)
                       ("rust-instant" ,rust-instant-0.1)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-mio" ,rust-mio-0.6)
                       ("rust-mio-extras" ,rust-mio-extras-2)
                       ("rust-ndk" ,rust-ndk-0.1)
                       ("rust-ndk-glue" ,rust-ndk-glue-0.1)
                       ("rust-ndk-sys" ,rust-ndk-sys-0.1)
                       ("rust-objc" ,rust-objc-0.2)
                       ("rust-objc" ,rust-objc-0.2)
                       ("rust-parking-lot" ,rust-parking-lot-0.10)
                       ("rust-percent-encoding" ,rust-percent-encoding-2)
                       ("rust-raw-window-handle" ,rust-raw-window-handle-0.3)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-smithay-client-toolkit" ,rust-smithay-client-toolkit-0.6)
                       ("rust-stdweb" ,rust-stdweb-0.4)
                       ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
                       ("rust-wayland-client" ,rust-wayland-client-0.23)
                       ("rust-web-sys" ,rust-web-sys-0.3)
                       ("rust-winapi" ,rust-winapi-0.3)
                       ("rust-x11-dl" ,rust-x11-dl-2))
       #:cargo-development-inputs (("rust-console-log" ,rust-console-log-0.1)
                                   ("rust-image" ,rust-image-0.23)
                                   ("rust-simple-logger" ,rust-simple-logger-1))))))

(define-public rust-winit-0.18
  (package
    (inherit rust-winit-0.28)
    (name "rust-winit")
    (version "0.18.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "winit" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0xh73k6182j1man1qxq55q7d0cmy592kw9iyyggiivy0sidw2mwc"))))
    (arguments
     `(#:cargo-inputs (("rust-android-glue" ,rust-android-glue-0.2)
                       ("rust-backtrace" ,rust-backtrace-0.3)
                       ("rust-cocoa" ,rust-cocoa-0.18)
                       ("rust-core-foundation" ,rust-core-foundation-0.6)
                       ("rust-core-graphics" ,rust-core-graphics-0.17)
                       ("rust-image" ,rust-image-0.20)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-objc" ,rust-objc-0.2)
                       ("rust-parking-lot" ,rust-parking-lot-0.7)
                       ("rust-percent-encoding" ,rust-percent-encoding-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-smithay-client-toolkit" ,rust-smithay-client-toolkit-0.4)
                       ("rust-wayland-client" ,rust-wayland-client-0.21)
                       ("rust-winapi" ,rust-winapi-0.3)
                       ("rust-x11-dl" ,rust-x11-dl-2))))))

(define-public rust-winit-0.12
  (package
    (inherit rust-winit-0.28)
    (name "rust-winit")
    (version "0.12.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "winit" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "08b5dajbnvrfi0d8ckzyr61p6jpp1zd8vh3jg5hp8zx5n94d70m2"))))
    (arguments
     `(#:cargo-inputs (("rust-android-glue" ,rust-android-glue-0.2)
                       ("rust-cocoa" ,rust-cocoa-0.14)
                       ("rust-core-foundation" ,rust-core-foundation-0.5)
                       ("rust-core-graphics" ,rust-core-graphics-0.13)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-objc" ,rust-objc-0.2)
                       ("rust-percent-encoding" ,rust-percent-encoding-1)
                       ("rust-wayland-client" ,rust-wayland-client-0.12)
                       ("rust-wayland-kbd" ,rust-wayland-kbd-0.13)
                       ("rust-wayland-protocols" ,rust-wayland-protocols-0.12)
                       ("rust-wayland-window" ,rust-wayland-window-0.13)
                       ("rust-winapi" ,rust-winapi-0.3)
                       ("rust-x11-dl" ,rust-x11-dl-2))))))

(define-public rust-winit-0.7
  (package
    (inherit rust-winit-0.28)
    (name "rust-winit")
    (version "0.7.6")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "winit" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1162sjhi0cn4z6c5ss1vcw4zwn25j6n464820dnrpw34aa7gi393"))))
    (arguments
     `(#:cargo-inputs (("rust-android-glue" ,rust-android-glue-0.2)
                       ("rust-cocoa" ,rust-cocoa-0.9)
                       ("rust-core-foundation" ,rust-core-foundation-0.4)
                       ("rust-core-graphics" ,rust-core-graphics-0.8)
                       ("rust-dwmapi-sys" ,rust-dwmapi-sys-0.1)
                       ("rust-gdi32-sys" ,rust-gdi32-sys-0.1)
                       ("rust-kernel32-sys" ,rust-kernel32-sys-0.2)
                       ("rust-lazy-static" ,rust-lazy-static-0.2)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-objc" ,rust-objc-0.2)
                       ("rust-shared-library" ,rust-shared-library-0.1)
                       ("rust-shell32-sys" ,rust-shell32-sys-0.1)
                       ("rust-tempfile" ,rust-tempfile-2)
                       ("rust-user32-sys" ,rust-user32-sys-0.1)
                       ("rust-wayland-client" ,rust-wayland-client-0.9)
                       ("rust-wayland-kbd" ,rust-wayland-kbd-0.9)
                       ("rust-wayland-protocols" ,rust-wayland-protocols-0.9)
                       ("rust-wayland-window" ,rust-wayland-window-0.7)
                       ("rust-winapi" ,rust-winapi-0.2)
                       ("rust-x11-dl" ,rust-x11-dl-2))))))

(define-public rust-winit-0.5
  (package
    (inherit rust-winit-0.28)
    (name "rust-winit")
    (version "0.5.11")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "winit" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1fl64gwcl4iwnnjhjbk9cf8vilyqkhyc8wizm4h593pn8dkpb37n"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-android-glue" ,rust-android-glue-0.2)
                       ("rust-cgl" ,rust-cgl-0.1)
                       ("rust-cocoa" ,rust-cocoa-0.5.2)
                       ("rust-core-foundation" ,rust-core-foundation-0.2)
                       ("rust-core-graphics" ,rust-core-graphics-0.4)
                       ("rust-dwmapi-sys" ,rust-dwmapi-sys-0.1)
                       ("rust-gdi32-sys" ,rust-gdi32-sys-0.1)
                       ("rust-kernel32-sys" ,rust-kernel32-sys-0.2)
                       ("rust-lazy-static" ,rust-lazy-static-0.2)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-objc" ,rust-objc-0.2)
                       ("rust-shared-library" ,rust-shared-library-0.1)
                       ("rust-shell32-sys" ,rust-shell32-sys-0.1)
                       ("rust-user32-sys" ,rust-user32-sys-0.1)
                       ("rust-wayland-client" ,rust-wayland-client-0.7)
                       ("rust-wayland-kbd" ,rust-wayland-kbd-0.6)
                       ("rust-wayland-window" ,rust-wayland-window-0.4)
                       ("rust-winapi" ,rust-winapi-0.2)
                       ("rust-x11-dl" ,rust-x11-dl-2))))))

(define-public rust-wgpu-0.15
  (package
    (name "rust-wgpu")
    (version "0.15.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wgpu" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1mrjs44n1wmsyr5y31yr2hk1s3j5w3pg0advxwyw718xv6va2ifp"))))
    (build-system cargo-build-system)
    (arguments
      ;wgpu-hal backend stopgap
     `(#:skip-build? #t
       #:cargo-inputs (("rust-arrayvec" ,rust-arrayvec-0.7)
                       ("rust-cfg-if" ,rust-cfg-if-1)
                       ("rust-js-sys" ,rust-js-sys-0.3)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-naga" ,rust-naga-0.11)
                       ("rust-parking-lot" ,rust-parking-lot-0.12)
                       ("rust-profiling" ,rust-profiling-1)
                       ("rust-raw-window-handle" ,rust-raw-window-handle-0.5)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-smallvec" ,rust-smallvec-1)
                       ("rust-static-assertions" ,rust-static-assertions-1)
                       ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
                       ("rust-wasm-bindgen-futures" ,rust-wasm-bindgen-futures-0.4)
                       ("rust-web-sys" ,rust-web-sys-0.3)
                       ("rust-wgpu-core" ,rust-wgpu-core-0.15)
                       ("rust-wgpu-hal" ,rust-wgpu-hal-0.15)
                       ("rust-wgpu-types" ,rust-wgpu-types-0.15))
       #:cargo-development-inputs (("rust-async-executor" ,rust-async-executor-1)
                                   ("rust-bitflags" ,rust-bitflags-1)
                                   ("rust-bytemuck" ,rust-bytemuck-1)
                                   ("rust-console-error-panic-hook" ,rust-console-error-panic-hook-0.1)
                                   ("rust-console-log" ,rust-console-log-0.2)
                                   ("rust-ddsfile" ,rust-ddsfile-0.5)
                                   ("rust-env-logger" ,rust-env-logger-0.9)
                                   ("rust-futures-intrusive" ,rust-futures-intrusive-0.4)
                                   ("rust-glam" ,rust-glam-0.21)
                                   ("rust-image" ,rust-image-0.24)
                                   ("rust-log" ,rust-log-0.4)
                                   ("rust-naga" ,rust-naga-0.11)
                                   ("rust-nanorand" ,rust-nanorand-0.7)
                                   ("rust-noise" ,rust-noise-0.7)
                                   ("rust-obj" ,rust-obj-0.10)
                                   ("rust-png" ,rust-png-0.17)
                                   ("rust-pollster" ,rust-pollster-0.2)
                                   ("rust-wasm-bindgen-test" ,rust-wasm-bindgen-test-0.3)
                                   ("rust-web-sys" ,rust-web-sys-0.3)
                                   ("rust-winit" ,rust-winit-0.27))))
    (home-page "https://wgpu.rs/")
    (synopsis "Rusty WebGPU API wrapper")
    (description
     "wgpu-rs is an idiomatic Rust wrapper over wgpu-core. It's designed to
be suitable for general purpose graphics and computation needs of the Rust
community.")
    (license (list license:expat license:asl2.0))))

(define-public rust-wgpu-core-0.15
  (package
    (name "rust-wgpu-core")
    (version "0.15.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wgpu-core" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1zrzkxnpqkxppbdwvrz2x6008fsp74vg162scj95fcqfjj6l0cbi"))))
    (build-system cargo-build-system)
    (arguments
      ;wgpu hal backend stopgap
     `(#:skip-build? #t
       #:cargo-inputs (("rust-arrayvec" ,rust-arrayvec-0.7)
                       ("rust-bit-vec" ,rust-bit-vec-0.6)
                       ("rust-bitflags" ,rust-bitflags-1)
                       ("rust-codespan-reporting" ,rust-codespan-reporting-0.11)
                       ("rust-fxhash" ,rust-fxhash-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-naga" ,rust-naga-0.11)
                       ("rust-parking-lot" ,rust-parking-lot-0.12)
                       ("rust-profiling" ,rust-profiling-1)
                       ("rust-raw-window-handle" ,rust-raw-window-handle-0.5)
                       ("rust-ron" ,rust-ron-0.8)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-smallvec" ,rust-smallvec-1)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-web-sys" ,rust-web-sys-0.3)
                       ("rust-wgpu-hal" ,rust-wgpu-hal-0.15)
                       ("rust-wgpu-types" ,rust-wgpu-types-0.15))))
    (home-page "https://wgpu.rs/")
    (synopsis "WebGPU core logic on wgpu-hal")
    (description "This package provides @code{WebGPU} core logic on wgpu-hal")
    (license (list license:expat license:asl2.0))))

(define-public rust-wgpu-hal-0.15
  (package
    (name "rust-wgpu-hal")
    (version "0.15.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wgpu-hal" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "12r8w1577ivkdhf6c9xhlrf6vy7kj7m8ipakajxl9ixdhfi63kxx"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t ;Enable backend feature, maybe cargo compile flag?
       #:cargo-inputs (("rust-android-system-properties" ,rust-android-system-properties-0.1)
                       ("rust-arrayvec" ,rust-arrayvec-0.7)
                       ("rust-ash" ,rust-ash-0.37)
                       ("rust-bit-set" ,rust-bit-set-0.5)
                       ("rust-bitflags" ,rust-bitflags-1)
                       ("rust-block" ,rust-block-0.1)
                       ("rust-core-graphics-types" ,rust-core-graphics-types-0.1)
                       ("rust-d3d12" ,rust-d3d12-0.6)
                       ("rust-foreign-types" ,rust-foreign-types-0.3)
                       ("rust-fxhash" ,rust-fxhash-0.2)
                       ("rust-glow" ,rust-glow-0.12)
                       ("rust-gpu-alloc" ,rust-gpu-alloc-0.5)
                       ("rust-gpu-allocator" ,rust-gpu-allocator-0.22)
                       ("rust-gpu-descriptor" ,rust-gpu-descriptor-0.2)
                       ("rust-hassle-rs" ,rust-hassle-rs-0.9)
                       ("rust-js-sys" ,rust-js-sys-0.3)
                       ("rust-khronos-egl" ,rust-khronos-egl-4)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-libloading" ,rust-libloading-0.7)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-metal" ,rust-metal-0.24)
                       ("rust-naga" ,rust-naga-0.11)
                       ("rust-objc" ,rust-objc-0.2)
                       ("rust-parking-lot" ,rust-parking-lot-0.12)
                       ("rust-profiling" ,rust-profiling-1)
                       ("rust-range-alloc" ,rust-range-alloc-0.1)
                       ("rust-raw-window-handle" ,rust-raw-window-handle-0.5)
                       ("rust-renderdoc-sys" ,rust-renderdoc-sys-0.7)
                       ("rust-smallvec" ,rust-smallvec-1)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
                       ("rust-web-sys" ,rust-web-sys-0.3)
                       ("rust-wgpu-types" ,rust-wgpu-types-0.15)
                       ("rust-winapi" ,rust-winapi-0.3))
       #:cargo-development-inputs (("rust-env-logger" ,rust-env-logger-0.9)
                                   ("rust-glutin" ,rust-glutin-0.29)
                                   ("rust-naga" ,rust-naga-0.11)
                                   ("rust-winit" ,rust-winit-0.27))))
    (home-page "https://wgpu.rs/")
    (synopsis "WebGPU hardware abstraction layer")
    (description
     "This package provides the @code{WebGPU} hardware abstraction layer.")
    (license (list license:expat license:asl2.0))))

(define-public rust-wgpu-types-0.15
  (package
    (name "rust-wgpu-types")
    (version "0.15.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wgpu-types" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "047iysi1v6yznrfplgp08jyr353s8pg2zqqd5jq0rl0b3c94wi1j"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-js-sys" ,rust-js-sys-0.3)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-web-sys" ,rust-web-sys-0.3))
       #:cargo-development-inputs (("rust-serde" ,rust-serde-1)
                                   ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://wgpu.rs/")
    (synopsis "WebGPU types")
    (description "This package provides @code{WebGPU} types.")
    (license (list license:expat license:asl2.0))))

(define-public rust-x11-clipboard-0.3
  (package
    (inherit rust-x11-clipboard-0.7)
    (name "rust-x11-clipboard")
    (version "0.3.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "x11-clipboard" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1smwyr23jns0dncm6bwv00xfxxy99bv6qlx6df7dkdcydk04kgc9"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-xcb" ,rust-xcb-0.8))))
    (inputs (list libx11 libxcb xcb-proto))
    (native-inputs (list pkg-config python))))
 ; from rust-xcb-0.8

(define-public rust-x11-dl-2.3
  (package
    (inherit rust-x11-dl-2)
    (name "rust-x11-dl")
    (version "2.3.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "x11-dl" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0l9a7gzgsm5l4y5755id97sdy6bc05xyns0p5dfhpmv0r93pp2rk"))))
    (arguments
     `(#:cargo-inputs (("rust-dylib" ,rust-dylib-0.0.1)
                       ("rust-libc" ,rust-libc-0.2))))))
