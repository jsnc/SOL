(define-module (sol packages crates-io)
  #:use-module (guix packages)
  #:use-module (guix build-system cargo)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages crates-graphics)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xorg)
  #:use-module (sol packages crates-graphics))

(define-public rust-abomonation-0.5
  (package
    (inherit rust-abomonation-0.7)
    (name "rust-abomonation")
    (version "0.5.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "abomonation" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "19h4s5ai8pbaap7n8pcd6yinqp22hx29ls9d2gdwsjka3m9xy6gv"))))))

(define-public rust-abort-on-panic-1
  (package
    (name "rust-abort-on-panic")
    (version "1.0.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "abort_on_panic" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "06gr9ryabrcg59b4496c6gwlwxy51b84zgpgap4mq2czxkwliacz"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/emk/abort_on_panic-rs")
    (synopsis "Intercept panic! from unsafe locations and abort the process")
    (description
     "When calling Rust code from C, it's unsafe to call panic!. Doing so
may cause unsafe behavior. But when calling user-defined functions, we
sometimes need to enforce these rules.")
    (license license:unlicense)))

(define-public rust-afl-0.1
  (package
    (inherit rust-afl-0.12)
    (name "rust-afl")
    (version "0.1.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "afl" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "033gvl4pnyry34vh5pbr01h78c4zpycn997nc0mmha0ah356271z"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t ;Can't find afl-sys locally
       #:cargo-inputs (("rust-afl-sys" ,rust-afl-sys-0.1)
                       ("rust-gcc" ,rust-gcc-0.3))
       #:cargo-development-inputs (("rust-afl-plugin" ,rust-afl-plugin-0.1)
                                   ("rust-byteorder" ,rust-byteorder-0.3)
                                   ("rust-libc" ,rust-libc-0.2)
                                   ("rust-tempdir" ,rust-tempdir-0.3))))))

(define-public rust-afl-sys-0.1
  (package
    (name "rust-afl-sys")
    (version "0.1.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "afl-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0kipfjhbypflv0bbrvrccm0jan0jampl13d2f68pjxj1ymhcwpid"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;Broken tests using external program
       #:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-gcc" ,rust-gcc-0.3))))
    (home-page "https://github.com/frewsxcv/afl.rs")
    (synopsis "Wrapper around AFL source")
    (description "Wrapper around AFL source.")
    (license license:asl2.0)))

(define-public rust-afl-plugin-0.1
  (package
    (name "rust-afl-plugin")
    (version "0.1.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "afl-plugin" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "103ba3r1y285w7vmqfnsya5wdq2v8jlsc9wbrl6hbxsp8z9spkab"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t ;Relies on an unsupported LLVM for a custom build
       #:cargo-inputs (("rust-gcc" ,rust-gcc-0.3)
                       ("rust-quale" ,rust-quale-1))))
    (home-page "https://github.com/frewsxcv/afl.rs")
    (synopsis "LLVM instrumentation compiler plugin for afl.rs")
    (description
     "This package provides a LLVM instrumentation compiler plugin for
afl.rs.")
    (license license:asl2.0)))

(define-public rust-al-sys-0.6
  (package
    (name "rust-al-sys")
    (version "0.6.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "al-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "08whlcfrhn4gqi4nbglkdqv5ysdpnvnlsqg51q34q9hh9l7rp3gz"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-cmake" ,rust-cmake-0.1)
                       ("rust-libloading" ,rust-libloading-0.5)
                       ("rust-rental" ,rust-rental-0.5))))
    (home-page "https://github.com/jpernst/alto.git")
    (synopsis "Raw bindings for OpenAL 1.1")
    (description "Raw bindings for @code{OpenAL} 1.1.")
    (license (list license:expat license:asl2.0))))

(define-public rust-alga-0.7
  (package
    (inherit rust-alga-0.9)
    (name "rust-alga")
    (version "0.7.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "alga" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0y2p2n5i61a7qf7l49xy8vj0qcaj3lkjz33vfin9iwjrrbp01fr4"))))
    (arguments
     `(#:tests? #f ;can't find macro in scope
       #:cargo-inputs (("rust-approx" ,rust-approx-0.3)
                       ("rust-decimal" ,rust-decimal-2)
                       ("rust-libm" ,rust-libm-0.1)
                       ("rust-num-complex" ,rust-num-complex-0.2)
                       ("rust-num-traits" ,rust-num-traits-0.2))
       #:cargo-development-inputs (("rust-alga-derive" ,rust-alga-derive-0.7)
                                   ("rust-quickcheck" ,rust-quickcheck-0.7))))))

(define-public rust-alga-derive-0.7
  (package
    (inherit rust-alga-derive-0.9)
    (name "rust-alga-derive")
    (version "0.7.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "alga-derive" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "05lccfjhm6sm1lalkmi1c2vdzcpz9h1s9yyk5cwkyh69s8a6xw46"))))
    (arguments
     `(#:cargo-inputs (("rust-edit-distance" ,rust-edit-distance-2)
                       ("rust-quickcheck" ,rust-quickcheck-0.7)
                       ("rust-quote" ,rust-quote-0.3)
                       ("rust-syn" ,rust-syn-0.11))))))

(define-public rust-alsa-0.8
  (package
    (name "rust-alsa")
    (version "0.8.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "alsa" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "02k93vj8h789qx6as8yzi7r2wycqgmcsmk6m1pbb99dkwkhhjwww"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;integration tests fail on ALSA lib
       #:cargo-inputs (("rust-alsa-sys" ,rust-alsa-sys-0.3)
                       ("rust-bitflags" ,rust-bitflags-2.4.0)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-nix" ,rust-nix-0.26))))
    (inputs (list alsa-lib))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/diwic/alsa-rs")
    (synopsis "Thin but safe wrappers for ALSA (Linux sound API)")
    (description
     "Thin but safe wrappers for ALSA, the most common API for accessing
audio devices on Linux.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-alsa-0.7
  (package
    (inherit rust-alsa-0.8)
    (name "rust-alsa")
    (version "0.7.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "alsa" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0iwbdgb6lr81iji9sr4f91mys24pia5avnkgbkv8kxzhvkc2lmp2"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;integration tests fail on ALSA lib
       #:cargo-inputs (("rust-alsa-sys" ,rust-alsa-sys-0.3)
                       ("rust-bitflags" ,rust-bitflags-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-nix" ,rust-nix-0.24))))))

(define-public rust-alsa-0.6
  (package
    (inherit rust-alsa-0.8)
    (name "rust-alsa")
    (version "0.6.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "alsa" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0szx8finhqbffh08fp3bgh4ywz0b572vcdyh4hwyhrfgw8pza5ar"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;integration tests fail on ALSA lib
       #:cargo-inputs (("rust-alsa-sys" ,rust-alsa-sys-0.3)
                       ("rust-bitflags" ,rust-bitflags-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-nix" ,rust-nix-0.23))))))

(define-public rust-alsa-sys-0.3
  (package
    (name "rust-alsa-sys")
    (version "0.3.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "alsa-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "09qmmnpmlcj23zcgx2xsi4phcgm5i02g9xaf801y7i067mkfx3yv"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;doc tests fail due to scoping issues
       #:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (inputs (list alsa-lib))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/diwic/alsa-sys")
    (synopsis
     "FFI bindings for the ALSA project (Advanced Linux Sound Architecture)")
    (description "This crate provides raw FFI bindings for ALSA.")
    (license license:expat)))

(define-public rust-alsa-sys-0.1
  (package
    (inherit rust-alsa-sys-0.3)
    (name "rust-alsa-sys")
    (version "0.1.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "alsa-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0n3xr2msblmqlsx313b2y2v9hamqh0hp43v23fp1b3znkszwpvdh"))))))

(define-public rust-alto-3
  (package
    (name "rust-alto")
    (version "3.0.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "alto" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1rgsdmh346s3rwhzqacjc6nz7jap4dd72c1gfmkaq9sgzh9fhnyp"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;Attempts to open windows DLL object files
       #:cargo-inputs (("rust-al-sys" ,rust-al-sys-0.6)
                       ("rust-lazy-static" ,rust-lazy-static-0.2)
                       ("rust-parking-lot" ,rust-parking-lot-0.4))))
    (home-page "https://github.com/jpernst/alto.git")
    (synopsis
     "Idiomatic interface for OpenAL 1.1 and extensions (including EFX)")
    (description
     "Idiomatic interface for @code{OpenAL} 1.1 and extensions (including EFX).")
    (license (list license:expat license:asl2.0))))

(define-public rust-android-glue-0.1
  (package
    (inherit rust-android-glue-0.2)
    (name "rust-android-glue")
    (version "0.1.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "android-glue" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0y07v7vvirxlxbbkajglfdx4hfrm2vbzqbpwzkh5ib3vid7j25zp"))))
    (arguments
     `(#:skip-build? #t))))

(define-public rust-android-logger-0.11
  (package
    (inherit rust-android-logger-0.10) ;HIGHER THAN UPSTREAM
    (name "rust-android-logger")
    (version "0.11.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "android-logger" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0fl8ix7q1cj73lzy6xcwyrqwpvnx5aaxszawidivv9ra4h6bh6c6"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-android-log-sys" ,rust-android-log-sys-0.2)
                       ("rust-env-logger" ,rust-env-logger-0.10)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-once-cell" ,rust-once-cell-1))))))

(define-public rust-annotate-snippets-0.1
  (package
    (name "rust-annotate-snippets")
    (version "0.1.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "annotate-snippets" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "11pc61qyjk68z3inq5pysx9h96vp8bpcx1240d4b3kcigia7wpj5"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-ansi-term" ,rust-ansi-term-0.11))
       #:cargo-development-inputs (("rust-ansi-term" ,rust-ansi-term-0.11)
                                   ("rust-difference" ,rust-difference-2)
                                   ("rust-glob" ,rust-glob-0.2)
                                   ("rust-serde" ,rust-serde-1)
                                   ("rust-serde-derive" ,rust-serde-derive-1)
                                   ("rust-serde-yaml" ,rust-serde-yaml-0.7))))
    (home-page "https://github.com/rust-lang/annotate-snippets-rs")
    (synopsis "Library for building code annotations")
    (description
     "This package provides a library for building code annotations.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-arrayvec-0.3
  (package
    (inherit rust-arrayvec-0.7)
    (name "rust-arrayvec")
    (version "0.3.25")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "arrayvec" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0krkhgiwsd54r11sqnx8xymgy8xd70h8rllhrpx7ifq60ghrzx86"))))
    (arguments
     `(#:cargo-inputs (("rust-generic-array" ,rust-generic-array-0.5)
                       ("rust-nodrop" ,rust-nodrop-0.1)
                       ("rust-odds" ,rust-odds-0.2))))))

(define-public rust-asio-sys-0.2
  (package
    (name "rust-asio-sys")
    (version "0.2.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "asio-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "16lbavksj2aasadyxbdnbrll6a1m8cwl4skbxgbvr1ma2wpwv82c"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t ;broken build.rs can't find dependencies
       #:cargo-inputs (("rust-num-derive" ,rust-num-derive-0.3)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-bindgen" ,rust-bindgen-0.56)
                       ("rust-cc" ,rust-cc-1)
                       ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "https://github.com/RustAudio/cpal/")
    (synopsis
     "Low-level interface and binding generation for the steinberg ASIO SDK")
    (description
     "Low-level interface and binding generation for the steinberg ASIO SDK.")
    (license license:asl2.0)))

(define-public rust-as-raw-xcb-connection-1
  (package
    (name "rust-as-raw-xcb-connection")
    (version "1.0.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "as-raw-xcb-connection" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1am99fbsp5f5vnbvr0qnjma36q49c9zvdbn0czwwvian18mk2prd"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/psychon/as-raw-xcb-connection")
    (synopsis "Trait to facilitate interoperatibility with libxcb C API")
    (description "Trait to facilitate interoperatibility with libxcb C API")
    (license (list license:expat license:asl2.0))))

(define-public rust-aster-0.22
  (package
    (name "rust-aster")
    (version "0.22.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "aster" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "07kls3rhjsr20g6rqqc6kaf08kds3syxsq94fvhj4rggfvj22biw"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-clippy" ,rust-clippy-0.0)
                       ("rust-compiletest-rs" ,rust-compiletest-rs-0.2)
                       ("rust-syntex-syntax" ,rust-syntex-syntax-0.39))))
    (home-page "https://github.com/serde-rs/aster")
    (synopsis "A libsyntax ast builder")
    (description "This package provides a libsyntax ast builder.")
    (license (list license:expat license:asl2.0))))

(define-public rust-atomic-refcell-0.1
  (package
    (name "rust-atomic-refcell")
    (version "0.1.11")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "atomic-refcell" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "01f9c4l5fz8g1ghlvvxh3bzjqja8m3vlwjbbbgybcg6bysrzcbhi"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/bholley/atomic_refcell")
    (synopsis "Threadsafe RefCell")
    (description "This package provides a Threadsafe @code{RefCell}.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-backtrace-0.2
  (package
    (inherit rust-backtrace-0.3)
    (name "rust-backtrace")
    (version "0.2.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "backtrace" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0brakrf6lkhq9rra7kz0rhszs1csnqv24frd11rvrydmy127cv9l"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t ;failed to load dependency
       #:cargo-inputs (("rust-backtrace-sys" ,rust-backtrace-sys-0.1)
                       ("rust-cfg-if" ,rust-cfg-if-0.1)
                       ("rust-dbghelp-sys" ,rust-dbghelp-sys-0.2)
                       ("rust-kernel32-sys" ,rust-kernel32-sys-0.2)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-rustc-demangle" ,rust-rustc-demangle-0.1)
                       ("rust-rustc-serialize" ,rust-rustc-serialize-0.3)
                       ("rust-serde" ,rust-serde-0.7)
                       ("rust-serde-codegen" ,rust-serde-codegen-0.7)
                       ("rust-winapi" ,rust-winapi-0.2))))))

(define-public rust-backtrace-0.1
  (package
    (inherit rust-backtrace-0.3)
    (name "rust-backtrace")
    (version "0.1.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "backtrace" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "182hccf7pgr9h2nyidk56cgxw89d0xj9v42g8xnznypsia1ff2hm"))))
    (arguments
     `(#:skip-build? #t ;Failed to get dependencies
       #:cargo-inputs (("rust-backtrace-sys" ,rust-backtrace-sys-0.1)
                       ("rust-cfg-if" ,rust-cfg-if-0.1)
                       ("rust-dbghelp-sys" ,rust-dbghelp-sys-0.2)
                       ("rust-debug-builders" ,rust-debug-builders-0.1)
                       ("rust-kernel32-sys" ,rust-kernel32-sys-0.2)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-winapi" ,rust-winapi-0.2))))))

(define-public rust-backtrace-sys-0.1
  (package
    (name "rust-backtrace-sys")
    (version "0.1.37")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "backtrace-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "16a3igz22q9lnnjjr77f4k8ci48v8zdwrs67khx3h7wx3jzfpyqq"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-compiler-builtins" ,rust-compiler-builtins-0.1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-rustc-std-workspace-core" ,rust-rustc-std-workspace-core-1))))
    (home-page "https://github.com/alexcrichton/backtrace-rs")
    (synopsis "Bindings to the libbacktrace gcc library")
    (description
     "This package provides bindings to the libbacktrace gcc library")
    (license (list license:expat license:asl2.0))))

(define-public rust-bincode-0.8
  (package
    (inherit rust-bincode-1)
    (name "rust-bincode")
    (version "0.8.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "bincode" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0nbj0lwykwa1a7sa4303rxgpng9p2hcz9s5d5qcrckrpmcxjsjkf"))))
    (arguments
     `(#:cargo-inputs (("rust-byteorder" ,rust-byteorder-1)
                       ("rust-num-traits" ,rust-num-traits-0.1)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-serde-bytes" ,rust-serde-bytes-0.10)
                                   ("rust-serde-derive" ,rust-serde-derive-1))))))

(define-public rust-bindgen-0.56
  (package
    (inherit rust-bindgen-0.57)
    (name "rust-bindgen")
    (version "0.56.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "bindgen" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0fajmgk2064ca1z9iq1jjkji63qwwz38z3d67kv6xdy0xgdpk8rd"))))
    (inputs (list clang))
    (arguments
     `(#:tests? #f ;One of the test headers was not found
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-cexpr" ,rust-cexpr-0.4)
                       ("rust-cfg-if" ,rust-cfg-if-0.1)
                       ("rust-clang-sys" ,rust-clang-sys-1)
                       ("rust-clap" ,rust-clap-2)
                       ("rust-env-logger" ,rust-env-logger-0.7)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-lazycell" ,rust-lazycell-1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-peeking-take-while" ,rust-peeking-take-while-0.1)
                       ("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-regex" ,rust-regex-1)
                       ("rust-rustc-hash" ,rust-rustc-hash-1)
                       ("rust-shlex" ,rust-shlex-0.1)
                       ("rust-which" ,rust-which-3))
       #:cargo-development-inputs (("rust-clap" ,rust-clap-2)
                                   ("rust-diff" ,rust-diff-0.1)
                                   ("rust-shlex" ,rust-shlex-0.1))
       #:phases (modify-phases %standard-phases
                  (add-after 'unpack 'enable-unstable-features
                    (lambda _
                      (setenv "RUSTC_BOOTSTRAP" "1") #t)))))))

(define-public rust-bindgen-0.31
  (package
    (inherit rust-bindgen-0.57)
    (name "rust-bindgen")
    (version "0.31.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "bindgen" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1snl975kkyjcm98r6qcj0ah0da13pk6vblzzaygg5x46q2ck69ap"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-cexpr" ,rust-cexpr-0.2)
                       ("rust-cfg-if" ,rust-cfg-if-0.1)
                       ("rust-clang-sys" ,rust-clang-sys-0.21)
                       ("rust-clap" ,rust-clap-2)
                       ("rust-env-logger" ,rust-env-logger-0.4)
                       ("rust-lazy-static" ,rust-lazy-static-0.2)
                       ("rust-log" ,rust-log-0.3)
                       ("rust-peeking-take-while" ,rust-peeking-take-while-0.1)
                       ("rust-quote" ,rust-quote-0.3)
                       ("rust-regex" ,rust-regex-0.2)
                       ("rust-which" ,rust-which-1))
       #:cargo-development-inputs (("rust-clap" ,rust-clap-2)
                                   ("rust-diff" ,rust-diff-0.1)
                                   ("rust-shlex" ,rust-shlex-0.1))))))

(define-public rust-bitflags-2.4.0
  (package
    (name "rust-bitflags")
    (version "2.4.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "bitflags" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0dc6xa7flfl59makmhixjcrslwlvdxxwrgxbr8p7bkvz53k2ls5l"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;Not all files included.
       #:cargo-inputs (("rust-arbitrary" ,rust-arbitrary-1)
                       ("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-compiler-builtins" ,rust-compiler-builtins-0.1)
                       ("rust-rustc-std-workspace-core" ,rust-rustc-std-workspace-core-1)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-arbitrary" ,rust-arbitrary-1)
                                   ("rust-bytemuck" ,rust-bytemuck-1)
                                   ("rust-rustversion" ,rust-rustversion-1)
                                   ("rust-serde-derive" ,rust-serde-derive-1)
                                   ("rust-serde-json" ,rust-serde-json-1)
                                   ("rust-serde-test" ,rust-serde-test-1)
                                   ("rust-trybuild" ,rust-trybuild-1)
                                   ("rust-zerocopy" ,rust-zerocopy-0.6))))
    (home-page "https://github.com/bitflags/bitflags")
    (synopsis "Macro to generate structures which behave like bitflags")
    (description "This package provides a macro to generate structures which
behave like a set of bitflags.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-bitflags-0.6
  (package
    (inherit rust-bitflags-2)
    (name "rust-bitflags")
    (version "0.6.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "bitflags" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0imflafk7k1grpacfnk0l62h7s06qwi74521283j9q2fpla77kbj"))))
    (arguments
     `())))

(define-public rust-bitflags-0.5
  (package
    (inherit rust-bitflags-2)
    (name "rust-bitflags")
    (version "0.5.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "bitflags" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "08qdq5w1nd3hzwsrxk0dxzqv4g8wbwj6v2193njskwzdd09r6rsg"))))
    (arguments
     `())))

(define-public rust-bitflags-0.3
  (package
    (inherit rust-bitflags-2)
    (name "rust-bitflags")
    (version "0.3.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "bitflags" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "07rvj36f3ifi4vlgdm8h2ddv6s1aiglip0l6gn6ln49bjcy29ljg"))))
    (arguments
     `())))

(define-public rust-bytemuck-1
  ;; Bump
  (package
    (name "rust-bytemuck")
    (version "1.14.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "bytemuck" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1ik1ma5n3bg700skkzhx50zjk7kj7mbsphi773if17l04pn2hk9p"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bytemuck-derive" ,rust-bytemuck-derive-1))))
    (home-page "https://github.com/Lokathor/bytemuck")
    (synopsis "A crate for mucking around with piles of bytes")
    (description
     "This package provides a crate for mucking around with piles of bytes.")
    (license (list license:zlib license:asl2.0 license:expat))))

(define-public rust-bytemuck-derive-1
  ;; Bump
  (package
    (name "rust-bytemuck-derive")
    (version "1.5.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "bytemuck-derive" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1cgj75df2v32l4fmvnp25xxkkz4lp6hz76f7hfhd55wgbzmvfnln"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-2))))
    (home-page "https://github.com/Lokathor/bytemuck")
    (synopsis "Derive proc-macros for @code{bytemuck}")
    (description
     "This package derives proc-macros for the @code{bytemuck} crate.")
    (license (list license:zlib license:asl2.0 license:expat))))

(define-public rust-byteorder-0.4
  (package
    (inherit rust-byteorder-1)
    (name "rust-byteorder")
    (version "0.4.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "byteorder" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "010k8mb3fi4jgbj4nxzggyi7zg2jvm7aqirdyf5c1348h4cb9j4n"))))
    (arguments
     `(#:tests? #f
       #:cargo-development-inputs (("rust-quickcheck" ,rust-quickcheck-0.2)
                                   ("rust-rand" ,rust-rand-0.3))))))

(define-public rust-byteorder-0.3
  (package
    (inherit rust-byteorder-1)
    (name "rust-byteorder")
    (version "0.3.13")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "byteorder" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0xd1vzp1yzw9f9qpm7w3mp9kqxdxwrwzqs4d620n6m4g194smci9"))))
    (arguments
     `(#:tests? #f
       #:cargo-development-inputs (("rust-quickcheck" ,rust-quickcheck-0.2)
                                   ("rust-rand" ,rust-rand-0.3))))))

(define-public rust-c-vec-1
  (package
    (inherit rust-c-vec-2)
    (name "rust-c-vec")
    (version "1.3.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "c-vec" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0c3wgb15h97k6lzfm9qgkwk35ij2ad7w8fb5rbqvalyf3n8ii8zq"))))
    (arguments
     `(#:cargo-development-inputs (("rust-doc-comment" ,rust-doc-comment-0.3)
                                   ("rust-libc" ,rust-libc-0.2))))))

(define-public rust-cache-padded-1
  ;; Upstream from 1.1.1, but also deprecated.
  (package
    (name "rust-cache-padded")
    (version "1.3.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cache-padded" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "08gb1407k0cvhfllgg06j45r0lv99qrmraf19mccqbs2iz4j05cq"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/stjepang/cache-padded")
    (synopsis "Prevent once piece of data invalidating other cached data")
    (description
     "In concurrent programming, sometimes it is desirable to make sure
commonly accessed shared data is not all placed into the same cache line.
Updating an atomic value invalides the whole cache line it belongs to, which
makes the next access to the same cache line slower for other CPU cores.  Use
CachePadded to ensure updating one piece of data doesn't invalidate other
cached data.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-cc-1
  ;; upstream bump
  (package
    (name "rust-cc")
    (version "1.0.82")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cc" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "00cgp2xjw0jbryp2xqajgb9rh9s23nk6nwmnm07jli61xm2ycprh"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-jobserver" ,rust-jobserver-0.1)
                       ("rust-libc" ,rust-libc-0.2))
       #:cargo-development-inputs (("rust-tempfile" ,rust-tempfile-3))))
    (home-page "https://github.com/rust-lang/cc-rs")
    (synopsis "Invoke the native C compiler")
    (description
     "This package provides a build-time dependency for Cargo build scripts to assist
in invoking the native C compiler to compile native C code into a static archive
to be linked into Rust code.")
    (license (list license:expat license:asl2.0))))

(define-public rust-chlorine-1
  (package
    (name "rust-chlorine")
    (version "1.0.10")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "chlorine" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1v2230qas1xapywgmk8zfi8l8zd8mwafb8z2x867rbx8cvlnyivm"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/Lokathor/chlorine")
    (synopsis "C types for `no_std`")
    (description "Just the C types for `no_std`, but builds faster.")
    (license (list license:zlib license:asl2.0 license:expat))))

(define-public rust-clang-sys-0.21
  (package
    (inherit rust-clang-sys-1)
    (name "rust-clang-sys")
    (version "0.21.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "clang-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0bhd90xjp2rlbfi06ncyyjskz041zg3wqwqyh1h1dlg14sbsy574"))))
    (arguments
     `(#:cargo-inputs (("rust-clippy" ,rust-clippy-0.0)
                       ("rust-glob" ,rust-glob-0.2)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-libloading" ,rust-libloading-0.4))))))

(define-public rust-claxon-0.4
  (package
    (name "rust-claxon")
    (version "0.4.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "claxon" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1206mxvw833ysg10029apcsjjwly8zmsvksgza5cm7ma4ikzbysb"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-development-inputs (("rust-hound" ,rust-hound-3)
                                   ("rust-mp4parse" ,rust-mp4parse-0.8)
                                   ("rust-ogg" ,rust-ogg-0.5)
                                   ("rust-time" ,rust-time-0.1)
                                   ("rust-walkdir" ,rust-walkdir-1))))
    (home-page "https://github.com/ruuda/claxon")
    (synopsis "A FLAC decoding library")
    (description
     "Claxon is a FLAC decoder written in pure Rust. It has been fuzzed and
verified against the reference decoder for correctness. Its
performance is similar to the reference decoder.")
    (license license:asl2.0)))

(define-public rust-claxon-0.3
  (package
    (inherit rust-claxon-0.4)
    (name "rust-claxon")
    (version "0.3.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "claxon" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1j4p47y8hs88ysh4z8k9sa2bda3ni2lbfrjv2dg90v44zybka69m"))))
    (arguments
     `(#:tests? #f
       #:cargo-development-inputs (("rust-hound" ,rust-hound-3)
                                   ("rust-time" ,rust-time-0.1)
                                   ("rust-walkdir" ,rust-walkdir-1))))))

(define-public rust-clipboard-0.5
  (package
    (name "rust-clipboard")
    (version "0.5.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "clipboard" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1rxjfn811h09g6jpjjs2vx7z52wj6dxnflbwryfj6h03dij09a95"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-clipboard-win" ,rust-clipboard-win-2)
                       ("rust-objc" ,rust-objc-0.2)
                       ("rust-objc-foundation" ,rust-objc-foundation-0.1)
                       ("rust-objc-id" ,rust-objc-id-0.1)
                       ("rust-x11-clipboard" ,rust-x11-clipboard-0.3))))
    (inputs (list libx11 libxcb xcb-proto))
    (native-inputs (list pkg-config python))
    (home-page "https://github.com/aweinstock314/rust-clipboard")
    (synopsis
     "rust-clipboard is a cross-platform library for getting and setting the
contents of the OS-level clipboard")
    (description
     "rust-clipboard is a cross-platform library for getting and setting the
contents of the OS-level clipboard.")
    (license (list license:expat license:asl2.0))))

(define-public rust-clipboard-win-2
  (package
    (inherit rust-clipboard-win-4)
    (name "rust-clipboard-win")
    (version "2.2.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "clipboard-win" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0svqk0lrw66abaxd6h7l4k4g2s5vd1dcipy34kzfan6mzvb97873"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-winapi" ,rust-winapi-0.3))))))

(define-public rust-clock-ticks-0.1
  (package
    (name "rust-clock-ticks")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "clock-ticks" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0qrd0hb6k19ckhmvryimvdv0jpzb3j34khhaxm0mzb3kivsr16n4"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/tomaka/clock_ticks")
    (synopsis
     "Contains precise_time_s() and precise_time_ns() without any C code")
    (description
     "This package contains precise_time_s() and precise_time_ns() without
any C code")
    (license (list license:expat license:asl2.0))))

(define-public rust-cocoa-0.20
  (package
    (inherit rust-cocoa-0.24)
    (name "rust-cocoa")
    (version "0.20.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cocoa" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1y0wd1lyiz8cgbsf0fwyw06gb1akg6rvg5jr3wah8mvdqdpyhj8c"))))
    (arguments
     `(#:skip-build? #t ;Requires MacOS.
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-block" ,rust-block-0.1)
                       ("rust-core-foundation" ,rust-core-foundation-0.7)
                       ("rust-core-graphics" ,rust-core-graphics-0.19)
                       ("rust-foreign-types" ,rust-foreign-types-0.3)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-objc" ,rust-objc-0.2))))))

(define-public rust-cocoa-0.14
  (package
    (inherit rust-cocoa-0.24)
    (name "rust-cocoa")
    (version "0.14.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cocoa" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1imdnl5h339p7vz3md0z6bfmrlsnjfw9cn3fvwll9vz1vn2k1hmh"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-block" ,rust-block-0.1)
                       ("rust-core-graphics" ,rust-core-graphics-0.13)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-objc" ,rust-objc-0.2))))))

(define-public rust-cocoa-0.9
  (package
    (inherit rust-cocoa-0.24)
    (name "rust-cocoa")
    (version "0.9.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cocoa" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1kkns239dy4nj7b4q70rf217hb1qisrlz0z7wlmcqh3gagbgwis0"))))
    (arguments
     `(#:skip-build? #t ;Requires macOS
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.9)
                       ("rust-block" ,rust-block-0.1)
                       ("rust-core-graphics" ,rust-core-graphics-0.8)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-objc" ,rust-objc-0.2))))))

(define-public rust-cocoa-0.5
  (package
    (inherit rust-cocoa-0.24)
    (name "rust-cocoa")
    (version "0.5.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cocoa" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0xb31lrapdxrkl0jp30g3q4c3icy5z24xpfr7y9c3nxic7xd0vcc"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.7)
                       ("rust-block" ,rust-block-0.1)
                       ("rust-core-graphics" ,rust-core-graphics-0.5)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-objc" ,rust-objc-0.2))))))

(define-public rust-cocoa-0.5.2
  (package
    (inherit rust-cocoa-0.24)
    (name "rust-cocoa")
    (version "0.5.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cocoa" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1hadykx20yibn6w90ah0zbffr1af9yca8cx2xvq8wzmvk3yya6qy"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.7)
                       ("rust-block" ,rust-block-0.1)
                       ("rust-core-graphics" ,rust-core-graphics-0.4)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-objc" ,rust-objc-0.2))))))

(define-public rust-cocoa-0.3
  (package
    (inherit rust-cocoa-0.24)
    (name "rust-cocoa")
    (version "0.3.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cocoa" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1yrv988ysvmdgmgh4kdhgqr4afg70qkm8is09ckbzsy8pyq9nmac"))))
    (arguments
     `(#:skip-build? #t ;Requires MacOS
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.7)
                       ("rust-core-graphics" ,rust-core-graphics-0.3)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-objc" ,rust-objc-0.2))))))

(define-public rust-cocoa-0.2
  (package
    (inherit rust-cocoa-0.24)
    (name "rust-cocoa")
    (version "0.2.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cocoa" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "10lzgy15wdk2vq5bg6xa8iqsbh8b44159p839z9mimmgkrxi6bbn"))))
    (arguments
     `(#:skip-build? #t ;Requires MacOS
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.3)
                       ("rust-core-graphics" ,rust-core-graphics-0.3)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-objc" ,rust-objc-0.1))))))

(define-public rust-com-rs-0.2
  (package
    (name "rust-com-rs")
    (version "0.2.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "com-rs" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0hk6051kwpabjs2dx32qkkpy0xrliahpqfh9df292aa0fv2yshxz"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f)) ;Windows only!
    (home-page "https://github.com/Eljay/com-rs")
    (synopsis "Use the `com` crate instead")
    (description "This is deprecated; use the `com` crate instead.")
    (license (list license:expat license:asl2.0))))

(define-public rust-const-cstr-0.1
  (package
    (inherit rust-const-cstr-0.3)
    (name "rust-const-cstr")
    (version "0.1.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "const-cstr" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1pd7pkx25h3kw8991vip286lnvay14amqsmfyyn6d8dkfzcg86h3"))))
    (arguments
     `(#:cargo-inputs (("rust-libc" ,rust-libc-0.1))))))

(define-public rust-coreaudio-rs-0.10
  (package
    (name "rust-coreaudio-rs")
    (version "0.10.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "coreaudio-rs" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "125d4zr3n363ybga4629p41ym7iqjfb2alnwrc1zj7zyxch4p28i"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t ;Requires proprietary platform IOS/macOS
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-coreaudio-sys" ,rust-coreaudio-sys-0.2))))
    (home-page "https://github.com/RustAudio/coreaudio-rs.git")
    (synopsis "A friendly rust interface for Apple's CoreAudio API")
    (description
     "This crate aims to expose and wrap the functionality of the original C
API in a zero-cost, safe, Rust-esque manner.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-coreaudio-rs-0.9
  (package
    (inherit rust-coreaudio-rs-0.10)
    (name "rust-coreaudio-rs")
    (version "0.9.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "coreaudio-rs" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "14g4yqsbhif2bqdk4qk0lixfy78gl1p8lrl122qyklysclcpcagj"))))))

(define-public rust-coreaudio-rs-0.5
  (package
    (inherit rust-coreaudio-rs-0.10)
    (name "rust-coreaudio-rs")
    (version "0.5.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "coreaudio-rs" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1njbj52xr8l96b9iy1g7xxmr6k2wjv766b0gm39w27rqnhfdv5vz"))))
    (arguments
     `(#:skip-build? #t ;Requires proprietary platform IOS/macOS
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.3)
                       ("rust-coreaudio-sys" ,rust-coreaudio-sys-0.1)
                       ("rust-libc" ,rust-libc-0.2))))))

(define-public rust-coreaudio-sys-0.2
  (package
    (name "rust-coreaudio-sys")
    (version "0.2.12")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "coreaudio-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "091b4sq3kl8n4dy86l4mxq9vjzsn8w8b51xzfcpxwjkciqjv4d7h"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t ;Requires proprietary platform IOS/macOS
       #:cargo-inputs (("rust-bitflags" ,rust-bindgen-0.64))))
    (home-page "https://github.com/RustAudio/coreaudio-rs.git")
    (synopsis
     "Bindings for Apple's CoreAudio frameworks generated via rust-bindgen")
    (description
     "Raw bindings to Apple's Core Audio API for macos and iOS generated
using rust-bindgen. coreaudio-rs is an attempt at offering a higher
level API around this crate.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-coreaudio-sys-0.1
  (package
    (inherit rust-coreaudio-sys-0.2)
    (name "rust-coreaudio-sys")
    (version "0.1.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "coreaudio-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0i6dh6sz1f4xrs3caq7sg20cgw7wsqk6zbqin96d2k1acabih8ri"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t ;Requires proprietary platform IOS/macOS
       #:cargo-inputs (("rust-libc" ,rust-libc-0.2))))))

(define-public rust-core-foundation-0.5
  (package
    (inherit rust-core-foundation-0.9)
    (name "rust-core-foundation")
    (version "0.5.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "core-foundation" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1079vjg9x9yqmr1np5dk0zyikmc50ll0l0666rjs43d2qd0hnvi8"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-chrono" ,rust-chrono-0.4)
                       ("rust-core-foundation-sys" ,rust-core-foundation-sys-0.5)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-uuid" ,rust-uuid-0.5))))))

(define-public rust-core-foundation-0.4
  (package
    (inherit rust-core-foundation-0.9)
    (name "rust-core-foundation")
    (version "0.4.6")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "core-foundation" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "03lwv9frmbmh8400maqxpzjd1wr15y6yyxfx3idx8mk8rm3zaiw0"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-chrono" ,rust-chrono-0.4)
                       ("rust-core-foundation-sys" ,rust-core-foundation-sys-0.4)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-uuid" ,rust-uuid-0.5))))))

(define-public rust-core-foundation-sys-0.4
  (package
    (inherit rust-core-foundation-sys-0.8)
    (name "rust-core-foundation-sys")
    (version "0.4.6")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "core-foundation-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1mdwish2fvb0y4kbcrf4v1gf9s2fklp6g5ci2yl9fr1f3919a88m"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-libc" ,rust-libc-0.2))))))

(define-public rust-core-foundation-0.3
  (package
    (inherit rust-core-foundation-0.9)
    (name "rust-core-foundation")
    (version "0.3.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "core-foundation" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1hss3w89rnxwk75knayksbmsn59caxbynca2w5nwa4g3xfwf677m"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-core-foundation-sys" ,rust-core-foundation-sys-0.3)
                       ("rust-libc" ,rust-libc-0.2))))))

(define-public rust-core-foundation-sys-0.5
  (package
    (inherit rust-core-foundation-sys-0.8)
    (name "rust-core-foundation-sys")
    (version "0.5.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "core-foundation-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1yiyi30bnlnh29i21gp5f411b4qaj05vc8zp8j1y9b0khqg2fv3i"))))
    (arguments
     `(#:cargo-inputs (("rust-libc" ,rust-libc-0.2))))))

(define-public rust-core-foundation-sys-0.3
  (package
    (inherit rust-core-foundation-sys-0.8)
    (name "rust-core-foundation-sys")
    (version "0.3.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "core-foundation-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0956j1zfdycmchhkym8q73azmlbib7r770qlk3pybqfklmm5l4a1"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-libc" ,rust-libc-0.2))))))

(define-public rust-core-graphics-0.3
  (package
    (inherit rust-core-graphics-0.22)
    (name "rust-core-graphics")
    (version "0.3.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "core-graphics" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "08y7si6hiwm25g4a9vdcqzhwpgkqaxabwcfjlyxfsam25c1ccmhc"))))
    (arguments
     `(#:skip-build? #t ;macOS only!
       #:cargo-inputs (("rust-core-foundation" ,rust-core-foundation-0.2)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-serde" ,rust-serde-0.7))))))

(define-public rust-core-text-13
  (package
    (inherit rust-core-text-19)
    (name "rust-core-text")
    (version "13.3.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "core-text" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0v9lxn277p39cf81pb45r7k0lzf17pwgd5cpry1c04ajv556b16v"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-core-foundation" ,rust-core-foundation-0.6)
                       ("rust-core-graphics" ,rust-core-graphics-0.17)
                       ("rust-foreign-types" ,rust-foreign-types-0.3)
                       ("rust-libc" ,rust-libc-0.2))))))

(define-public rust-cpal-0.14
  (package
    (name "rust-cpal")
    (version "0.14.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cpal" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "02rdb90fj2fa787322k1jq6qalxz4sbikwjgb19rjphq7svc2hpk"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-alsa" ,rust-alsa-0.6)
                       ("rust-coreaudio-rs" ,rust-coreaudio-rs-0.10)
                       ("rust-core-foundation-sys" ,rust-core-foundation-sys-0.8)
                       ("rust-jni" ,rust-jni-0.19)
                       ("rust-js-ys" ,rust-js-sys-0.3)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-mach" ,rust-mach-0.3)
                       ("rust-ndk" ,rust-ndk-0.7)
                       ("rust-ndk-context" ,rust-ndk-context-0.1)
                       ("rust-oboe" ,rust-oboe-0.4)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-parking-lot" ,rust-parking-lot-0.12)
                       ("rust-stdweb" ,rust-stdweb-0.4)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-web-sys" ,rust-web-sys-0.3)
                       ("rust-windows" ,rust-windows-0.37)
                       ("rust-asio-sys" ,rust-asio-sys-0.2)
                       ("rust-jack" ,rust-jack-0.9)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2))
       #:cargo-development-inputs (("rust-anyhow" ,rust-anyhow-1)
                                   ("rust-clap" ,rust-clap-3)
                                   ("rust-hound" ,rust-hound-3)
                                   ("rust-ndk-glue" ,rust-ndk-glue-0.7)
                                   ("rust-ringbuf" ,rust-ringbuf-0.2))))
    (inputs (list alsa-lib))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/rustaudio/cpal")
    (synopsis "Low-level cross-platform audio I/O library in pure Rust.")
    (description "Low-level library for audio input and output in pure Rust.")
    (license license:asl2.0)))

(define-public rust-cpal-0.8
  (package
    (inherit rust-cpal-0.14)
    (name "rust-cpal")
    (version "0.8.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cpal" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "17l298jyp4lanl0igxp30m6xnv84gacvdbp3ylrv5c9ncpny32nm"))))
    (arguments
     `(#:cargo-inputs (("rust-alsa-sys" ,rust-alsa-sys-0.1)
                       ("rust-core-foundation-sys" ,rust-core-foundation-sys-0.5)
                       ("rust-coreaudio-rs" ,rust-coreaudio-rs-0.9)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-stdweb" ,rust-stdweb-0.1)
                       ("rust-winapi" ,rust-winapi-0.3))
       #:cargo-development-inputs (("rust-hound" ,rust-hound-3))))))

(define-public rust-cpal-0.2
  (package
    (inherit rust-cpal-0.14)
    (name "rust-cpal")
    (version "0.2.12")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cpal" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "04xplav35z7n0jpv2xlyg3n9jilna0z6qhs6c1vihpy6b56zd7cq"))))
    (arguments
     `(#:skip-build? #t ;cross-platform and cannot fetch dependencies
       #:cargo-inputs (("rust-alsa-sys" ,rust-alsa-sys-0.3)
                       ("rust-coreaudio-rs" ,rust-coreaudio-rs-0.5)
                       ("rust-lazy-static" ,rust-lazy-static-0.2)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-ole32-sys" ,rust-ole32-sys-0.2)
                       ("rust-winapi" ,rust-winapi-0.2))
       #:cargo-development-inputs (("rust-vorbis" ,rust-vorbis-0.1))))))

(define-public rust-criterion-0.5
  (package
    (name "rust-criterion")
    (version "0.5.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "criterion" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0bv9ipygam3z8kk6k771gh9zi0j0lb9ir0xi1pc075ljg80jvcgj"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-anes" ,rust-anes-0.1)
                       ("rust-async-std" ,rust-async-std-1)
                       ("rust-cast" ,rust-cast-0.3)
                       ("rust-ciborium" ,rust-ciborium-0.2)
                       ("rust-clap" ,rust-clap-4)
                       ("rust-criterion-plot" ,rust-criterion-plot-0.5)
                       ("rust-csv" ,rust-csv-1)
                       ("rust-futures" ,rust-futures-0.3)
                       ("rust-is-terminal" ,rust-is-terminal-0.4)
                       ("rust-itertools" ,rust-itertools-0.10)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-oorandom" ,rust-oorandom-11.1)
                       ("rust-plotters" ,rust-plotters-0.3)
                       ("rust-rayon" ,rust-rayon-1)
                       ("rust-regex" ,rust-regex-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-derive" ,rust-serde-derive-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-smol" ,rust-smol-1)
                       ("rust-tinytemplate" ,rust-tinytemplate-1)
                       ("rust-tokio" ,rust-tokio-1)
                       ("rust-walkdir" ,rust-walkdir-2))
       #:cargo-development-inputs (("rust-approx" ,rust-approx-0.5)
                                   ("rust-futures" ,rust-futures-0.3)
                                   ("rust-quickcheck" ,rust-quickcheck-1)
                                   ("rust-rand" ,rust-rand-0.8)
                                   ("rust-tempfile" ,rust-tempfile-3))))
    (home-page "https://bheisler.github.io/criterion.rs/book/index.html")
    (synopsis "Statistics-driven micro-benchmarking library")
    (description
     "This package provides a statistics-driven micro-benchmarking library.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-crossbeam-0.4
  (package
    (inherit rust-crossbeam-0.8)
    (name "rust-crossbeam")
    (version "0.4.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "crossbeam" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0l1nsd3gs3fwfkz36d43fi18m6ldbw66g2zjj044hzxqn53q4h6p"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-crossbeam-channel" ,rust-crossbeam-channel-0.2)
                       ("rust-crossbeam-deque" ,rust-crossbeam-deque-0.5)
                       ("rust-crossbeam-epoch" ,rust-crossbeam-epoch-0.5)
                       ("rust-crossbeam-utils" ,rust-crossbeam-utils-0.5))
       #:cargo-development-inputs (("rust-rand" ,rust-rand-0.5))))))

(define-public rust-crossbeam-0.3
  (package
    (inherit rust-crossbeam-0.8)
    (name "rust-crossbeam")
    (version "0.3.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "crossbeam" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "06ga3l3lavn1vya3r6v4rg08y6m2ccc4qskacis3difmsj19gki4"))))
    (arguments
     `(#:tests? #f
       #:cargo-development-inputs (("rust-rand" ,rust-rand-0.3))))))

(define-public rust-crossbeam-0.2
  (package
    (inherit rust-crossbeam-0.8)
    (name "rust-crossbeam")
    (version "0.2.12")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "crossbeam" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1gmrayc93lygb8v62bj0c5zwyflvj5bli7ari650k259nlyncrmx"))))
    (arguments
     `(#:tests? #f
       #:cargo-development-inputs (("rust-rand" ,rust-rand-0.3))))))

(define-public rust-crossbeam-channel-0.2
  (package
    (inherit rust-crossbeam-channel-0.5)
    (name "rust-crossbeam-channel")
    (version "0.2.6")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "crossbeam-channel" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "09wqcjs4nq0cqs0x0ylvpzwbg1gndsc1833ybsy61wdpc4bp91bv"))))
    (arguments
     `(#:cargo-inputs (("rust-crossbeam-epoch" ,rust-crossbeam-epoch-0.6)
                       ("rust-crossbeam-utils" ,rust-crossbeam-utils-0.5)
                       ("rust-parking-lot" ,rust-parking-lot-0.6)
                       ("rust-rand" ,rust-rand-0.5)
                       ("rust-smallvec" ,rust-smallvec-0.6))
       #:cargo-development-inputs (("rust-crossbeam" ,rust-crossbeam-0.3))))))

(define-public rust-crossbeam-deque-0.5
  (package
    (inherit rust-crossbeam-deque-0.8)
    (name "rust-crossbeam-deque")
    (version "0.5.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "crossbeam-deque" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "16nb554hxn35s9p0j6is5i6w5b2sjkfs6a1p9rjjy8m4nnlw94kp"))))
    (arguments
     `(#:cargo-inputs (("rust-crossbeam-epoch" ,rust-crossbeam-epoch-0.5)
                       ("rust-crossbeam-utils" ,rust-crossbeam-utils-0.5))
       #:cargo-development-inputs (("rust-rand" ,rust-rand-0.5))))))

(define-public rust-crossbeam-epoch-0.6
  (package
    (inherit rust-crossbeam-epoch-0.9)
    (name "rust-crossbeam-epoch")
    (version "0.6.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "crossbeam-epoch" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1f57zhpcmvlgwshw1kp1ka773prmp4j61cadn9gnxybyxjjalj94"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-arrayvec" ,rust-arrayvec-0.4)
                       ("rust-cfg-if" ,rust-cfg-if-0.1)
                       ("rust-crossbeam-utils" ,rust-crossbeam-utils-0.6)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-memoffset" ,rust-memoffset-0.2)
                       ("rust-scopeguard" ,rust-scopeguard-0.3))
       #:cargo-development-inputs (("rust-rand" ,rust-rand-0.5))))))

(define-public rust-crossbeam-epoch-0.5
  (package
    (inherit rust-crossbeam-epoch-0.9)
    (name "rust-crossbeam-epoch")
    (version "0.5.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "crossbeam-epoch" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1s9n0mlf5zfa2hrj7jrb5h8xqklypi5bxy2i25vzizmbqv5czzih"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-arrayvec" ,rust-arrayvec-0.4)
                       ("rust-cfg-if" ,rust-cfg-if-0.1)
                       ("rust-crossbeam-utils" ,rust-crossbeam-utils-0.5)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-memoffset" ,rust-memoffset-0.2)
                       ("rust-scopeguard" ,rust-scopeguard-0.3))
       #:cargo-development-inputs (("rust-rand" ,rust-rand-0.5))))))

(define-public rust-crossbeam-utils-0.5
  (package
    (inherit rust-crossbeam-utils-0.8)
    (name "rust-crossbeam-utils")
    (version "0.5.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "crossbeam-utils" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "05b00dr1srl94d7jkalhwxdvpkaby2f8p3m37y8jpgg82wx4azb7"))))
    (arguments
     `())))

(define-public rust-csv-0.15
  (package
    (inherit rust-csv-1)
    (name "rust-csv")
    (version "0.15.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "csv" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "173dv65zmmm4kfqnjk66vhgj4w82vh9c14jq6r55c755qwvjpwky"))))
    (arguments
     `(#:cargo-inputs (("rust-byteorder" ,rust-byteorder-1)
                       ("rust-memchr" ,rust-memchr-1)
                       ("rust-rustc-serialize" ,rust-rustc-serialize-0.3))
       #:cargo-development-inputs (("rust-regex" ,rust-regex-0.2))))))

(define-public rust-dbghelp-sys-0.2
  (package
    (name "rust-dbghelp-sys")
    (version "0.2.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "dbghelp-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0l5qqm7pqr4i4lq67ymkix5gvl94m51sj70ng61c52nb7fjhnncp"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t ;Can't find source winapi
       #:cargo-inputs (("rust-winapi" ,rust-winapi-0.2)
                       ("rust-winapi-build" ,rust-winapi-build-0.1))))
    (home-page "https://github.com/retep998/winapi-rs")
    (synopsis
     "Contains function definitions for the Windows API library dbghelp")
    (description
     "This package contains function definitions for the Windows API library
dbghelp. ee winapi for types and constants.")
    (license license:expat)))

(define-public rust-debug-builders-0.1
  (package
    (name "rust-debug-builders")
    (version "0.1.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "debug-builders" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "04ngq10zjy2hp3v2di8bgaksvikkj4l4fwcxlnlb5g6a2hyqwp8g"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/sfackler/rust-debug-builders")
    (synopsis
     "A copy of libstd's debug builders for use before they stabilize")
    (description
     "This package provides a copy of libstd's debug builders for use before
they stabilize.")
    (license (list license:expat license:asl2.0))))

(define-public rust-derivative-1
  (package
    (inherit rust-derivative-2)
    (name "rust-derivative")
    (version "1.0.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "derivative" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0dbiw51q4b33gwhkicai06xym0hb6fkid9xn24h3x2k68qsqhv9w"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-0.4)
                       ("rust-quote" ,rust-quote-0.6)
                       ("rust-syn" ,rust-syn-0.15)
                       ("rust-trybuild" ,rust-trybuild-1))))))

(define-public rust-dlib-0.5
  (package
    (name "rust-dlib")
    (version "0.5.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "dlib" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "04m4zzybx804394dnqs1blz241xcy480bdwf3w9p4k6c3l46031k"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;FIXME: Several macros are not found.
       #:cargo-inputs (("rust-libloading" ,rust-libloading-0.7))))
    (inputs (list rust-libloading-0.7))
    (home-page "https://github.com/vberger/dlib")
    (synopsis "Helper macros for manually loading optional system libraries")
    (description
     "This package provides helper macros for handling manually loading optional
system libraries.")
    (license license:expat)))

(define-public rust-dlib-0.3
  (package
    (inherit rust-dlib-0.5)
    (name "rust-dlib")
    (version "0.3.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "dlib" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1v36322ilmdd1w3kh2xhm58ma9mxq9i4xdcwy84lav63w56cx2ql"))))
    (arguments
     `(#:cargo-inputs (("rust-libloading" ,rust-libloading-0.3))))))

(define-public rust-dlopen-0.1
  (package
    (name "rust-dlopen")
    (version "0.1.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "dlopen" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0dq9m3fqf194vaw1ldn3c5ssdj255l5dag2qd2z9ljl1kz9hms3i"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;Doesn't download the example dynamic library
       #:cargo-inputs (("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-dlopen-derive" ,rust-dlopen-derive-0.1)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-winapi" ,rust-winapi-0.3))
       #:cargo-development-inputs (("rust-const-cstr" ,rust-const-cstr-0.1)
                                   ("rust-example-dylib" ,rust-example-dylib-0.1)
                                   ("rust-libc" ,rust-libc-0.2)
                                   ("rust-regex" ,rust-regex-0.2))))
    (home-page "https://github.com/szymonwieloch/rust-dlopen")
    (synopsis "Library for opening and operating on dynamic link libraries")
    (description
     "Library for opening and operating on dynamic link libraries (also
known as shared objects or shared libraries). This is a modern and
more flexible alternative to the already existing libraries like
libloading or sharedlib.")
    (license license:expat)))

(define-public rust-dlopen-derive-0.1
  (package
    (name "rust-dlopen-derive")
    (version "0.1.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "dlopen-derive" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "10f58rcw1xnm5b5xg735ws0cvsz8qzfcigcw1zm1rn7vn7hxjdpj"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-quote" ,rust-quote-0.6)
                       ("rust-syn" ,rust-syn-0.15))))
    (home-page "https://github.com/szymonwieloch/rust-dlopen")
    (synopsis "Derive macros for the dlopen crate")
    (description "Derive macros for the dlopen crate.")
    (license license:expat)))

(define-public rust-dtoa-0.2
  (package
    (inherit rust-dtoa-0.4)
    (name "rust-dtoa")
    (version "0.2.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "dtoa" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0g96cap6si1g6wi62hsdk2fnj3sf5vd4i97zj6163j8hhnsl3n0d"))))))

(define-public rust-dwmapi-sys-0.1
  (package
    (name "rust-dwmapi-sys")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "dwmapi-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0xiyc8vibsda0kbamr9zkjvkdzdxcq8bs1g5mq4yc4mbmr168jxl"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-winapi" ,rust-winapi-0.2)
                       ("rust-winapi-build" ,rust-winapi-build-0.1))))
    (home-page "https://github.com/retep998/winapi-rs")
    (synopsis
     "Contains function definitions for the Windows API library dwmapi")
    (description
     "This package contains function definitions for the Windows API library
dwmapi. See winapi for types and constants.")
    (license license:expat)))

(define-public rust-dwrote-0.5
  (package
    (inherit rust-dwrote-0.11)
    (name "rust-dwrote")
    (version "0.5.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "dwrote" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0d0h68k9a67h5iqb0b64xrak89avmmhid37vzss6b7hdpaa2yizm"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-derive" ,rust-serde-derive-1)
                       ("rust-winapi" ,rust-winapi-0.3))))))

(define-public rust-dylib-0.0.3
  (package
    (name "rust-dylib")
    (version "0.0.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "dylib" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "07kkci6i50wks3d34m5hlql216j9f8sl7qgfcfri5a8k603i6v7h"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "Standalone version of former dylib module")
    (description
     "This package provides the standalone version of former dylib module")
    (license license:bsd-3)))

(define-public rust-dylib-0.0.1
  (package
    (inherit rust-dylib-0.0.3)
    (name "rust-dylib")
    (version "0.0.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "dylib" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1bj7bs0vm31yf9vvfagmdqa333vyhshbypmcqliizmv7qh7hgzyd"))))
    (license license:bsd-3)))
; Pulled from later versions

(define-public rust-enum-primitive-0.0.2
  (package
    (inherit rust-enum-primitive-0.1)
    (name "rust-enum-primitive")
    (version "0.0.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "enum-primitive" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "01pv73vfbxrzmf8qkmi5zip9kb4iz1y6f45rhr2y1dk78lyxh7ic"))))
    (arguments
     `(#:cargo-inputs (("rust-num" ,rust-num-0.1))))))

(define-public rust-enum-primitive-0.0.1
  (package
    (inherit rust-enum-primitive-0.0.2)
    (name "rust-enum-primitive")
    (version "0.0.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "enum-primitive" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1pgmpa1ga1vps7ag6jhxslalyljbgndvhfmalrcz87jxb69cp2q1"))))))

(define-public rust-evdev-0.11
  (package
    (name "rust-evdev")
    (version "0.11.7")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "evdev" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1zknxkgkyh9fx3mq4div9kcgvgsiy91vzd5sq7bdinsn467sfx65"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bitvec" ,rust-bitvec-1)
                       ("rust-cfg-if" ,rust-cfg-if-1)
                       ("rust-futures-core" ,rust-futures-core-0.3)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-nix" ,rust-nix-0.23)
                       ("rust-paste" ,rust-paste-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-tokio" ,rust-tokio-1))
       #:cargo-development-inputs (("rust-itertools" ,rust-itertools-0.10)
                                   ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://github.com/cmr/evdev")
    (synopsis "evdev interface for Linux")
    (description "evdev interface for Linux")
    (license (list license:asl2.0 license:expat))))

(define-public rust-example-dylib-0.1
  (package
    (name "rust-example-dylib")
    (version "0.1.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "example_dylib" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0rib81plaxx6czdm1zgnm24bjzb24id3dpn4ysl10aakbyny6p98"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-libc" ,rust-libc-0.2))))
    (home-page "https://github.com/szymonwieloch/rust-dlopen")
    (synopsis
     "Example dynamic link library for executing tests of libraries that
load and operate on dynamic link libraries")
    (description
     "Example dynamic link library for executing tests of libraries that
load and operate on dynamic link libraries.")
    (license license:expat)))

(define-public rust-fern-0.5
  (package
    (inherit rust-fern-0.6)
    (name "rust-fern")
    (version "0.5.9")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "fern" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1anslk0hx9an4ypcaxqff080hgbcxm7ji7d4qf4f6qx1mkav16p6"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-chrono" ,rust-chrono-0.4)
                       ("rust-colored" ,rust-colored-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-reopen" ,rust-reopen-0.3)
                       ("rust-syslog" ,rust-syslog-4)
                       ("rust-syslog" ,rust-syslog-3))
       #:cargo-development-inputs (("rust-clap" ,rust-clap-2)
                                   ("rust-tempdir" ,rust-tempdir-0.3))))))

(define-public rust-fetch-unroll-0.3
  (package
    (name "rust-fetch-unroll")
    (version "0.3.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "fetch-unroll" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1l3cf8fhcrw354hdmjf03f5v4bxgn2wkjna8n0fn8bgplh8b3666"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t ;Failed custom build command for rust-ring-0.16
       #:cargo-inputs (("rust-libflate" ,rust-libflate-1)
                       ("rust-tar" ,rust-tar-0.4)
                       ("rust-ureq" ,rust-ureq-2))))
    (home-page "https://github.com/katyo/fetch_unroll")
    (synopsis "Simple utilities for fetching and unrolling .tar.gz archives")
    (description
     "Simple functions intended to use in Rust @code{build.rs} scripts for tasks
which related to fetching from HTTP and unrolling @code{.tar.gz} archives
with precompiled binaries and etc.")
    (license license:asl2.0)))

(define-public rust-flate2-0.2
  (package
    (inherit rust-flate2-1)
    (name "rust-flate2")
    (version "0.2.20")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "flate2" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "08z4h6y01gzg78wqmiirld8dqvv9b2qf4vxhvgid3rca8va4s8z6"))))
    (arguments
     `(#:cargo-inputs (("rust-futures" ,rust-futures-0.1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-libz-sys" ,rust-libz-sys-1)
                       ("rust-miniz-sys" ,rust-miniz-sys-0.1)
                       ("rust-tokio-io" ,rust-tokio-io-0.1))
       #:cargo-development-inputs (("rust-quickcheck" ,rust-quickcheck-0.4)
                                   ("rust-rand" ,rust-rand-0.3)
                                   ("rust-tokio-core" ,rust-tokio-core-0.1))))))

(define-public rust-float-next-after-0.1
  (package
    (name "rust-float-next-after")
    (version "0.1.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "float_next_after" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0cnn60pslh0gilpy2jr7qpqk22a6lmsdz847988bg1krhg2i5ijg"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-num-traits" ,rust-num-traits-0.2))))
    (home-page "https://gitlab.com/bronsonbdevost/next_afterf")
    (synopsis "Trait for native rust f64/f32 nextafter")
    (description
     "A native Rust next after float function, which is provided as a trait
for f32 and f64 types. It steps to the next representable floating
point, even if it is subnormal. If a subnormal is undesired, users
should handle that eventuality themselves.")
    (license license:expat)))

(define-public rust-fluent-0.4
  (package
    (name "rust-fluent")
    (version "0.4.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "fluent" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1zkrw52mrrqafixka3nsylxjkckb95qgabmjpdd7bn50pv4iwqz1"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-clap" ,rust-clap-2)
                       ("rust-failure" ,rust-failure-0.1)
                       ("rust-failure-derive" ,rust-failure-derive-0.1)
                       ("rust-fluent-locale" ,rust-fluent-locale-0.4)
                       ("rust-fluent-syntax" ,rust-fluent-syntax-0.1)
                       ("rust-intl-pluralrules" ,rust-intl-pluralrules-1))))
    (home-page "http://www.projectfluent.org")
    (synopsis
     "A localization system designed to unleash the entire expressive power of
natural language translations.")
    (description
     "This package provides a localization system designed to unleash the entire
expressive power of natural language translations.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-fluent-locale-0.4
  (package
    (name "rust-fluent-locale")
    (version "0.4.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "fluent-locale" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "00by5i9v9myldqwbvvm4027l15c4bcahnj0n0lws0ls1il7shm2a"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-development-inputs (("rust-serde" ,rust-serde-1)
                                   ("rust-serde-derive" ,rust-serde-derive-1)
                                   ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "http://projectfluent.org/")
    (synopsis "A library for language and locale negotiation.")
    (description
     "This package provides a library for language and locale negotiation.")
    (license license:asl2.0)))

(define-public rust-fluent-syntax-0.1
  (package
    (name "rust-fluent-syntax")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "fluent-syntax" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1x65ag6ma09s5vzbls906j66sng2wm4rwq17mfziy3dssp8m44xl"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-annotate-snippets" ,rust-annotate-snippets-0.1))
       #:cargo-development-inputs (("rust-glob" ,rust-glob-0.2))))
    (home-page "http://www.projectfluent.org")
    (synopsis "Parser/Serializer tools for Fluent Syntax")
    (description
     "This package provides Parser/Serializer tools for Fluent Syntax.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-font-kit-0.11
  (package
    (name "rust-font-kit")
    (version "0.11.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "font-kit" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1rbcgpjvjs7vlfx5g3kww1qsp2djy1838ymcx7x8a41p9m82izi1"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;Cannot find test file
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-byteorder" ,rust-byteorder-1)
                       ("rust-core-foundation" ,rust-core-foundation-0.9)
                       ("rust-core-graphics" ,rust-core-graphics-0.22)
                       ("rust-core-text" ,rust-core-text-19)
                       ("rust-dirs-next" ,rust-dirs-next-2)
                       ("rust-dwrote" ,rust-dwrote-0.11)
                       ("rust-float-ord" ,rust-float-ord-0.2)
                       ("rust-freetype" ,rust-freetype-0.7)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-pathfinder-geometry" ,rust-pathfinder-geometry-0.5)
                       ("rust-pathfinder-simd" ,rust-pathfinder-simd-0.5)
                       ("rust-walkdir" ,rust-walkdir-2)
                       ("rust-winapi" ,rust-winapi-0.3)
                       ("rust-yeslogic-fontconfig-sys" ,rust-yeslogic-fontconfig-sys-3))
       #:cargo-development-inputs (("rust-clap" ,rust-clap-2)
                                   ("rust-colored" ,rust-colored-1)
                                   ("rust-pbr" ,rust-pbr-1)
                                   ("rust-prettytable-rs" ,rust-prettytable-rs-0.8))))
    (inputs (list fontconfig))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/servo/font-kit")
    (synopsis "Cross-platform font loading library")
    (description
     "This package provides a cross-platform font loading library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-font-kit-0.1
  (package
    (inherit rust-font-kit-0.11)
    (name "rust-font-kit")
    (version "0.1.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "font-kit" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0nbq6i98swbnci8irgakaq7d2wn13nwd8nzxd83j0a8fb0xi7q2v"))))
    (arguments
     `(#:cargo-inputs (("rust-arrayvec" ,rust-arrayvec-0.4)
                       ("rust-bitflags" ,rust-bitflags-1)
                       ("rust-byteorder" ,rust-byteorder-1)
                       ("rust-cocoa" ,rust-cocoa-0.18)
                       ("rust-core-foundation" ,rust-core-foundation-0.6)
                       ("rust-core-graphics" ,rust-core-graphics-0.17)
                       ("rust-core-text" ,rust-core-text-13)
                       ("rust-dirs" ,rust-dirs-1)
                       ("rust-dwrote" ,rust-dwrote-0.5)
                       ("rust-euclid" ,rust-euclid-0.19)
                       ("rust-failure" ,rust-failure-0.1)
                       ("rust-float-ord" ,rust-float-ord-0.2)
                       ("rust-freetype" ,rust-freetype-0.4)
                       ("rust-itertools" ,rust-itertools-0.7)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-lyon-path" ,rust-lyon-path-0.12)
                       ("rust-memmap" ,rust-memmap-0.6)
                       ("rust-servo-fontconfig" ,rust-servo-fontconfig-0.4)
                       ("rust-walkdir" ,rust-walkdir-2)
                       ("rust-winapi" ,rust-winapi-0.3))
       #:cargo-development-inputs (("rust-clap" ,rust-clap-2)
                                   ("rust-colored" ,rust-colored-1)
                                   ("rust-pbr" ,rust-pbr-1)
                                   ("rust-prettytable-rs" ,rust-prettytable-rs-0.7))))))

(define-public rust-freetype-0.4
  (package
    (inherit rust-freetype-0.7)
    (name "rust-freetype")
    (version "0.4.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "freetype" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0a70x03n68997f08bi3n47q9wyi3pv5s9v4rjc79sihb84mnp4hi"))))
    (arguments
     `(#:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-servo-freetype-sys" ,rust-servo-freetype-sys-4))))
    (native-inputs (list cmake))))

(define-public rust-futures-intrusive-0.4
  (package
    (name "rust-futures-intrusive")
    (version "0.4.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "futures-intrusive" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1mdf2qb6ayfi19l7qbqi7zp62wyyibfgmc93flrh70dziykgf156"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-futures-core" ,rust-futures-core-0.3)
                       ("rust-lock-api" ,rust-lock-api-0.4)
                       ("rust-parking-lot" ,rust-parking-lot-0.11))
       #:cargo-development-inputs (("rust-async-std" ,rust-async-std-1)
                                   ("rust-criterion" ,rust-criterion-0.3)
                                   ("rust-crossbeam" ,rust-crossbeam-0.7)
                                   ("rust-futures" ,rust-futures-0.3)
                                   ("rust-futures-test" ,rust-futures-test-0.3)
                                   ("rust-lazy-static" ,rust-lazy-static-1)
                                   ("rust-pin-utils" ,rust-pin-utils-0.1)
                                   ("rust-rand" ,rust-rand-0.7)
                                   ("rust-signal-hook" ,rust-signal-hook-0.1)
                                   ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://github.com/Matthias247/futures-intrusive")
    (synopsis "Futures based on intrusive data structures")
    (description
     "This crate provides a variety of Futures-based and
@code{async/await} compatible types that are based on the idea of
intrusive collections.")
    (license (list license:expat license:asl2.0))))

(define-public rust-gdi32-sys-0.1
  (package
    (inherit rust-gdi32-sys-0.2)
    (name "rust-gdi32-sys")
    (version "0.1.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gdi32-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "04bcwaia0q46k4rajwpivdvlfyc2gw5vnvkbz247hlh724nbjglf"))))
    (arguments
     `(#:cargo-inputs (("rust-winapi" ,rust-winapi-0.2)
                       ("rust-winapi-build" ,rust-winapi-build-0.1))))))

(define-public rust-generic-array-0.11
  (package
    (inherit rust-generic-array-0.14)
    (name "rust-generic-array")
    (version "0.11.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "generic-array" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0a7w8w0rg47nmcinnfzv443lcyb8mplwc251p1jyr5xj2yh6wzv6"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-serde" ,rust-serde-1)
                       ("rust-typenum" ,rust-typenum-1))
       #:cargo-development-inputs (("rust-serde-json" ,rust-serde-json-1))))))

(define-public rust-generic-array-0.5
  (package
    (inherit rust-generic-array-0.14)
    (name "rust-generic-array")
    (version "0.5.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "generic-array" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0pmc6l9jk07f52y6d1cqzjnq3jpkrd0v9w6lbk4hzgrzzvgvhgss"))))
    (arguments
     `(#:cargo-inputs (("rust-nodrop" ,rust-nodrop-0.1)
                       ("rust-serde" ,rust-serde-0.8)
                       ("rust-typenum" ,rust-typenum-1))
       #:cargo-development-inputs (("rust-serde-json" ,rust-serde-json-0.8))))))

(define-public rust-gilrs-0.9
  (package
    (name "rust-gilrs")
    (version "0.9.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gilrs" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0pv8g66k6yvwsb21gd2bgkjb2wbp3bx5syg3qfh7pspqgg1sfsqx"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-fnv" ,rust-fnv-1)
                       ("rust-gilrs-core" ,rust-gilrs-core-0.4)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-uuid" ,rust-uuid-1)
                       ("rust-vec-map" ,rust-vec-map-0.8)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-env-logger" ,rust-env-logger-0.9))))
    (inputs (list eudev))
    (native-inputs (list pkg-config))
    (home-page "https://gitlab.com/gilrs-project/gilrs")
    (synopsis "Game Input Library for Rust")
    (description "GilRs abstract platform specific APIs to provide unified
 interfaces for working with gamepads.")
    (license (list license:expat license:asl2.0))))

(define-public rust-gilrs-core-0.4
  (package
    (name "rust-gilrs-core")
    (version "0.4.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gilrs-core" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "00kxkhwx7fi10zll0wfcbgw3g4m4wp7s822fi5zd5by5gx5dka4n"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-core-foundation" ,rust-core-foundation-0.9)
                       ("rust-io-kit-sys" ,rust-io-kit-sys-0.2)
                       ("rust-js-sys" ,rust-js-sys-0.3)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-libudev-sys" ,rust-libudev-sys-0.1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-nix" ,rust-nix-0.24)
                       ("rust-rusty-xinput" ,rust-rusty-xinput-1)
                       ("rust-uuid" ,rust-uuid-1)
                       ("rust-vec-map" ,rust-vec-map-0.8)
                       ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
                       ("rust-web-sys" ,rust-web-sys-0.3)
                       ("rust-winapi" ,rust-winapi-0.3)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-env-logger" ,rust-env-logger-0.9))))
    (inputs (list eudev))
    (native-inputs (list pkg-config))
    (home-page "https://gitlab.com/gilrs-project/gilrs")
    (synopsis "Minimal event based abstraction for working with gamepads")
    (description
     "This library is minimal event based abstraction for working with
gamepads. If you are looking for something more hight level, take a
look at gilrs crate.")
    (license (list license:expat license:asl2.0))))

(define-public rust-half-1.8
  (package
    (name "rust-half")
    (version "1.8.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "half" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1mqbmx2m9qd4lslkb42fzgldsklhv9c4bxsc8j82r80d8m24mfza"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-crunchy" ,rust-crunchy-0.2)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-zerocopy" ,rust-zerocopy-0.6))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.4)
                                   ("rust-crunchy" ,rust-crunchy-0.2)
                                   ("rust-quickcheck" ,rust-quickcheck-1)
                                   ("rust-quickcheck-macros" ,rust-quickcheck-macros-1)
                                   ("rust-rand" ,rust-rand-0.8))))
    (home-page "https://github.com/starkat99/half-rs")
    (synopsis "Half-precision floating point f16 type")
    (description
     "Half-precision floating point f16 type for Rust implementing the
IEEE 754-2008 binary16 type.")
    (license (list license:expat license:asl2.0))))

(define-public rust-hetseq-0.2
  (package
    (name "rust-hetseq")
    (version "0.2.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "hetseq" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1bm6vnwsnyb6fi99ykqi7nasfm3dkgwbgzpbqjcwjpf4k2kyi337"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-rayon" ,rust-rayon-1)
                       ("rust-shred" ,rust-shred-0.5))))
    (home-page "https://github.com/omni-viral/hetseq")
    (synopsis "Defines traits and types to work with heterogenous sequences")
    (description
     "This package defines traits and types to work with heterogenous
sequences")
    (license (list license:asl2.0 license:expat))))

(define-public rust-hexf-parse-0.2
  (package
    (name "rust-hexf-parse")
    (version "0.2.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "hexf-parse" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1pr3a3sk66ddxdyxdxac7q6qaqjcn28v0njy22ghdpfn78l8d9nz"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/lifthrasiir/hexf")
    (synopsis "Parses hexadecimal floats (see also hexf)")
    (description
     "This package provides parsing for hexadecimal floats (see also hexf).")
    (license license:cc0)))

(define-public rust-hibitset-0.5
  (package
    (name "rust-hibitset")
    (version "0.5.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "hibitset" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1l4jb5xd5xvmrymd1b4ajwv9p88ppyr78a2pqwk3j39fyf4bq9v5"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-atom" ,rust-atom-0.3)
                       ("rust-rayon" ,rust-rayon-1))
       #:cargo-development-inputs (("rust-rand" ,rust-rand-0.6))))
    (home-page "https://github.com/slide-rs/hibitset")
    (synopsis "Hierarchical bit set structure")
    (description "This package provides a hierarchical bit set structure.")
    (license (list license:expat license:asl2.0))))

(define-public rust-hound-3
  (package
    (name "rust-hound")
    (version "3.5.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "hound" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1cadkxzdsb3bxwzri6r6l78a1jy9j0jxrfwmh34gjadvbnyws4sd"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-development-inputs (("rust-cpal" ,rust-cpal-0.2))))
    (inputs (list alsa-lib))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/starkat99/half-rs")
    (synopsis "A wav encoding and decoding library in Rust")
    (description
     "Hound can read and write the WAVE audio format, an ubiquitous format
for raw, uncompressed audio.")
    (license license:asl2.0)))

(define-public rust-hyper-0.11
  (package
    (inherit rust-hyper-0.14)
    (name "rust-hyper")
    (version "0.11.27")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "hyper" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1msrx9fgiiv7vl4kryn2zgahbqndph5szrgqvm6fjhfk1759199l"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-base64" ,rust-base64-0.9)
                       ("rust-bytes" ,rust-bytes-0.4)
                       ("rust-futures" ,rust-futures-0.1)
                       ("rust-futures-cpupool" ,rust-futures-cpupool-0.1)
                       ("rust-http" ,rust-http-0.1)
                       ("rust-httparse" ,rust-httparse-1)
                       ("rust-iovec" ,rust-iovec-0.1)
                       ("rust-language-tags" ,rust-language-tags-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-mime" ,rust-mime-0.3)
                       ("rust-net2" ,rust-net2-0.2)
                       ("rust-percent-encoding" ,rust-percent-encoding-1)
                       ("rust-relay" ,rust-relay-0.1)
                       ("rust-time" ,rust-time-0.1)
                       ("rust-tokio-core" ,rust-tokio-core-0.1)
                       ("rust-tokio-io" ,rust-tokio-io-0.1)
                       ("rust-tokio-proto" ,rust-tokio-proto-0.1)
                       ("rust-tokio-service" ,rust-tokio-service-0.1)
                       ("rust-unicase" ,rust-unicase-2)
                       ("rust-want" ,rust-want-0.0))
       #:cargo-development-inputs (("rust-num-cpus" ,rust-num-cpus-1)
                                   ("rust-pretty-env-logger" ,rust-pretty-env-logger-0.2)
                                   ("rust-spmc" ,rust-spmc-0.2)
                                   ("rust-url" ,rust-url-1))))))

(define-public rust-hyper-tls-0.1
  (package
    (inherit rust-hyper-tls-0.5)
    (name "rust-hyper-tls")
    (version "0.1.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "hyper-tls" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0y13a98grzvgza9wpql0gmghwhp48jzxn5dk1a26ac4da5gbvcgz"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-futures" ,rust-futures-0.1)
                       ("rust-hyper" ,rust-hyper-0.11)
                       ("rust-native-tls" ,rust-native-tls-0.1)
                       ("rust-tokio-core" ,rust-tokio-core-0.1)
                       ("rust-tokio-io" ,rust-tokio-io-0.1)
                       ("rust-tokio-service" ,rust-tokio-service-0.1)
                       ("rust-tokio-tls" ,rust-tokio-tls-0.1))))))

(define-public rust-inflate-0.1
  (package
    (inherit rust-inflate-0.4)
    (name "rust-inflate")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "inflate" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1fs3gkcfypag11bskzghka8gz8p8d8qra3bm2cppvwf25lnhdq77"))))
    (arguments
     `())))

(define-public rust-intl-pluralrules-1
  (package
    (name "rust-intl-pluralrules")
    (version "1.1.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "intl-pluralrules" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "11l23sjaxzh7dr65hrnwb2bjlqp8x2dqjqmabqzflzs155jf5c2q"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-matches" ,rust-matches-0.1)
                       ("rust-phf" ,rust-phf-0.7)
                       ("rust-unic-langid" ,rust-unic-langid-0.3))))
    (home-page "https://github.com/zbraniecki/pluralrules")
    (synopsis "Unicode Plural Rules categorizer for numeric input")
    (description
     "This package provides Unicode Plural Rules categorizer for numeric input.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-io-kit-sys-0.2
  (package
    (name "rust-io-kit-sys")
    (version "0.2.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "io-kit-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "068r17sns736ayykdxw4w9v9wxfyaa8xc2ai9wb9cvv8r7rzg2bp"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-core-foundation-sys" ,rust-core-foundation-sys-0.8)
                       ("rust-mach" ,rust-mach-0.3))))
    (home-page "https://github.com/jtakakura/io-kit-rs")
    (synopsis "Bindings to IOKit for macOS")
    (description "Bindings to IOKit for macOS.")
    (license (list license:expat license:asl2.0))))

(define-public rust-itoa-0.1
  (package
    (inherit rust-itoa-1)
    (name "rust-itoa")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "itoa" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "18g7p2hrb3dk84z3frfgmszfc9hjb4ps9vp99qlb1kmf9gm8hc5f"))))
    (arguments
     `())))

(define-public rust-jack-0.9
  (package
    (name "rust-jack")
    (version "0.9.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "jack" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0q10pq0zq10rnxv1ay2ffylxg4q363g9x6j5lcmvay9szg92did1"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;Cannot open libjack.so whatsoever
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-dlib" ,rust-dlib-0.5)
                       ("rust-jack-sys" ,rust-jack-sys-0.3)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-log" ,rust-log-0.4))
       #:cargo-development-inputs (("rust-crossbeam-channel" ,rust-crossbeam-channel-0.5))))
    (home-page "https://github.com/RustAudio/rust-jack")
    (synopsis "Real time audio and midi with JACK")
    (description
     "Rust bindings for the JACK Audio Connection Kit. These bindings work
on every operating system that JACK does.")
    (license license:expat)))

(define-public rust-jack-sys-0.3
  (package
    (name "rust-jack-sys")
    (version "0.3.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "jack-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1m2ci6p44ssaipfqf9hgwl3mhq3637magqj88xir2ddyc8zj77ig"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-dlib" ,rust-dlib-0.5)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-libloading" ,rust-libloading-0.7)
                       ("rust-dlopen" ,rust-dlopen-0.1)
                       ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (inputs (list jack-2))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/RustAudio/rust-jack/tree/main/jack-sys")
    (synopsis "Low-level binding to the JACK audio API.")
    (description
     "Low-level bindings for the JACK Audio Connection Kit. Generated using
rust-bindgen.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-khronos-api-2
  (package
    (inherit rust-khronos-api-3)
    (name "rust-khronos-api")
    (version "2.2.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "khronos-api" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0m5mpi5zyzzbsjkfymfkzib577ii8lk3l5p9sgxvarrzqdrb8yh3"))))))

(define-public rust-khronos-api-1
  (package
    (inherit rust-khronos-api-3)
    (name "rust-khronos-api")
    (version "1.1.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "khronos-api" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0v8cgxbcqqncny7mkljii65ixfpbxn3n4z69avgxv439c321p373"))))))

(define-public rust-khronos-api-0
  (package
    (inherit rust-khronos-api-3)
    (name "rust-khronos-api")
    (version "0.0.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "khronos-api" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0yc58wmdzvh6jf4ljs0a6z3994skr0zpa9y4fklzf4g8n77jf8i9"))))))

(define-public rust-lalrpop-util-0.11
  (package
    (name "rust-lalrpop-util")
    (version "0.11.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "lalrpop-util" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0ss0v11c508n8x0szqsgjmbvrhh2gifb2r3vi04afv7rs7j47i5z"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/lalrpop/lalrpop")
    (synopsis "Runtime library for parsers generated by LALRPOP")
    (description
     "This package provides Runtime library for parsers generated by
LALRPOP.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-laminar-0.1
  (package
    (name "rust-laminar")
    (version "0.1.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "laminar" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0pq1809g62dc31ix3zdw11y48ngifgydqrvb7wwgj9d48vvdcm1w"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bincode" ,rust-bincode-1)
                       ("rust-byteorder" ,rust-byteorder-1)
                       ("rust-clap" ,rust-clap-2)
                       ("rust-crc" ,rust-crc-1)
                       ("rust-env-logger" ,rust-env-logger-0.5)
                       ("rust-failure" ,rust-failure-0.1)
                       ("rust-failure-derive" ,rust-failure-derive-0.1)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-rand" ,rust-rand-0.5)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-derive" ,rust-serde-derive-1))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.2))))
    (home-page "https://github.com/amethyst/laminar")
    (synopsis "Simple semi-reliable UDP protocol for multiplayer games")
    (description
     "This package provides a simple semi-reliable UDP protocol for
multiplayer games.")
    (license (list license:expat license:asl2.0))))

(define-public rust-lazycell-0.4
  (package
    (inherit rust-lazycell-1)
    (name "rust-lazycell")
    (version "0.4.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "lazycell" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0szgqfl2aw18kn9cf6qqpxxkiw6x6hx9y4r3gklnxn1r8xn304nf"))))
    (arguments
     `(#:cargo-inputs (("rust-clippy" ,rust-clippy-0.0))))))

(define-public rust-lewton-0.10
  (package
    (name "rust-lewton")
    (version "0.10.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "lewton" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0c60fn004awg5c3cvx82d6na2pirf0qdz9w3b93mbcdakbglhyvp"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-byteorder" ,rust-byteorder-1)
                       ("rust-futures" ,rust-futures-0.1)
                       ("rust-ogg" ,rust-ogg-0.8)
                       ("rust-tinyvec" ,rust-tinyvec-1)
                       ("rust-tokio-io" ,rust-tokio-io-0.1))
       #:cargo-development-inputs (("rust-alto" ,rust-alto-3)
                                   ("rust-ogg" ,rust-ogg-0.8))))
    (home-page "https://github.com/RustAudio/lewton")
    (synopsis "Pure Rust vorbis decoder")
    (description "Vorbis decoder written in pure Rust.")
    (license (list license:expat license:asl2.0))))

(define-public rust-lewton-0.9
  (package
    (inherit rust-lewton-0.10)
    (name "rust-lewton")
    (version "0.9.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "lewton" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1l4bc88cpr8p94dfycykn8gajg20kp611kx159fc8dkh64d2qm4d"))))
    (arguments
     `(#:cargo-inputs (("rust-byteorder" ,rust-byteorder-1)
                       ("rust-futures" ,rust-futures-0.1)
                       ("rust-ogg" ,rust-ogg-0.7)
                       ("rust-smallvec" ,rust-smallvec-0.6)
                       ("rust-tokio-io" ,rust-tokio-io-0.1))
       #:cargo-development-inputs (("rust-alto" ,rust-alto-3)
                                   ("rust-ogg" ,rust-ogg-0.7))))))

(define-public rust-libc-0.1
  (package
    (inherit rust-libc-0.2)
    (name "rust-libc")
    (version "0.1.12")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "libc" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "08k14zb7bw25avmaj227calcdglb4ac394kklr9nv175fp7p0ap3"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))))
; std reimported multiple times, too old.

(define-public rust-libloading-0.4
  (package
    (inherit rust-libloading-0.7)
    (name "rust-libloading")
    (version "0.4.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "libloading" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1fbr96fpzzqxqwzpl74d8ami38vdpga4c1fk2w65v5ppx0yhff7x"))))
    (arguments
     `(#:cargo-inputs (("rust-kernel32-sys" ,rust-kernel32-sys-0.2)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-winapi" ,rust-winapi-0.2))))))

(define-public rust-libudev-sys-0.1
  (package
    (name "rust-libudev-sys")
    (version "0.1.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "libudev-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "09236fdzlx9l0dlrsc6xx21v5x8flpfm3d5rjq9jr5ivlas6k11w"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (inputs (list eudev))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/dcuddeback/libudev-sys")
    (synopsis "FFI bindings to libudev")
    (description
     "The libudev-sys crate provides declarations and linkage for the
libudev C library. Following the *-sys package conventions, the
libudev-sys crate does not define higher-level abstractions over the
native libudev library functions.")
    (license license:expat)))

(define-public rust-lz4-compress-0.1
  (package
    (name "rust-lz4-compress")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "lz4-compress" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "14cb8rpdfk6q3bjkf7mirpyzb6rvvcglqnayx6lvpa92m4rnb5hg"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-byteorder" ,rust-byteorder-0.5)
                       ("rust-quick-error" ,rust-quick-error-1))))
    (home-page "https://github.com/ticki/tfs")
    (synopsis "Pure Rust implementation of raw LZ4 compression/decompression.")
    (description "This package provides a pure Rust implementation of raw LZ4
compression/decompression.")
    (license license:expat)))

(define-public rust-lz4-flex-0.10
  (package
    (name "rust-lz4-flex")
    (version "0.10.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "lz4-flex" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "10sgbj93sagbl0ngzqvnlkldzbfz5vnzr7fry8sgssy299cp534b"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-twox-hash" ,rust-twox-hash-1))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.4)
                                   ("rust-lz4-compress" ,rust-lz4-compress-0.1)
                                   ("rust-lzzzz" ,rust-lzzzz-1)
                                   ("rust-more-asserts" ,rust-more-asserts-0.3)
                                   ("rust-serde-json" ,rust-serde-json-1)
                                   ("rust-snap" ,rust-snap-1))))
    (home-page "https://github.com/pseitz/lz4_flex")
    (synopsis "Fastest LZ4 implementation in Rust, no unsafe by default")
    (description
     "Fastest LZ4 implementation in Rust. Originally based on redox-os' lz4
compression, but now a complete rewrite.")
    (license license:expat)))

(define-public rust-lzw-0.9
  (package
    (inherit rust-lzw-0.10)
    (name "rust-lzw")
    (version "0.9.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "lzw" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "14pf04x8ps9a2vly4i275cjhcaplg1p5l752iywvq69f8d227a6b"))))))

(define-public rust-lzw-0.8
  (package
    (inherit rust-lzw-0.10)
    (name "rust-lzw")
    (version "0.8.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "lzw" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1y18lk7jdrw3rb2ncmwq0minrirjrrz4yk1lpv4rpgwajkbl0bvc"))))))

(define-public rust-lzzzz-1
  (package
    (name "rust-lzzzz")
    (version "1.0.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "lzzzz" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1g0kmzbbdsv9k4dbaxd0a1k1n3c3lwx5mhg4j5m6wxq440vd2540"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-cc" ,rust-cc-1))
       #:cargo-development-inputs (("rust-assert-fs" ,rust-assert-fs-1)
                                   ("rust-base64" ,rust-base64-0.13)
                                   ("rust-bytes" ,rust-bytes-1)
                                   ("rust-criterion" ,rust-criterion-0.3)
                                   ("rust-lazy-static" ,rust-lazy-static-1)
                                   ("rust-rand" ,rust-rand-0.8)
                                   ("rust-rayon" ,rust-rayon-1)
                                   ("rust-static-assertions" ,rust-static-assertions-1))))
    (home-page "https://github.com/picoHz/lzzzz")
    (synopsis "Full-featured liblz4 binding for Rust")
    (description
     "This package provides Rust APIs for the LZ4 compression algorithm.")
    (license license:expat)))

(define-public rust-mach-0.3
  ;; bump on upstream
  (package
    (name "rust-mach")
    (version "0.3.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "mach" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1yksa8lwzqh150gr4417rls1wk20asy9vhp8kq5g9n7z58xyh8xq"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-libc" ,rust-libc-0.2))))
    (home-page "https://github.com/fitzgen/mach")
    (synopsis "Rust interface to the Mach 3.0 kernel that underlies OSX")
    (description
     "This package provides a Rust interface to the user-space API of the
Mach 3.0 kernel that underlies OSX.")
    (license (list license:asl2.0 license:expat license:bsd-2))))

(define-public rust-memmap-0.4
  (package
    (inherit rust-memmap-0.7)
    (name "rust-memmap")
    (version "0.4.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "memmap" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1skgf49pasjqkq639frhg4fjpz039blxpscgx9ahh1qhm8j349b9"))))
    (arguments
     `(#:cargo-inputs (("rust-fs2" ,rust-fs2-0.2)
                       ("rust-kernel32-sys" ,rust-kernel32-sys-0.2)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-winapi" ,rust-winapi-0.2))
       #:cargo-development-inputs (("rust-tempdir" ,rust-tempdir-0.3))))))

(define-public rust-memmap2-0.7
  (package
    (name "rust-memmap2")
    (version "0.7.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "memmap2" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1il82b0mw304jlwvl0m89aa8bj5dgmm3vbb0jg8lqlrk0p98i4zl"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-stable-deref-trait" ,rust-stable-deref-trait-1))
       #:cargo-development-inputs (("rust-owning-ref" ,rust-owning-ref-0.4)
                                   ("rust-tempfile" ,rust-tempfile-3))))
    (home-page "https://github.com/RazrFalcon/memmap2-rs")
    (synopsis "Cross-platform Rust API for memory-mapped file IO")
    (description "This package provides a Rust API for memory-mapped file IO.")
    (license (list license:expat license:asl2.0))))

(define-public rust-memoffset-0.2
  (package
    (inherit rust-memoffset-0.9)
    (name "rust-memoffset")
    (version "0.2.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "memoffset" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1cvm2z7dy138s302ii7wlzcxbka5a8yfl5pl5di7lbdnw9hw578g"))))
    (arguments
     `(#:tests? #f))))

(define-public rust-minimp3-0.5
  (package
    (name "rust-minimp3")
    (version "0.5.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "minimp3" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0wj3nzj1swnvwsk3a4a3hkfj1d21jsi7babi40wlrxzbbzvkhm4q"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;examples for tokio fail.
       #:cargo-inputs (("rust-minimp3-sys" ,rust-minimp3-sys-0.3)
                       ("rust-slice-deque" ,rust-slice-deque-0.3)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-tokio" ,rust-tokio-1))
       #:cargo-development-inputs (("rust-futures" ,rust-futures-0.3)
                                   ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://github.com/germangb/minimp3-rs.git")
    (synopsis "Rust bindings for the minimp3 library")
    (description "Rust bindings for the minimp3 library.")
    (license license:expat)))

(define-public rust-minimp3-0.3
  (package
    (inherit rust-minimp3-0.5)
    (name "rust-minimp3")
    (version "0.3.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "minimp3" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1mfj2vg9vx0gvn5qhlsyqifccfynvnxij21mnavgilxzl3vczq6w"))))
    (arguments
     `(#:cargo-inputs (("rust-minimp3-sys" ,rust-minimp3-sys-0.3)
                       ("rust-slice-deque" ,rust-slice-deque-0.3))))))

(define-public rust-minimp3-sys-0.3
  (package
    (name "rust-minimp3-sys")
    (version "0.3.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "minimp3-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "144vmf3s89kad0smjprzigcp2c9r5dm95n4ydilrbp399irp6772"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;Uses incompatbile macro + can't find files
       #:cargo-inputs (("rust-cc" ,rust-cc-1))))
    (home-page "https://github.com/germangb/minimp3-rs.git")
    (synopsis "Rust bindings for the minimp3 library.")
    (description "Rust bindings for the minimp3 library (sys).")
    (license license:expat)))

(define-public rust-miniz-sys-0.1
  (package
    (name "rust-miniz-sys")
    (version "0.1.12")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "miniz-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "00l2r4anm8g35x0js2zfdnwfbrih9m43vphdpb77c5ga3kjkm7hy"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-libc" ,rust-libc-0.2))))
    (home-page "https://github.com/alexcrichton/flate2-rs")
    (synopsis "Bindings to the miniz.c library")
    (description "This package provides bindings to the miniz.c library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-mint-0.4
  (package
    (inherit rust-mint-0.5)
    (name "rust-mint")
    (version "0.4.9")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "mint" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1kpr1ncyamhwpckkzsbyg1vyl3vdn8shxdxh9rzwksmizq1fgn49"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-mint" ,rust-mint-0.5))))))

(define-public rust-mio-misc-1
  (package
    (name "rust-mio-misc")
    (version "1.2.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "mio-misc" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "131r9jjrv329y718gsxd6b9v9iwq6j09n8iazwvbj591lpri4x5l"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-crossbeam" ,rust-crossbeam-0.8)
                       ("rust-crossbeam-queue" ,rust-crossbeam-queue-0.3)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-mio" ,rust-mio-0.7))
       #:cargo-development-inputs (("rust-rand" ,rust-rand-0.8))))
    (home-page "https://github.com/onurzdg/mio-misc")
    (synopsis "Miscellaneous components for use with Mio")
    (description
     "Provides channels and schedulers that trigger events on Mio event
listener(Poll) to build loosely-coupled event-based applications")
    (license license:expat)))

(define-public rust-more-asserts-0.3
  (package
    (name "rust-more-asserts")
    (version "0.3.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "more-asserts" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0zj0f9z73nsn1zxk2y21f0mmafvz7dz5v93prlxwdndb3jbadbqz"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/thomcc/rust-more-asserts")
    (synopsis
     "Small library providing additional assert_* and debug_assert_* macros.")
    (description
     "This package provides a small library providing assertion macros
similar to the {debug_,}assert_{eq,ne} macros in the stdlib.")
    (license (list license:unlicense license:expat license:asl2.0 license:cc0))))

(define-public rust-mopa-0.2
  (package
    (name "rust-mopa")
    (version "0.2.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "mopa" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "05grm5s996fmjs7w2bq6lwrq969gwn1knba6aw7j6v15f41791d7"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/chris-morgan/mopa")
    (synopsis
     "My Own Personal Any: get your own Any with additional functionality")
    (description "A macro to implement all the Any methods on your own trait.")
    (license (list license:expat license:asl2.0))))

(define-public rust-nalgebra-0.32
  (package
    (name "rust-nalgebra")
    (version "0.32.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "nalgebra" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1ahwmg15v2qschc1y64hg5r2hdp7m0izv11zx0lkyhn2ijqxjzih"))
              (modules '((guix build utils)))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;TODO: how to enable required features?
       #:cargo-inputs (("rust-abomonation" ,rust-abomonation-0.7)
                       ("rust-alga" ,rust-alga-0.9)
                       ("rust-approx" ,rust-approx-0.5)
                       ("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-cust-core" ,rust-cust-core-0.1)
                       ("rust-glam" ,rust-glam-0.24)
                       ("rust-glam" ,rust-glam-0.23)
                       ("rust-glam" ,rust-glam-0.22)
                       ("rust-glam" ,rust-glam-0.21)
                       ("rust-glam" ,rust-glam-0.20)
                       ("rust-glam" ,rust-glam-0.19)
                       ("rust-glam" ,rust-glam-0.18)
                       ("rust-glam" ,rust-glam-0.17)
                       ("rust-glam" ,rust-glam-0.16)
                       ("rust-glam" ,rust-glam-0.15)
                       ("rust-glam" ,rust-glam-0.14)
                       ("rust-glam" ,rust-glam-0.13)
                       ("rust-matrixcompare-core" ,rust-matrixcompare-core-0.1)
                       ("rust-matrixmultiply" ,rust-matrixmultiply-0.3)
                       ("rust-mint" ,rust-mint-0.5)
                       ("rust-nalgebra-macros" ,rust-nalgebra-macros-0.2)
                       ("rust-num-complex" ,rust-num-complex-0.4)
                       ("rust-num-rational" ,rust-num-rational-0.4)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-pest" ,rust-pest-2)
                       ("rust-pest-derive" ,rust-pest-derive-2)
                       ("rust-proptest" ,rust-proptest-1)
                       ("rust-quickcheck" ,rust-quickcheck-1)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-rand-distr" ,rust-rand-distr-0.4)
                       ("rust-rkyv" ,rust-rkyv-0.6)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-simba" ,rust-simba-0.8)
                       ("rust-typenum" ,rust-typenum-1))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.3)
                                   ("rust-itertools" ,rust-itertools-0.10)
                                   ("rust-matrixcompare" ,rust-matrixcompare-0.3)
                                   ("rust-rand-isaac" ,rust-rand-isaac-0.3)
                                   ("rust-rand-xorshift" ,rust-rand-xorshift-0.3)
                                   ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://nalgebra.org")
    (synopsis "Linear algebra library")
    (description
     "This package provides a general-purpose linear algebra library with
transformations and statically-sized or dynamically-sized matrices.")
    (license license:bsd-3)))

(define-public rust-nalgebra-0.16
  (package
    (inherit rust-nalgebra-0.32)
    (name "rust-nalgebra")
    (version "0.16.14")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "nalgebra" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "163g3arb9m1l9p5c34621cnr3p9cjzj6sbz1yr4zbxgc97rnvf6c"))))
    (arguments
     `(#:cargo-inputs (("rust-abomonation" ,rust-abomonation-0.5)
                       ("rust-alga" ,rust-alga-0.7)
                       ("rust-approx" ,rust-approx-0.3)
                       ("rust-generic-array" ,rust-generic-array-0.11)
                       ("rust-matrixmultiply" ,rust-matrixmultiply-0.1)
                       ("rust-mint" ,rust-mint-0.5)
                       ("rust-num-complex" ,rust-num-complex-0.2)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-quickcheck" ,rust-quickcheck-0.6)
                       ("rust-rand" ,rust-rand-0.5)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-derive" ,rust-serde-derive-1)
                       ("rust-typenum" ,rust-typenum-1))
       #:cargo-development-inputs (("rust-serde-json" ,rust-serde-json-1))))))

(define-public rust-nalgebra-macros-0.2
  (package
    (name "rust-nalgebra-macros")
    (version "0.2.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "nalgebra-macros" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "166rzbzi1hcyjfvwxmrdimrcmflvxxifjfkqxxkdjfnhcznilxli"))
              (modules '((guix build utils)))
              (snippet '(begin
                          (substitute* "Cargo.toml"
                            ;; The resolver feature is not supported by our versions of Cargo.
                            (("resolver = \"2\".*")
                             ""))))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-1))
       #:cargo-development-inputs (("rust-nalgebra" ,rust-nalgebra-0.26))))
    (home-page "https://nalgebra.org")
    (synopsis "Procedural macros for nalgebra")
    (description "This package provides procedural macros for the nalgebra
linear algebra library.")
    (license license:asl2.0)))

(define-public rust-ndk-0.6
  (package
    (inherit rust-ndk-0.7)
    (name "rust-ndk")
    (version "0.6.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ndk" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1m1dfjw35qpys1hr4qib6mm3zacd01k439l7cx5f7phd0dzcfci0"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t ;Android Only
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-jni" ,rust-jni-0.18)
                       ("rust-jni-glue" ,rust-jni-glue-0.0)
                       ("rust-jni-sys" ,rust-jni-sys-0.3)
                       ("rust-ndk-sys" ,rust-ndk-sys-0.3)
                       ("rust-num-enum" ,rust-num-enum-0.5)
                       ("rust-thiserror" ,rust-thiserror-1))))))

(define-public rust-ndk-0.3
  (package
    (inherit rust-ndk-0.7)
    (name "rust-ndk")
    (version "0.3.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ndk" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1avk39s8w21inkzq09x83yghgq3v8rmhrycba8prg6rif8hk5547"))))
    (arguments
     `(#:skip-build? #t ;XXX: Android only
       #:cargo-inputs (("rust-jni" ,rust-jni-0.14)
                       ("rust-jni-glue" ,rust-jni-glue-0.0)
                       ("rust-jni-sys" ,rust-jni-sys-0.3)
                       ("rust-ndk-sys" ,rust-ndk-sys-0.2)
                       ("rust-num-enum" ,rust-num-enum-0.4)
                       ("rust-thiserror" ,rust-thiserror-1))))))

(define-public rust-ndk-0.1
  (package
    (inherit rust-ndk-0.7)
    (name "rust-ndk")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ndk" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1qksz81s5ymkpmlyg0jnab4h7fk024w6zh1z5jbkrzygb9yyf03q"))))
    (arguments
     `(#:skip-build? #t ;Android only.
       #:cargo-inputs (("rust-jni" ,rust-jni-0.14)
                       ("rust-jni-glue" ,rust-jni-glue-0.0) ;.10
                       ("rust-jni-sys" ,rust-jni-sys-0.3)
                       ("rust-ndk-sys" ,rust-ndk-sys-0.1)
                       ("rust-num-enum" ,rust-num-enum-0.4)
                       ("rust-thiserror" ,rust-thiserror-1))))))

(define-public rust-ndk-glue-0.7
  (package
    (inherit rust-ndk-glue-0.5) ;This verison is higher than upstream
    (name "rust-ndk-glue")
    (version "0.7.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ndk-glue" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0zyniqkkrzx3l3akf87h7kq1fdrkgddiv8wcfsmhlpn1sayzld04"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t ;XXX: Android only
       #:cargo-inputs (("rust-android-logger" ,rust-android-logger-0.11)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-ndk" ,rust-ndk-0.7)
                       ("rust-ndk-context" ,rust-ndk-context-0.1)
                       ("rust-ndk-macro" ,rust-ndk-macro-0.3)
                       ("rust-ndk-sys" ,rust-ndk-sys-0.4)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-parking-lot" ,rust-parking-lot-0.12))))))

(define-public rust-ndk-glue-0.3
  (package
    (inherit rust-ndk-glue-0.5)
    (name "rust-ndk-glue")
    (version "0.3.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ndk-glue" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "11cksaj1f2sy4dwqyssrvvhbnd86zajfvm17bj81rb2i9p1g1jn5"))))
    (arguments
     `(#:skip-build? #t ;XXX: Android only
       #:cargo-inputs (("rust-android-logger" ,rust-android-logger-0.8)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-ndk" ,rust-ndk-0.2)
                       ("rust-ndk-macro" ,rust-ndk-macro-0.2)
                       ("rust-ndk-sys" ,rust-ndk-sys-0.2))))))

(define-public rust-ndk-glue-0.1
  (package
    (inherit rust-ndk-glue-0.7)
    (name "rust-ndk-glue")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ndk-glue" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0pf8n60kggcswm5ld9dp8a23gks6vgvx56z11sl3fpsar42ghafb"))))
    (arguments
     `(#:skip-build? #t ;Android Only!
       #:cargo-inputs (("rust-android-log-sys" ,rust-android-log-sys-0.1)
                       ("rust-android-logger" ,rust-android-logger-0.8)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-ndk" ,rust-ndk-0.1)
                       ("rust-ndk-macro" ,rust-ndk-macro-0.1)
                       ("rust-ndk-sys" ,rust-ndk-sys-0.1))))))

(define-public rust-ndk-macro-0.1
  (package
    (inherit rust-ndk-macro-0.3)
    (name "rust-ndk-macro")
    (version "0.1.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ndk-macro" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1b2fg6cy43kh1zxvj97prl3ximvj38clpfjch7fymjrna3vqib11"))))
    (arguments
     `(#:cargo-inputs (("rust-log" ,rust-log-0.4)
                       ("rust-proc-macro-crate" ,rust-proc-macro-crate-0.1)
                       ("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-1))))))

(define-public rust-ndk-sys-0.3
  (package
    (inherit rust-ndk-sys-0.4)
    (name "rust-ndk-sys")
    (version "0.3.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ndk-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "15zsq4p6k5asf4mc0rknd8cz9wxrwvi50qdspgf87qcfgkknlnkf"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-jni-sys" ,rust-jni-sys-0.3))))))

(define-public rust-ndk-sys-0.1
  (package
    (inherit rust-ndk-sys-0.4)
    (name "rust-ndk-sys")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ndk-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "02xafh4z7kzs1bz0bivzsvavyy90gygxsax42983a4ljgmj4ya1x"))))
    (arguments
     `(#:skip-build? #t))))

(define-public rust-nix-0.9
  (package
    (inherit rust-nix-0.26)
    (name "rust-nix")
    (version "0.9.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "nix" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0cpvygha6bak7apkp9cm5snmld3lnm26crlxavl7pv4q07mszid2"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.9)
                       ("rust-cfg-if" ,rust-cfg-if-0.1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-void" ,rust-void-1))
       #:cargo-development-inputs (("rust-lazy-static" ,rust-lazy-static-0.2)
                                   ("rust-nix-test" ,rust-nix-test-0.0)
                                   ("rust-rand" ,rust-rand-0.3)
                                   ("rust-tempdir" ,rust-tempdir-0.3)
                                   ("rust-tempfile" ,rust-tempfile-2))))))

(define-public rust-nix-test-0.0
  (package
    (name "rust-nix-test")
    (version "0.0.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "nix-test" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1g7i9p3lizipyg99swdmkg0jyxm0i3wm2jkf7hrbrkk3k3rbhp9y"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/carllerche/nix-rust")
    (synopsis "Testing helpers for Nix")
    (description "This package provides testing helpers for Nix.")
    (license license:expat)))

(define-public rust-nonzero-signed-1
  (package
    (name "rust-nonzero-signed")
    (version "1.0.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "nonzero-signed" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1n1pdqzfzj8a8yh1s16sjrk5jjj5c3xvh4albwzhsfrkh823ly02"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/Xaeroxe/nonzero_signed")
    (synopsis "DEPRECATED: A small set of types for signed nonzero integers")
    (description
     "DEPRECATED Rust's std lib will stabilize their own signed
@code{NonZero} types in Rust 1.34, please use those instead if you're
using Rust 1.34 or greater.  You can find them in std::num or
core::num.  Description A small set of types for signed nonzero
integers.")
    (license (list license:expat license:asl2.0))))

(define-public rust-num-derive-0.3
  (package
    (name "rust-num-derive")
    (version "0.3.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "num-derive" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0gbl94ckzqjdzy4j8b1p55mz01g6n1l9bckllqvaj0wfz7zm6sl7"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-syn" ,rust-syn-1)
                       ("rust-quote" ,rust-quote-1))
       #:cargo-development-inputs (("rust-num" ,rust-num-0.3)
                                   ("rust-num-traits" ,rust-num-traits-0.2))))
    (home-page "https://github.com/rust-num/num-derive")
    (synopsis "Numeric syntax extensions")
    (description "This package provides numeric syntax extensions.")
    (license (list license:expat license:asl2.0))))

(define-public rust-obj-0.5
  (package
    (inherit rust-obj-0.10)
    (name "rust-obj")
    (version "0.5.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "obj" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0pl9blz3pj81yk2wih0nrbdmzg1vag0c0159xfdk818zzq91hch2"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-genmesh" ,rust-genmesh-0.4))))))

(define-public rust-objc-0.1
  (package
    (inherit rust-objc-0.2)
    (name "rust-objc")
    (version "0.1.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "objc" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0mj00d6fzdn518ryq4r1r32njgvgg1yri8n7by2rh4q3r1zgscsr"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-malloc-buf" ,rust-malloc-buf-0.0)
                       ("rust-objc-exception" ,rust-objc-exception-0.0))))))

(define-public rust-objc-exception-0.0
  (package
    (inherit rust-objc-exception-0.1)
    (name "rust-objc-exception")
    (version "0.0.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "objc-exception" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "02a22wqcx0hkbnsin7fzgpvvhw8ynv0sks2l4ra1wmk556k8axar"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-gcc" ,rust-gcc-0.3)
                       ("rust-libc" ,rust-libc-0.2))))))

(define-public rust-oboe-0.4
  (package
    (name "rust-oboe")
    (version "0.4.6")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "oboe" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1hd5626s8qkpgrl2alwz73i8rh1rzifbxj6pxz7zp82gicskrxi7"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-jni" ,rust-jni-0.19)
                       ("rust-ndk" ,rust-ndk-0.6)
                       ("rust-ndk-context" ,rust-ndk-context-0.1)
                       ("rust-num-derive" ,rust-num-derive-0.3)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-oboe-sys" ,rust-oboe-sys-0.4))))
    (home-page "https://github.com/katyo/oboe-rs")
    (synopsis
     "Safe interface for oboe an android library for low latency audio IO")
    (description
     "Safe Rust interface for Oboe High-Performance Audio library for
Android. Also it provides interface for some platform APIs significant
to Audio IO.")
    (license license:asl2.0)))

(define-public rust-oboe-sys-0.4
  (package
    (name "rust-oboe-sys")
    (version "0.4.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "oboe-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1gcl494yy880h2gfgsbdd32g2h0s1n94v58j5hil9mrf6yvsnw1k"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t ;Android Only!
       #:cargo-inputs (("rust-bindgen" ,rust-bindgen-0.59)
                       ("rust-cc" ,rust-cc-1)
                       ("rust-fetch-unroll" ,rust-fetch-unroll-0.3))))
    (home-page "https://github.com/katyo/oboe-rs")
    (synopsis
     "Unsafe bindings for oboe an android library for low latency audio IO")
    (description
     "This crate provides unsafe bindings for oboe. Usually you shouldn't
use this crate directly, instead use oboe crate which provides safe
interface.")
    (license license:asl2.0)))

(define-public rust-ogg-0.9
  (package
    (name "rust-ogg")
    (version "0.9.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ogg" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1b4p92yamybip97a211wfighlc2bf27jhxy78b25591i0py0w3cn"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-byteorder" ,rust-byteorder-1)
                       ("rust-bytes" ,rust-bytes-1)
                       ("rust-futures-core" ,rust-futures-core-0.3)
                       ("rust-futures-io" ,rust-futures-io-0.3)
                       ("rust-pin-project" ,rust-pin-project-1)
                       ("rust-tokio" ,rust-tokio-1)
                       ("rust-tokio-util" ,rust-tokio-util-0.6))
       #:cargo-development-inputs (("rust-futures-util" ,rust-futures-util-0.3)
                                   ("rust-rand" ,rust-rand-0.8)
                                   ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://github.com/RustAudio/ogg")
    (synopsis "Ogg container decoder and encoder written in pure Rust")
    (description
     "An Ogg decoder and encoder. Implements the xiph.org Ogg spec in pure
Rust.")
    (license license:bsd-3)))

(define-public rust-ogg-0.8
  (package
    (inherit rust-ogg-0.9)
    (name "rust-ogg")
    (version "0.8.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ogg" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0vjxmqcv9252aj8byy70iy2krqfjknfcxg11lcyikj11pzlb8lb9"))))
    (arguments
     `(#:cargo-inputs (("rust-byteorder" ,rust-byteorder-1)
                       ("rust-bytes" ,rust-bytes-0.4)
                       ("rust-futures" ,rust-futures-0.1)
                       ("rust-tokio-io" ,rust-tokio-io-0.1))
       #:cargo-development-inputs (("rust-rand" ,rust-rand-0.3))))))

(define-public rust-ogg-0.7
  (package
    (inherit rust-ogg-0.9)
    (name "rust-ogg")
    (version "0.7.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ogg" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0ldnq9hccrsaqyzp6yb6w2nn1mpd4wd5fqsckmrf3ybsa71p3r8k"))))
    (arguments
     `(#:cargo-inputs (("rust-byteorder" ,rust-byteorder-1)
                       ("rust-bytes" ,rust-bytes-0.4)
                       ("rust-futures" ,rust-futures-0.1)
                       ("rust-tokio-io" ,rust-tokio-io-0.1))
       #:cargo-development-inputs (("rust-rand" ,rust-rand-0.3))))))

(define-public rust-ogg-0.5
  (package
    (inherit rust-ogg-0.9)
    (name "rust-ogg")
    (version "0.5.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ogg" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1sg29xkg332s3cqj7axawvnzv7nfldk7f853c2xa1a006d1yb39z"))))
    (arguments
     `(#:cargo-inputs (("rust-byteorder" ,rust-byteorder-1)
                       ("rust-bytes" ,rust-bytes-0.4)
                       ("rust-futures" ,rust-futures-0.1)
                       ("rust-tokio-io" ,rust-tokio-io-0.1))
       #:cargo-development-inputs (("rust-rand" ,rust-rand-0.3))))))

(define-public rust-ogg-sys-0.0.9
  (package
    (name "rust-ogg-sys")
    (version "0.0.9")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ogg-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1cpx6n5ndh2d59g43l6rj3myzi5jsc0n6rldpx0impqp5qbqqnx9"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-gcc" ,rust-gcc-0.3)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/tomaka/ogg-sys")
    (synopsis "FFI for libogg, the media container")
    (description "FFI for libogg, the media container.")
    (license license:expat)))

(define-public rust-ole32-sys-0.2
  (package
    (name "rust-ole32-sys")
    (version "0.2.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ole32-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "134xg38xicrqynx4pfjfxnpp8x83m3gqw5j3s8y27rc22w14jb2x"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t ;for the proprietary windows platform
       #:cargo-inputs (("rust-winapi" ,rust-winapi-0.2)
                       ("rust-winapi-build" ,rust-winapi-build-0.1))))
    (home-page "https://github.com/retep998/winapi-rs")
    (synopsis
     "Contains function definitions for the Windows API library ole32. See
winapi for types and constants")
    (description
     "Contains function definitions for the Windows API library ole32. See
winapi for types and constants.")
    (license license:expat)))

(define-public rust-optick-1
  (package
    (name "rust-optick")
    (version "1.3.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "optick" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0j35dj8ggfpcc399h1ljm6xfz8kszqc4nrw3vcl9kfndd1hapryp"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f)) ;Fails to link -lOptickCore_MD
    (home-page "https://github.com/bombomby/optick-rs")
    (synopsis "Super Lightweight Performance Profiler")
    (description
     "This package provides a super lightweight performance profiler.")
    (license license:expat)))

(define-public rust-option-set-0.2
  ;; upstream bump
  (package
    (name "rust-option-set")
    (version "0.2.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "option-set" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0i6s3bmnrw44nffqbbcaiq7fyhz7j881lcgspb57jxsi752m11k0"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-heck" ,rust-heck-0.4)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-bitflags" ,rust-bitflags-2)
                                   ("rust-serde" ,rust-serde-1)
                                   ("rust-serde-json" ,rust-serde-json-1)
                                   ("rust-serde-yaml" ,rust-serde-yaml-0.9))))
    (home-page "https://github.com/H2CO3/option_set.git")
    (synopsis "Bitflags on steroids")
    (description "This package provides an extended version of rust bitflags.")
    (license license:expat)))

(define-public rust-ordered-float-2.10
  (package
    (inherit rust-ordered-float-3)
    (name "rust-ordered-float")
    (version "2.10.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ordered-float" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "11qdskfgk911bs541avzkrfahq6arnb2bkvzs0c36na2m4ncyh3r"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-arbitrary" ,rust-arbitrary-1)
                       ("rust-proptest" ,rust-proptest-1)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-rkyv" ,rust-rkyv-0.7)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-serde-test" ,rust-serde-test-1))))))

(define-public rust-owning-ref-0.3
  (package
    (inherit rust-owning-ref-0.4)
    (name "rust-owning-ref")
    (version "0.3.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "owning-ref" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0dqgf5hwbmvkf2ffbik5xmhvaqvqi6iklhwk9x47n0wycd0lzy6d"))))
    (arguments
     `(#:cargo-inputs (("rust-stable-deref-trait" ,rust-stable-deref-trait-1))))))

(define-public rust-parking-lot-0.6
  (package
    (inherit rust-parking-lot-0.12)
    (name "rust-parking-lot")
    (version "0.6.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "parking-lot" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1rfd4h24kvvxhjzaz53ycqqwql9y65wpxp2nlwdjjfq017zjp07h"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-lock-api" ,rust-lock-api-0.1)
                       ("rust-parking-lot-core" ,rust-parking-lot-core-0.3))
       #:cargo-development-inputs (("rust-rand" ,rust-rand-0.5))))))

(define-public rust-parking-lot-0.4
  (package
    (inherit rust-parking-lot-0.12)
    (name "rust-parking-lot")
    (version "0.4.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "parking-lot" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0ph0kv3dfcxpjbi83wkzammqb7lm95j8in7w7hz17hgkjxdqz78l"))))
    (arguments
     `(#:cargo-inputs (("rust-owning-ref" ,rust-owning-ref-0.3)
                       ("rust-parking-lot-core" ,rust-parking-lot-core-0.2))
       #:cargo-development-inputs (("rust-rand" ,rust-rand-0.3))))))

(define-public rust-parking-lot-core-0.3
  (package
    (inherit rust-parking-lot-core-0.9)
    (name "rust-parking-lot-core")
    (version "0.3.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "parking-lot-core" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "130by1bszx1iaav33sz3i6lzfx01c9dsb1ybzpvdz7n7pmp7wzxd"))))
    (arguments
     `(#:cargo-inputs (("rust-backtrace" ,rust-backtrace-0.3)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-petgraph" ,rust-petgraph-0.4)
                       ("rust-rand" ,rust-rand-0.5)
                       ("rust-rustc-version" ,rust-rustc-version-0.2)
                       ("rust-smallvec" ,rust-smallvec-0.6)
                       ("rust-thread-id" ,rust-thread-id-3)
                       ("rust-winapi" ,rust-winapi-0.3))))))

(define-public rust-parking-lot-core-0.2
  (package
    (inherit rust-parking-lot-core-0.9)
    (name "rust-parking-lot-core")
    (version "0.2.15")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "parking-lot-core" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "15qv920lymlbbwzzkkhn86i3q2pbvhlb7hlacfpai8fmpk1i5w6r"))))
    (arguments
     `(#:cargo-inputs (("rust-backtrace" ,rust-backtrace-0.3)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-petgraph" ,rust-petgraph-0.4)
                       ("rust-rand" ,rust-rand-0.5)
                       ("rust-smallvec" ,rust-smallvec-0.6)
                       ("rust-thread-id" ,rust-thread-id-3)
                       ("rust-winapi" ,rust-winapi-0.3))))))

(define-public rust-paste-1
  (package
    (name "rust-paste")
    (version "1.0.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "paste" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0n7y1pabn6vspdxgzx62rs9wdlbnay9r1g84j8jk2pn6s1x59gxc"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/dtolnay/paste")
    (synopsis "Macros for all your token pasting needs")
    (description
     "This package provides macros for all your token pasting needs.")
    (license (list license:expat license:asl2.0))))

(define-public rust-path-slash-0.1
  (package
    (name "rust-path-slash")
    (version "0.1.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "path-slash" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "15mni0f28mblwd97192c4zyyi6054yljmiqrdb6bx97ga69hk2j9"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-development-inputs (("rust-lazy-static" ,rust-lazy-static-1))))
    (home-page "https://github.com/rhysd/path-slash")
    (synopsis "Conversion to/from a file path from/to slash path")
    (description
     "This package provides conversion to/from a file path from/to slash path.")
    (license license:expat)))

(define-public rust-pbr-1
  (package
    (name "rust-pbr")
    (version "1.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "pbr" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "056mqvw168ziig1dgl2kq4vmkamv6gk3hv1x9696r6ynl3gjfn7d"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;Tries to access /dev/stderr
       #:cargo-inputs (("rust-crossbeam-channel" ,rust-crossbeam-channel-0.5)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-winapi" ,rust-winapi-0.3))
       #:cargo-development-inputs (("rust-rand" ,rust-rand-0.8))))
    (home-page "https://github.com/a8m/pb")
    (synopsis "Console progress bar for Rust")
    (description "This package provides a console progress bar for Rust.")
    (license license:expat)))

(define-public rust-pollster-0.2
  (package
    (name "rust-pollster")
    (version "0.2.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "pollster" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1xzji8cq4crr795c7liy3ksd798sb45pjphbm8h5gvnp7whb18sx"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-development-inputs (("rust-futures-timer" ,rust-futures-timer-3)
                                   ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://github.com/zesterer/pollster")
    (synopsis "Synchronously block the thread until a future completes")
    (description
     "Pollster is an incredibly minimal async executor for Rust that lets
you block a thread until a future completes.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-pretty-env-logger-0.2
  (package
    (inherit rust-pretty-env-logger-0.4)
    (name "rust-pretty-env-logger")
    (version "0.2.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "pretty-env-logger" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0a0c53plsr4abw0y1iyjxs0d64f0a6dn48464a2rp21f0iiix3gd"))))
    (arguments
     `(#:cargo-inputs (("rust-ansi-term" ,rust-ansi-term-0.11)
                       ("rust-chrono" ,rust-chrono-0.4)
                       ("rust-env-logger" ,rust-env-logger-0.5)
                       ("rust-log" ,rust-log-0.4))))))

(define-public rust-prettytable-rs-0.7
  (package
    (inherit rust-prettytable-rs-0.8)
    (name "rust-prettytable-rs")
    (version "0.7.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "prettytable-rs" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "17nd0vhvrwhmyx7g828cnq448rhx4cipkgpnpw55z8ssh16cl4am"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-atty" ,rust-atty-0.2)
                       ("rust-csv" ,rust-csv-0.15)
                       ("rust-encode-unicode" ,rust-encode-unicode-0.3)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-term" ,rust-term-0.5)
                       ("rust-unicode-width" ,rust-unicode-width-0.1))))))

(define-public rust-puffin-0.16
  (package
    (name "rust-puffin")
    (version "0.16.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "puffin" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "08ass1hfdcq86y7dywa1jylzq57la95rgpcmd6yx82hs9symlhkn"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-anyhow" ,rust-anyhow-1)
                       ("rust-bincode" ,rust-bincode-1)
                       ("rust-byteorder" ,rust-byteorder-1)
                       ("rust-cfg-if" ,rust-cfg-if-1)
                       ("rust-instant" ,rust-instant-0.1)
                       ("rust-js-sys" ,rust-js-sys-0.3)
                       ("rust-lz4-flex" ,rust-lz4-flex-0.10)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-parking-lot" ,rust-parking-lot-0.12)
                       ("rust-ruzstd" ,rust-ruzstd-0.4)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-zstd" ,rust-zstd-0.12))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.4))))
    (home-page "https://github.com/EmbarkStudios/puffin")
    (synopsis "Simple instrumentation profiler for games")
    (description "The friendly little instrumentation profiler for Rust.")
    (license (list license:expat license:asl2.0))))

(define-public rust-quale-1
  (package
    (name "rust-quale")
    (version "1.0.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "quale" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "135wx0fhg5qv4887c4fyj0bhqixknf97ihmd4gmwqr6c2g2i2s64"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;Incompatible unit test (/bin/sh != /gnu/store/.../bin/sh)
       #:cargo-inputs (("rust-libc" ,rust-libc-0.2))))
    (home-page "https://github.com/frewsxcv/rust-quale")
    (synopsis "A Rust port of the `which` utility")
    (description
     "A Rust port of the `which` utility. Locates an executable in the
user’s path.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-quasi-0.16
  (package
    (name "rust-quasi")
    (version "0.16.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "quasi" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0qf1rgbvi62dw39gi236g35jrav6irgb3yh93xdimxwswplmckii"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t ;Broken with rust-syntex
       #:cargo-inputs (("rust-clippy" ,rust-clippy-0.0)
                       ("rust-syntex-errors" ,rust-syntex-errors-0.39)
                       ("rust-syntex-syntax" ,rust-syntex-syntax-0.39))))
    (home-page "https://github.com/serde-rs/quasi")
    (synopsis "A quasi-quoting macro system")
    (description "This package provides a quasi-quoting macro system.")
    (license (list license:expat license:asl2.0))))

(define-public rust-quasi-codegen-0.16
  (package
    (name "rust-quasi-codegen")
    (version "0.16.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "quasi-codegen" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1dylcc3n41p8m63s5jsaxla3ihsswv63f25cxrrzh4pcsnmmcf0s"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t ;borrow checker error
       #:cargo-inputs (("rust-aster" ,rust-aster-0.22)
                       ("rust-clippy" ,rust-clippy-0.0)
                       ("rust-syntex" ,rust-syntex-0.39)
                       ("rust-syntex-errors" ,rust-syntex-errors-0.39)
                       ("rust-syntex-syntax" ,rust-syntex-syntax-0.39))))
    (home-page "https://github.com/serde-rs/quasi")
    (synopsis "A quasi-quoting macro system")
    (description "This package provides a quasi-quoting macro system.")
    (license (list license:expat license:asl2.0))))

(define-public rust-quasi-macros-0.16
  (package
    (name "rust-quasi-macros")
    (version "0.16.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "quasi-macros" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1if9fz160qfp8c4izslgdbphw3plax3cc29rspj3hskvymzhnlwv"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t ;fails to load source
       #:cargo-inputs (("rust-clippy" ,rust-clippy-0.0)
                       ("rust-quasi-codegen" ,rust-quasi-codegen-0.16))
       #:cargo-development-inputs (("rust-aster" ,rust-aster-0.22)
                                   ("rust-quasi" ,rust-quasi-0.16))))
    (home-page "https://github.com/serde-rs/quasi")
    (synopsis "A quasi-quoting macro system")
    (description "This package provides a quasi-quoting macro system.")
    (license (list license:expat license:asl2.0))))

(define-public rust-quick-xml-0.28
  (package
    (name "rust-quick-xml")
    (version "0.28.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "quick-xml" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1lfr3512x0s0i9kbyglyzn0rq0i1bvd2mqqfi8gs685808rfgr8c"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-arbitrary" ,rust-arbitrary-1)
                       ("rust-document-features" ,rust-document-features-0.2)
                       ("rust-encoding-rs" ,rust-encoding-rs-0.8)
                       ("rust-memchr" ,rust-memchr-2)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-tokio" ,rust-tokio-1))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.4)
                                   ("rust-pretty-assertions" ,rust-pretty-assertions-1)
                                   ("rust-regex" ,rust-regex-1)
                                   ("rust-serde-value" ,rust-serde-value-0.7)
                                   ("rust-serde-derive" ,rust-serde-derive-1)
                                   ("rust-tokio" ,rust-tokio-1)
                                   ("rust-tokio-test" ,rust-tokio-test-0.4))))
    (home-page "https://github.com/tafia/quick-xml")
    (synopsis "High performance xml reader and writer")
    (description
     "This package provides a high performance XML reader and writer.")
    (license license:expat)))

(define-public rust-range-alloc-0.1
  (package
    (name "rust-range-alloc")
    (version "0.1.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "range-alloc" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1azfwh89nd4idj0s272qgmw3x1cj6m7d3f44b2la02wzvkyrk2lw"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/gfx-rs/range-alloc")
    (synopsis "Generic range allocator")
    (description "This package provides a generic range allocator.")
    (license (list license:expat license:asl2.0))))

(define-public rust-relay-0.1
  (package
    (name "rust-relay")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "relay" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "16j8y57rjrfy3h5xfi9fwfbjs1nka3iifi52rvp9szldd21f6xhm"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-futures" ,rust-futures-0.1))))
    (home-page "https://github.com/seanmonstar/relay")
    (synopsis "Lightweight oneshot Future channel")
    (description "This package provides a lightweight oneshot Future channel.")
    (license (list license:expat license:asl2.0))))

(define-public rust-renderdoc-sys-0.7
  (package
    (name "rust-renderdoc-sys")
    (version "0.7.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "renderdoc-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0mx1crv83mwmfl7yvhnpmhjb01fx5yd9f3f2gpwlnb1518gjsf7i"))))
    (build-system cargo-build-system)
    (home-page
     "https://github.com/ebkalderon/renderdoc-rs/tree/master/renderdoc-sys")
    (synopsis "Low-level bindings to the RenderDoc API")
    (description
     "This package provides low-level bindings to the @code{RenderDoc} API")
    (license (list license:expat license:asl2.0))))

(define-public rust-rental-0.5
  (package
    (name "rust-rental")
    (version "0.5.6")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "rental" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0bhzz2pfbg0yaw8p1l31bggq4jn077wslf6ifhj22vf3r8mgx2fc"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;Borrow checker error in one of the tests
       #:cargo-inputs (("rust-rental-impl" ,rust-rental-impl-0.5)
                       ("rust-stable-deref-trait" ,rust-stable-deref-trait-1))))
    (home-page "https://github.com/jpernst/rental")
    (synopsis
     "A macro to generate safe self-referential structs, plus premade types for common use cases.")
    (description
     "This package provides a macro to generate safe self-referential structs, plus
premade types for common use cases.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rental-impl-0.5
  (package
    (name "rust-rental-impl")
    (version "0.5.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "rental-impl" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1pj0qgmvwwsfwyjqyjxzikkwbwc3vj7hm3hdykr47dy5inbnhpj7"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-1))))
    (home-page "https://www.jpernst.com")
    (synopsis "Implementation detail of rental. Should not be used directly")
    (description
     "An implementation detail of rental.  Should not be used directly.")
    (license (list license:expat license:asl2.0))))

(define-public rust-reqwest-0.7
  (package
    (inherit rust-reqwest-0.11)
    (name "rust-reqwest")
    (version "0.7.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "reqwest" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1zdiqppy3h88bhmx8b7h6k4vy7zz1qnvyq59g429r8z2hhyn2rjq"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bytes" ,rust-bytes-0.4)
                       ("rust-futures" ,rust-futures-0.1)
                       ("rust-hyper" ,rust-hyper-0.11)
                       ("rust-hyper-tls" ,rust-hyper-tls-0.1)
                       ("rust-libflate" ,rust-libflate-0.1)
                       ("rust-log" ,rust-log-0.3)
                       ("rust-native-tls" ,rust-native-tls-0.1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-serde-urlencoded" ,rust-serde-urlencoded-0.5)
                       ("rust-tokio-core" ,rust-tokio-core-0.1)
                       ("rust-tokio-io" ,rust-tokio-io-0.1)
                       ("rust-tokio-tls" ,rust-tokio-tls-0.1)
                       ("rust-url" ,rust-url-1))
       #:cargo-development-inputs (("rust-env-logger" ,rust-env-logger-0.4)
                                   ("rust-error-chain" ,rust-error-chain-0.10)
                                   ("rust-serde-derive" ,rust-serde-derive-1))))))

(define-public rust-ringbuf-0.2
  (package
    (name "rust-ringbuf")
    (version "0.2.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ringbuf" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "18n2qmbvvxj9s775p6q2dv5s68ndbpvb7fr3mx5fg2gpa26z2npn"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-cache-padded" ,rust-cache-padded-1))))
    (home-page "https://gitlab.com/agerasev/ringbuf.git")
    (synopsis
     "Lock-free SPSC FIFO ring buffer with direct access to inner data")
    (description
     "Lock-free SPSC FIFO ring buffer with direct access to inner data.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rodio-0.16
  (package
    (name "rust-rodio")
    (version "0.16.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "rodio" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1dbjj3b0hhdsffymb6avsf3zsij0gwn6xm77l88r83pcsm9vc47b"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-cpal" ,rust-cpal-0.14)
                       ("rust-claxon" ,rust-claxon-0.4)
                       ("rust-hound" ,rust-hound-3)
                       ("rust-lewton" ,rust-lewton-0.10)
                       ("rust-minimp3" ,rust-minimp3-0.5)
                       ("rust-symphonia" ,rust-symphonia-0.5))
       #:cargo-development-inputs (("rust-quickcheck" ,rust-quickcheck-0.9))))
    (inputs (list alsa-lib))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/RustAudio/rodio")
    (synopsis "Audio playback library")
    (description "A Rust playback library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rodio-0.8
  (package
    (inherit rust-rodio-0.16)
    (name "rust-rodio")
    (version "0.8.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "rodio" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1g20lagfv6nnp5vlnz4107sigkcgbhdmqfnig1wp9jv326a4gjqh"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-cgmath" ,rust-cgmath-0.14)
                       ("rust-claxon" ,rust-claxon-0.3)
                       ("rust-cpal" ,rust-cpal-0.8)
                       ("rust-hound" ,rust-hound-3)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-lewton" ,rust-lewton-0.9)
                       ("rust-minimp3" ,rust-minimp3-0.3))))))

(define-public rust-ron-0.8
  ;; upstream bump
  (package
    (name "rust-ron")
    (version "0.8.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ron" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "154w53s895yxdfg7rn87c6f6x4yncc535x1x31zpcj7p0pzpw7xr"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;Tests fail
       #:cargo-inputs (("rust-base64" ,rust-base64-0.21)
                       ("rust-bitflags" ,rust-bitflags-2)
                       ("rust-indexmap" ,rust-indexmap-2)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-derive" ,rust-serde-derive-1))
       #:cargo-development-inputs (("rust-option-set" ,rust-option-set-0.2)
                                   ("rust-serde" ,rust-serde-1)
                                   ("rust-serde-bytes" ,rust-serde-bytes-0.11)
                                   ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/ron-rs/ron")
    (synopsis "Rusty Object Notation")
    (description "This package provides Rusty Object Notation (RON).")
    (license (list license:expat license:asl2.0))))

(define-public rust-rudy-0.1
  (package
    (name "rust-rudy")
    (version "0.1.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "rudy" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0hm0wy5x858xhbcvc4wm3yhwl10ksk7y89afyr1rhh4436ys3nbw"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-nodrop" ,rust-nodrop-0.1)
                       ("rust-num-traits" ,rust-num-traits-0.1))
       #:cargo-development-inputs (("rust-bencher" ,rust-bencher-0.1))))
    (home-page "https://www.github.com/adevore/rudy/")
    (synopsis "Judy array implementation in pure Rust")
    (description
     "This package provides judy array implementation in pure Rust.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rustc-version-runtime-0.1
  (package
    (name "rust-rustc-version-runtime")
    (version "0.1.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "rustc-version-runtime" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0i8ii76a8yj2g5lw1p34wihqq5x3ql7f3dk9dwq1ywypzbbyrs3d"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-rustc-version" ,rust-rustc-version-0.2)
                       ("rust-rustc-version" ,rust-rustc-version-0.2)
                       ("rust-semver" ,rust-semver-0.9)
                       ("rust-semver" ,rust-semver-0.9))))
    (home-page "https://github.com/seppo0010/rustc-version-runtime-rs")
    (synopsis
     "Library for querying the version of the rustc compiler used in runtime")
    (description
     "This package provides a library for querying the version of the rustc compiler
used in runtime.")
    (license license:expat)))

(define-public rust-rusty-xinput-1
  (package
    (name "rust-rusty-xinput")
    (version "1.2.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "rusty-xinput" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1fxk5lkkjk09k8k3az2lli4kkr6zr6mq9871rhacmf9fqd5nbanj"))
              (modules '((guix build utils)))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-winapi" ,rust-winapi-0.3))
       #:cargo-development-inputs (("rust-simple-logger" ,rust-simple-logger-0.5))
       #:tests? #f))
    (home-page "https://github.com/Lokathor/rusty-xinput")
    (synopsis "Safe dynamic loading of xinput")
    (description
     "Rust crate to dynamically load an xinput dll and lets you safely call the functions.")
    (license (list license:zlib license:expat license:asl2.0))))

(define-public rust-ruzstd-0.4
  (package
    (name "rust-ruzstd")
    (version "0.4.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ruzstd" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1p4ghqzkq36dy1x1ijnk7jmml4wi3v9bkfzlbm2hsnkiz6wglgxc"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;Could not find test files
       #:cargo-inputs (("rust-byteorder" ,rust-byteorder-1)
                       ("rust-thiserror-core" ,rust-thiserror-core-1)
                       ("rust-twox-hash" ,rust-twox-hash-1))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.3)
                                   ("rust-rand" ,rust-rand-0.8))))
    (home-page "https://github.com/KillingSpark/zstd-rs")
    (synopsis "A decoder for the zstd compression format")
    (description
     "This package provides a decoder for the zstd compression format.")
    (license license:expat)))

(define-public rust-sctk-adwaita-0.4
  (package
    (inherit rust-sctk-adwaita-0.5)
    (name "rust-sctk-adwaita")
    (version "0.4.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "sctk-adwaita" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0f93q74iv1qsk0hkcx0jqh2a9qf2slri1nq737n7fkbbrhlhc9v1"))))
    (arguments
     `(#:cargo-inputs (("rust-crossfont" ,rust-crossfont-0.5)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-smithay-client-toolkit" ,rust-smithay-client-toolkit-0.16)
                       ("rust-tiny-skia" ,rust-tiny-skia-0.7))))))

(define-public rust-scoped-tls-hkt-0.1
  (package
    (name "rust-scoped-tls-hkt")
    (version "0.1.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "scoped-tls-hkt" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1p6x35cffxr6y293ip4m8pnf60bbgyvg06q7rb3gdn8h6ifpdp1x"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/Diggsey/scoped-tls-hkt")
    (synopsis "More flexible version of `scoped-tls`")
    (description
     "This package provides a more flexible version of `scoped-tls`,
allowing the following additional features: - Storage of references to
dynamically sized types. - Storage of mutable references. - Storage of
types containing unbound lifetime parameters (higher-kinded types). -
Some combination of the above.")
    (license (list license:expat license:asl2.0))))

(define-public rust-sd-notify-0.4
  (package
    (name "rust-sd-notify")
    (version "0.4.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "sd-notify" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0clc887rjdz0796c1lsbwnrgmcis4b30gyy3qb4v8zg0yf03c7k2"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/lnicola/sd-notify")
    (synopsis "Lightweight crate for systemd service state notifications")
    (description
     "A lightweight (no dependencies) crate for sending @code{systemd} service
state notifications.")
    (license (list license:expat license:asl2.0))))

(define-public rust-sema-0.1
  (package
    (name "rust-sema")
    (version "0.1.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "sema" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0ckq33sg84785p195m54h03jcn7fai8w08hjnb94nzaakgzibbz3"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-libc" ,rust-libc-0.1)
                       ("rust-rand" ,rust-rand-0.3)
                       ("rust-time" ,rust-time-0.1))
       #:cargo-development-inputs (("rust-lazy-static" ,rust-lazy-static-1)
                                   ("rust-nix" ,rust-nix-0.26))))
    (home-page "https://github.com/cpjreynolds/sema")
    (synopsis "Rust semaphore library")
    (description "Sema provides a safe Semaphore implementation.")
    (license license:expat)))

(define-public rust-serde-0.7
  (package
    (inherit rust-serde-1)
    (name "rust-serde")
    (version "0.7.15")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "serde" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "14haziy946c4m8h5bgg3c98558rbjim3jnl1c5zjdi4fm8r0f3hv"))))
    (arguments
     `(#:cargo-inputs (("rust-clippy" ,rust-clippy-0.0))))))

(define-public rust-serde-codegen-0.7
  (package
    (name "rust-serde-codegen")
    (version "0.7.15")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "serde-codegen" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1zd81rc49vz1nkl67dh7ymghmy5fsvg8h91kday361c7f2pkcf4p"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t ;syntex-sytnax borrow checker error
       #:cargo-inputs (("rust-aster" ,rust-aster-0.22)
                       ("rust-clippy" ,rust-clippy-0.0)
                       ("rust-quasi" ,rust-quasi-0.16)
                       ("rust-quasi-codegen" ,rust-quasi-codegen-0.16)
                       ("rust-quasi-macros" ,rust-quasi-macros-0.16)
                       ("rust-serde-codegen-internals" ,rust-serde-codegen-internals-0.4)
                       ("rust-syntex" ,rust-syntex-0.39)
                       ("rust-syntex" ,rust-syntex-0.39)
                       ("rust-syntex-syntax" ,rust-syntex-syntax-0.39))))
    (home-page "https://serde.rs")
    (synopsis
     "Macros to auto-generate implementations for the serde framework")
    (description
     "This package provides macros to auto-generate implementations for the
serde framework.")
    (license (list license:expat license:asl2.0))))

(define-public rust-serde-codegen-internals-0.4
  (package
    (name "rust-serde-codegen-internals")
    (version "0.4.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "serde-codegen-internals" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0jgqgrcxavy7r5g6qn3cmj59am8nr977pis4bc12a0j0lwdz3mpf"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t ;syntex-syntax borrow checker error
       #:cargo-inputs (("rust-clippy" ,rust-clippy-0.0)
                       ("rust-syntex-errors" ,rust-syntex-errors-0.39)
                       ("rust-syntex-syntax" ,rust-syntex-syntax-0.39))))
    (home-page "https://serde.rs")
    (synopsis "AST representation used by Serde codegen. Unstable.")
    (description
     "This package provides AST representation used by Serde codegen.")
    (license (list license:expat license:asl2.0))))

(define-public rust-serde-json-0.8
  (package
    (inherit rust-serde-json-1)
    (name "rust-serde-json")
    (version "0.8.6")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "serde-json" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0k3bclzbvzhiscjydqzym887i8mkh726xkf8isf3lln3xplx5xv7"))))
    (arguments
     `(#:cargo-inputs (("rust-clippy" ,rust-clippy-0.0)
                       ("rust-dtoa" ,rust-dtoa-0.2)
                       ("rust-itoa" ,rust-itoa-0.1)
                       ("rust-linked-hash-map" ,rust-linked-hash-map-0.3)
                       ("rust-num-traits" ,rust-num-traits-0.1)
                       ("rust-serde" ,rust-serde-0.8))))))

(define-public rust-serde-urlencoded-0.5
  (package
    (inherit rust-serde-urlencoded-0.7)
    (name "rust-serde-urlencoded")
    (version "0.5.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "serde-urlencoded" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0nhnzllx5xrij4x17g351n14md691r95mxr7sbpz4sl80n8xcbb4"))))
    (arguments
     `(#:cargo-inputs (("rust-dtoa" ,rust-dtoa-0.4)
                       ("rust-itoa" ,rust-itoa-0.4)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-url" ,rust-url-1))
       #:cargo-development-inputs (("rust-serde-derive" ,rust-serde-derive-1))))))

(define-public rust-serde-yaml-0.7
  (package
    (inherit rust-serde-yaml-0.9)
    (name "rust-serde-yaml")
    (version "0.7.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "serde-yaml" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0l9hqmzfwswqvx5gci0hji6497gim73r10bjl6ckq9r8vz9rk07g"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-dtoa" ,rust-dtoa-0.4)
                       ("rust-linked-hash-map" ,rust-linked-hash-map-0.5)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-yaml-rust" ,rust-yaml-rust-0.4))
       #:cargo-development-inputs (("rust-serde-derive" ,rust-serde-derive-1)
                                   ("rust-unindent" ,rust-unindent-0.1)
                                   ("rust-version-sync" ,rust-version-sync-0.5))))))

(define-public rust-servo-fontconfig-0.4
  (package
    (inherit rust-servo-fontconfig-0.5)
    (name "rust-servo-fontconfig")
    (version "0.4.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "servo-fontconfig" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1nach6s4hdf86jz5hlm4p5r7vin91cs7gg89mr533id5fpbzi250"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-servo-fontconfig-sys" ,rust-servo-fontconfig-sys-4))))
    (native-inputs (list cmake))))

(define-public rust-servo-fontconfig-sys-4
  (package
    (inherit rust-servo-fontconfig-sys-5)
    (name "rust-servo-fontconfig-sys")
    (version "4.0.9")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "servo-fontconfig-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0v0mbicy74wd6cjd5jyqnm4nvrrr5lmg053cn16kylhg8mkf3cv2"))))
    (arguments
     `(#:cargo-inputs (("rust-expat-sys" ,rust-expat-sys-2)
                       ("rust-pkg-config" ,rust-pkg-config-0.3)
                       ("rust-servo-freetype-sys" ,rust-servo-freetype-sys-4))))))

(define-public rust-servo-freetype-sys-4
  (package
    (name "rust-servo-freetype-sys")
    (version "4.0.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "servo-freetype-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1z0dvnakans4vn4vlpx4nxg984427lh8dskxxz9pglij1mnwnk1c"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-cmake" ,rust-cmake-0.1)
                       ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "http://www.freetype.org/")
    (synopsis
     "FreeType is a freely available software library to render fonts")
    (description
     "@code{FreeType} is a freely available software library to render
fonts.")
    (native-inputs (list cmake))
    (license (list license:freetype license:gpl2))))

(define-public rust-shell32-sys-0.1
  (package
    (name "rust-shell32-sys")
    (version "0.1.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "shell32-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0b0jjnv8rr4flllvpx9lkfvvxdl3hac53cd2klf14mqz2134pq4y"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-winapi" ,rust-winapi-0.2)
                       ("rust-winapi-build" ,rust-winapi-build-0.1))))
    (home-page "https://github.com/retep998/winapi-rs")
    (synopsis
     "Contains function definitions for the Windows API library shell32")
    (description
     "This package contains function definitions for the Windows API library
shell32, see winapi for types and constants.")
    (license license:expat)))

(define-public rust-shred-0.7
  (package
    (name "rust-shred")
    (version "0.7.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "shred" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1vdz4an3cwchbgb9v64zjny6wxxcshvqg25mrx7i8s1m2gk258bf"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-arrayvec" ,rust-arrayvec-0.4)
                       ("rust-fxhash" ,rust-fxhash-0.2)
                       ("rust-mopa" ,rust-mopa-0.2)
                       ("rust-rayon" ,rust-rayon-1)
                       ("rust-smallvec" ,rust-smallvec-0.6))
       #:cargo-development-inputs (("rust-cgmath" ,rust-cgmath-0.16)
                                   ("rust-shred-derive" ,rust-shred-derive-0.5))))
    (home-page "https://github.com/amethyst/shred")
    (synopsis
     "Dispatches systems in parallel which need read access to some resources,
and write access to others")
    (description
     "This library allows to dispatch systems, which can have
interdependencies, shared and exclusive resource access, in parallel.")
    (license (list license:expat license:asl2.0))))

(define-public rust-shred-0.5
  (package
    (inherit rust-shred-0.7)
    (name "rust-shred")
    (version "0.5.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "shred" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0n91xvx801paa7di8gnngmysvg11kh1cclsbp1xb9afhm7mbqfkx"))))
    (arguments
     `(#:cargo-inputs (("rust-arrayvec" ,rust-arrayvec-0.3)
                       ("rust-fnv" ,rust-fnv-1)
                       ("rust-mopa" ,rust-mopa-0.2)
                       ("rust-pulse" ,rust-pulse-0.5)
                       ("rust-rayon" ,rust-rayon-0.8)
                       ("rust-shred-derive" ,rust-shred-derive-0.3)
                       ("rust-smallvec" ,rust-smallvec-0.4))
       #:cargo-development-inputs (("rust-cgmath" ,rust-cgmath-0.15))))))

(define-public rust-shred-derive-0.5
  (package
    (name "rust-shred-derive")
    (version "0.5.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "shred-derive" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "18ci0vqs6swpjq3kqp83xmrpbjniys8w5bx7mcj30b9hwpjk9kwz"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-0.4)
                       ("rust-quote" ,rust-quote-0.6)
                       ("rust-syn" ,rust-syn-0.15))))
    (home-page "https://github.com/slide-rs/shred/shred-derive")
    (synopsis "Custom derive for shred")
    (description "This package provides a custom derive for shred.")
    (license (list license:expat license:asl2.0))))

(define-public rust-shred-derive-0.3
  (package
    (inherit rust-shred-derive-0.5)
    (name "rust-shred-derive")
    (version "0.3.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "shred-derive" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0jc0hrvkz8fz5yq5h72wcwfb1xn6bsavr8qjswngx4vf7f8r9a54"))))
    (arguments
     `(#:cargo-inputs (("rust-quote" ,rust-quote-0.3)
                       ("rust-syn" ,rust-syn-0.11))))))

(define-public rust-shrev-1
  (package
    (name "rust-shrev")
    (version "1.1.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "shrev" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "06hbw5hxgyg4qnmwmf6bm3hi7pjdm7jm0d533ilvzwfw5wik7sm5"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/rustgd/shrev-rs.git")
    (synopsis "Event channel, meant to be used with `specs`.")
    (description
     "This package provides an event channel, meant to be used with @code{specs}.")
    (license (list license:expat license:asl2.0))))

(define-public rust-sid-0.6
  (package
    (name "rust-sid")
    (version "0.6.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "sid" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1nqvp7qfy7a4cvvzykh1izfvq4f2aac537mskf3v8j0r29ncanmx"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-num-traits" ,rust-num-traits-0.2))))
    (home-page "https://github.com/nical/sid")
    (synopsis "Simple Id")
    (description
     "Tiny crate providing strongly typed ids and an id-based vector.")
    (license (list license:expat license:asl2.0))))

(define-public rust-simba-0.8
  (package
    (name "rust-simba")
    (version "0.8.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "simba" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1bnf7ainywmaz2z67ss1q0bjwccf80c50c50r6hlpay69z4hf586"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-approx" ,rust-approx-0.5)
                       ("rust-cordic" ,rust-cordic-0.1)
                       ("rust-cuda-std" ,rust-cuda-std-0.2)
                       ("rust-cust-core" ,rust-cust-core-0.1)
                       ("rust-decimal" ,rust-decimal-2)
                       ("rust-fixed" ,rust-fixed-1)
                       ("rust-libm" ,rust-libm-0.2)
                       ("rust-num-complex" ,rust-num-complex-0.4)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-packed-simd" ,rust-packed-simd-0.3)
                       ("rust-paste" ,rust-paste-1)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-wkyv" ,rust-rkyv-0.7)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-wide" ,rust-wide-0.7))))
    (home-page "https://github.com/dimforge/simba")
    (synopsis "SIMD algebra for Rust")
    (description
     "This package provides a set of mathematical traits to
facilitate the use of SIMD-based @dfn{Array of Struct of Array} (AoSoA) storage
pattern in Rust.")
    (license license:bsd-3)))

(define-public rust-simplelog-0.12
  (package
    (name "rust-simplelog")
    (version "0.12.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "simplelog" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0sa3hjdifxhcb9lnlg549fr2cc7vz89nygwbih2dbqsx3h20ivmc"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-ansi-term" ,rust-ansi-term-0.12)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-paris" ,rust-paris-1)
                       ("rust-termcolor" ,rust-termcolor-1)
                       ("rust-time" ,rust-time-0.3))))
    (home-page "https://github.com/drakulix/simplelog.rs")
    (synopsis "A simple and easy-to-use logging facility for Rust's log crate")
    (description
     "This package provides a simple and easy-to-use logging facility for Rust's
@code{log} crate.  It aims to be a maintainable, easy to integrate facility for
small to medium sized project.")
    (license (list license:expat license:asl2.0))))

(define-public rust-simple-logger-0.5
  (package
    (name "rust-simple-logger")
    (version "0.5.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "simple-logger" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1gf9qi7s4nig1fy5j6lbqcz0m6qzy2sh1akrpn8kl5221haij1ic"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-log" ,rust-log-0.4)
                       ("rust-time" ,rust-time-0.1))))
    (home-page "https://github.com/borntyping/rust-simple_logger")
    (synopsis "Logger with a readable output format")
    (description "This package provides a logger that prints all messages with
a readable output format.")
    (license license:expat)))

(define-public rust-slab-0.3
  (package
    (inherit rust-slab-0.4)
    (name "rust-slab")
    (version "0.3.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "slab" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "08xw8w61zdfn1094qkq1d554vh5wmm9bqdys8gqqxc4sv2pgrd0p"))))
    (arguments
     `(#:tests? #f))))

(define-public rust-slice-deque-0.3
  (package
    (name "rust-slice-deque") ;Upstream from 0.2
    (version "0.3.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "slice-deque" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "098gvqjw52qw4gac567c9hx3y6hw9al7hjqb5mnvmvydh3i6xvri"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-mach" ,rust-mach-0.3)
                       ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/gnzlbg/slice_deque")
    (synopsis "A double-ended queue that Deref's into a slice.")
    (description
     "This package provides a double-ended queue that Deref's into a slice.")
    (license (list license:expat license:asl2.0))))

(define-public rust-smallvec-0.4
  (package
    (inherit rust-smallvec-1)
    (name "rust-smallvec")
    (version "0.4.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "smallvec" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "047g7arsfik4p4k2pib08sfz6d9r68fn3z4lmc3qir1mwmgmw37r"))))
    (arguments
     `(#:cargo-inputs (("rust-heapsize" ,rust-heapsize-0.4)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-bincode" ,rust-bincode-0.8))))))

(define-public rust-smallvec-0.2
  (package
    (inherit rust-smallvec-1)
    (name "rust-smallvec")
    (version "0.2.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "smallvec" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "04z0bv5pcnwnvij8kfzw56lnib9mjq8bafp120i7q48yvzbbr32c"))))
    (arguments
     `(#:tests? #f))))

(define-public rust-smallvec-0.1
  (package
    (inherit rust-smallvec-1)
    (name "rust-smallvec")
    (version "0.1.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "smallvec" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "042lhr2f1lxh0lgz0p4i59rvkbpp4sdig8m7wjawzkma2a9d3j7w"))))
    (arguments
     `(#:tests? #f))))

(define-public rust-snap-1
  ;; bump upstream
  (package
    (name "rust-snap")
    (version "1.1.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "snap" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0c882cs4wbyi34nw8njpxa729gyi6sj71h8rj4ykbdvyxyv0m7sy"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-development-inputs (("rust-doc-comment" ,rust-doc-comment-0.3))))
    (home-page "https://github.com/BurntSushi/rust-snappy")
    (synopsis "Pure Rust implementation of the Snappy compression algorithm")
    (description
     "This package provides a pure Rust implementation of the Snappy compression
algorithm.  Includes streaming compression and decompression.")
    (license license:bsd-3)))

(define-public rust-spin-sleep-1
  (package
    (name "rust-spin-sleep")
    (version "1.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "spin_sleep" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0d1inb9z45yp9zyba4zac11s2fd8swjmw0n7vda46pq8vc07kyna"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-once-cell" ,rust-once-cell-1)
                       ("rust-winapi" ,rust-winapi-0.3))
       #:cargo-development-inputs (("rust-approx" ,rust-approx-0.5))))
    (home-page "https://github.com/alexheretic/spin-sleep")
    (synopsis
     "Accurate sleeping. Only use native sleep as far as it can be trusted, then spin.")
    (description
     "This library adds a middle ground, using a configurable native
accuracy setting allowing thread::sleep to wait the bulk of a sleep
time, and spin the final section to guarantee accuracy.")
    (license license:asl2.0)))

(define-public rust-spin-sleep-0.3
  (package
    (inherit rust-spin-sleep-1)
    (name "rust-spin-sleep")
    (version "0.3.7")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "spin-sleep" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0361h0cjfqykzigi10k4h1kl4yd3j59xfd4djdw9cnwabzpkc649"))))
    (arguments
     `(#:cargo-inputs (("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-winapi" ,rust-winapi-0.3))
       #:cargo-development-inputs (("rust-approx" ,rust-approx-0.3))))))

(define-public rust-spmc-0.2
  (package
    (inherit rust-spmc-0.3)
    (name "rust-spmc")
    (version "0.2.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "spmc" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1nhbjc65avbb4nffk6b49spbv7rsmmnrppj2qnx39mhwi57spgiw"))))
    (arguments
     `(#:tests? #f
       #:cargo-development-inputs (("rust-loom" ,rust-loom-0.2))))))

(define-public rust-stdweb-0.1
  (package
    (inherit rust-stdweb-0.4)
    (name "rust-stdweb")
    (version "0.1.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "stdweb" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0gjk7ch31a3kgdc39kj4zqinf10yqaf717wanh9kwwbbwg430m7g"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-clippy" ,rust-clippy-0.0)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1))
       #:cargo-development-inputs (("rust-serde-derive" ,rust-serde-derive-1)
                                   ("rust-serde-json" ,rust-serde-json-1))))))

(define-public rust-superluminal-perf-0.1
  (package
    (name "rust-superluminal-perf")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "superluminal-perf" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0q0ykfn04i2qg5zfizp75y4dn2klpvhb6xfwlygq8jiabpgqvvc0"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-superluminal-perf-sys" ,rust-superluminal-perf-sys-0.1))))
    (home-page "https://github.com/EmbarkStudios/superluminal-perf-rs")
    (synopsis
     "Superluminal Performance API for adding user events to profiler captures")
    (description
     "This package provides the Superluminal Performance profiler Rust API
for adding user events to captures.")
    (license (list license:expat license:asl2.0))))

(define-public rust-superluminal-perf-sys-0.1
  (package
    (name "rust-superluminal-perf-sys")
    (version "0.1.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "superluminal-perf-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "05pz0yybf4y2iw3rvqf2crk04zv7610jjm3glhi8hlv2rhms0hh3"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/EmbarkStudios/superluminal-perf-rs")
    (synopsis "Superluminal Performance C API bindings")
    (description
     "This package provides Superluminal Performance C API bindings.")
    (license (list license:expat license:asl2.0))))

(define-public rust-symphonia-0.5
  (package
    (name "rust-symphonia")
    (version "0.5.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "symphonia" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0hfr2h5k2qry70w82lqrws4lrl4mkdhrk6r6vdgjcpq9f2x8vr32"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-symphonia-bundle-flac" ,rust-symphonia-bundle-flac-0.5)
                       ("rust-symphonia-bundle-mp3" ,rust-symphonia-bundle-mp3-0.5)
                       ("rust-symphonia-codec-aac" ,rust-symphonia-codec-aac-0.5)
                       ("rust-symphonia-codec-adpcm" ,rust-symphonia-codec-adpcm-0.5)
                       ("rust-symphonia-codec-alac" ,rust-symphonia-codec-alac-0.5)
                       ("rust-symphonia-codec-pcm" ,rust-symphonia-codec-pcm-0.5)
                       ("rust-symphonia-codec-vorbis" ,rust-symphonia-codec-vorbis-0.5)
                       ("rust-symphonia-core" ,rust-symphonia-core-0.5)
                       ("rust-symphonia-format-isomp4" ,rust-symphonia-format-isomp4-0.5)
                       ("rust-symphonia-format-mkv" ,rust-symphonia-format-mkv-0.5)
                       ("rust-symphonia-format-ogg" ,rust-symphonia-format-ogg-0.5)
                       ("rust-symphonia-format-wav" ,rust-symphonia-format-wav-0.5)
                       ("rust-symphonia-metadata" ,rust-symphonia-metadata-0.5))))
    (home-page "https://github.com/pdeljanov/Symphonia")
    (synopsis "Pure Rust media container and audio decoding library")
    (description "Pure Rust media container and audio decoding library.")
    (license license:mpl2.0)))

(define-public rust-symphonia-bundle-flac-0.5
  (package
    (name "rust-symphonia-bundle-flac")
    (version "0.5.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "symphonia-bundle-flac" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "138574j4cg02a132jz8qh00fy07qinvv06lqz76qzcbw594b08vz"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-log" ,rust-log-0.4)
                       ("rust-symphonia-core" ,rust-symphonia-core-0.5)
                       ("rust-symphonia-metadata" ,rust-symphonia-metadata-0.5)
                       ("rust-symphonia-utils-xiph" ,rust-symphonia-utils-xiph-0.5))))
    (home-page "https://github.com/pdeljanov/Symphonia")
    (synopsis
     "Pure Rust FLAC demuxer and decoder (a part of project Symphonia)")
    (description
     "Pure Rust FLAC demuxer and decoder (a part of project Symphonia).")
    (license license:mpl2.0)))

(define-public rust-symphonia-bundle-mp3-0.5
  (package
    (name "rust-symphonia-bundle-mp3")
    (version "0.5.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "symphonia-bundle-mp3" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "12inws5f7cbkc0pxblwgs6xk70clmv79w6h1fdliwvslrvzdfc8g"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-symphonia-core" ,rust-symphonia-core-0.5)
                       ("rust-symphonia-metadata" ,rust-symphonia-metadata-0.5))))
    (home-page "https://github.com/pdeljanov/Symphonia")
    (synopsis
     "Pure Rust MP1, MP2, and MP3 demuxer and decoder (a part of project Symphonia)")
    (description
     "Pure Rust MP1, MP2, and MP3 demuxer and decoder (a part of project Symphonia).")
    (license license:mpl2.0)))

(define-public rust-symphonia-codec-aac-0.5
  (package
    (name "rust-symphonia-codec-aac")
    (version "0.5.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "symphonia-codec-aac" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "178hd0qlsl1kappjgln1gyyjs326lbz0pljb5aqq8jyf4mdxggb8"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-symphonia-core" ,rust-symphonia-core-0.5))))
    (home-page "https://github.com/pdeljanov/Symphonia")
    (synopsis "Pure Rust AAC decoder (a part of project Symphonia)")
    (description "Pure Rust AAC decoder (a part of project Symphonia).")
    (license license:mpl2.0)))

(define-public rust-symphonia-codec-adpcm-0.5
  (package
    (name "rust-symphonia-codec-adpcm")
    (version "0.5.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "symphonia-codec-adpcm" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0l0iqh5g2409z7gl711v58xkm9sk0l39v1qqcdxqr0axhv0ps3l7"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-log" ,rust-log-0.4)
                       ("rust-symphonia-core" ,rust-symphonia-core-0.5))))
    (home-page "https://github.com/pdeljanov/Symphonia")
    (synopsis "Pure Rust ADPCM audio decoder (a part of project Symphonia)")
    (description
     "Pure Rust ADPCM audio decoder (a part of project Symphonia).")
    (license license:mpl2.0)))

(define-public rust-symphonia-codec-alac-0.5
  (package
    (name "rust-symphonia-codec-alac")
    (version "0.5.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "symphonia-codec-alac" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1f83551nh7ac44y4xjizm7vy4kcxm5rkx45gdxkgz7hw7mvfh9rs"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-log" ,rust-log-0.4)
                       ("rust-symphonia-core" ,rust-symphonia-core-0.5))))
    (home-page "https://github.com/pdeljanov/Symphonia")
    (synopsis "Pure Rust ALAC decoder (a part of project Symphonia)")
    (description "Pure Rust ALAC decoder (a part of project Symphonia).")
    (license license:mpl2.0)))

(define-public rust-symphonia-codec-pcm-0.5
  (package
    (name "rust-symphonia-codec-pcm")
    (version "0.5.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "symphonia-codec-pcm" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1any34qhsfyl4m6h9nal1k6favpgyl8avpg2ihf68sm0439gpwa7"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-log" ,rust-log-0.4)
                       ("rust-symphonia-core" ,rust-symphonia-core-0.5))))
    (home-page "https://github.com/pdeljanov/Symphonia")
    (synopsis "Pure Rust PCM audio decoder (a part of project Symphonia)")
    (description "Pure Rust PCM audio decoder (a part of project Symphonia).")
    (license license:mpl2.0)))

(define-public rust-symphonia-codec-vorbis-0.5
  (package
    (name "rust-symphonia-codec-vorbis")
    (version "0.5.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "symphonia-codec-vorbis" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "00z295bncyyhb022z93pd13qvdcm9xz821a21hsh3ah66mz3jlrr"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-log" ,rust-log-0.4)
                       ("rust-symphonia-core" ,rust-symphonia-core-0.5)
                       ("rust-symphonia-utils-xiph" ,rust-symphonia-utils-xiph-0.5))))
    (home-page "https://github.com/pdeljanov/Symphonia")
    (synopsis "Pure Rust Vorbis decoder (a part of project Symphonia)")
    (description "Pure Rust Vorbis decoder (a part of project Symphonia).")
    (license license:mpl2.0)))

(define-public rust-symphonia-core-0.5
  (package
    (name "rust-symphonia-core")
    (version "0.5.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "symphonia-core" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0hj1fmq6k1ik10fpbawjw1v7nfn9gcn78yycd1970ygfiyw3xizp"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-arrayvec" ,rust-arrayvec-0.7)
                       ("rust-bitflags" ,rust-bitflags-1)
                       ("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-log" ,rust-log-0.4))))
    (home-page "https://github.com/pdeljanov/Symphonia")
    (synopsis "Project Symphonia shared structs, traits, and features")
    (description "Project Symphonia shared structs, traits, and features.")
    (license license:mpl2.0)))

(define-public rust-symphonia-format-isomp4-0.5
  (package
    (name "rust-symphonia-format-isomp4")
    (version "0.5.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "symphonia-format-isomp4" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1yqyg91d8i1va4w35grwscljs96nwm8l3ikb84r20dfgwnx19pzz"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-encoding-rs" ,rust-encoding-rs-0.8)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-symphonia-core" ,rust-symphonia-core-0.5)
                       ("rust-symphonia-metadata" ,rust-symphonia-metadata-0.5)
                       ("rust-symphonia-utils-xiph" ,rust-symphonia-utils-xiph-0.5))))
    (home-page "https://github.com/pdeljanov/Symphonia")
    (synopsis "Pure Rust ISO/MP4 demuxer (a part of project Symphonia)")
    (description "Pure Rust ISO/MP4 demuxer (a part of project Symphonia).")
    (license license:mpl2.0)))

(define-public rust-symphonia-format-mkv-0.5
  (package
    (name "rust-symphonia-format-mkv")
    (version "0.5.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "symphonia-format-mkv" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "15c8sm41iqmd46nyvh3c5b87r3vyc7c33hnq8d05vlhshpy1vipm"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-symphonia-core" ,rust-symphonia-core-0.5)
                       ("rust-symphonia-metadata" ,rust-symphonia-metadata-0.5)
                       ("rust-symphonia-utils-xiph" ,rust-symphonia-utils-xiph-0.5))))
    (home-page "https://github.com/pdeljanov/Symphonia")
    (synopsis "Pure Rust MKV/WebM demuxer (a part of project Symphonia)")
    (description
     "Pure Rust @code{MKV/WebM} demuxer (a part of project Symphonia).")
    (license license:mpl2.0)))

(define-public rust-symphonia-format-ogg-0.5
  (package
    (name "rust-symphonia-format-ogg")
    (version "0.5.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "symphonia-format-ogg" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "13acazi7wnvnv3cxn604cnp7ixs0h216h0wa0i22si8irl6a1wcv"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-log" ,rust-log-0.4)
                       ("rust-symphonia-core" ,rust-symphonia-core-0.5)
                       ("rust-symphonia-metadata" ,rust-symphonia-metadata-0.5)
                       ("rust-symphonia-utils-xiph" ,rust-symphonia-utils-xiph-0.5))))
    (home-page "https://github.com/pdeljanov/Symphonia")
    (synopsis "Pure Rust OGG demuxer (a part of project Symphonia)")
    (description "Pure Rust OGG demuxer (a part of project Symphonia).")
    (license license:mpl2.0)))

(define-public rust-symphonia-format-wav-0.5
  (package
    (name "rust-symphonia-format-wav")
    (version "0.5.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "symphonia-format-wav" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1903xxcaqaqx5zkfhpy9jjzzrn3b758sryydpl1w09zs513n2xns"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-log" ,rust-log-0.4)
                       ("rust-symphonia-core" ,rust-symphonia-core-0.5)
                       ("rust-symphonia-metadata" ,rust-symphonia-metadata-0.5))))
    (home-page "https://github.com/pdeljanov/Symphonia")
    (synopsis "Pure Rust WAV demuxer (a part of project Symphonia)")
    (description "Pure Rust WAV demuxer (a part of project Symphonia).")
    (license license:mpl2.0)))

(define-public rust-symphonia-metadata-0.5
  (package
    (name "rust-symphonia-metadata")
    (version "0.5.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "symphonia-metadata" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1h106cn1g5zq1sg2frb30n6s5yib5xmzcag8pdlf1l1igs9y3hw9"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-encoding-rs" ,rust-encoding-rs-0.8)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-symphonia-core" ,rust-symphonia-core-0.5))))
    (home-page "https://github.com/pdeljanov/Symphonia")
    (synopsis "Project Symphonia multimedia tag and metadata readers")
    (description "Project Symphonia multimedia tag and metadata readers.")
    (license license:mpl2.0)))

(define-public rust-symphonia-utils-xiph-0.5
  (package
    (name "rust-symphonia-utils-xiph")
    (version "0.5.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "symphonia-utils-xiph" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1y2ir4a5f10hp6m050lr545r883zvjznqmrmigzrmml0bdjcll54"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-symphonia-core" ,rust-symphonia-core-0.5)
                       ("rust-symphonia-metadata" ,rust-symphonia-metadata-0.5))))
    (home-page "https://github.com/pdeljanov/Symphonia")
    (synopsis "Project Symphonia utilities for Xiph codecs and formats")
    (description "Project Symphonia utilities for Xiph codecs and formats.")
    (license license:mpl2.0)))

(define-public rust-syntex-0.39
  (package
    (name "rust-syntex")
    (version "0.39.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "syntex" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1g8w87vkl2a3af2j1lmhmnz3g3pf28plm2a0min7axyin67v9nlc"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-syntex-errors" ,rust-syntex-errors-0.39)
                       ("rust-syntex-syntax" ,rust-syntex-syntax-0.39))))
    (home-page "https://github.com/erickt/rust-syntex")
    (synopsis "A library that enables compile time syntax extension expansion")
    (description
     "This package provides a library that enables compile time syntax
extension expansion.")
    (license (list license:expat license:asl2.0))))

(define-public rust-syntex-syntax-0.39
  (package
    (inherit rust-syntex-syntax-0.58)
    (name "rust-syntex-syntax")
    (version "0.39.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "syntex-syntax" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1w1jlizyn33ddz08qp8hpfzm328mznby0vv9q0lggg9srx4wad08"))))
    (arguments
     `(#:skip-build? #t ;Broken dependencies
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.5)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-log" ,rust-log-0.3)
                       ("rust-rustc-serialize" ,rust-rustc-serialize-0.3)
                       ("rust-syntex-errors" ,rust-syntex-errors-0.39)
                       ("rust-syntex-pos" ,rust-syntex-pos-0.39)
                       ("rust-term" ,rust-term-0.4)
                       ("rust-unicode-xid" ,rust-unicode-xid-0.0.3))))))

(define-public rust-syntex-errors-0.39
  (package
    (inherit rust-syntex-errors-0.58)
    (name "rust-syntex-errors")
    (version "0.39.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "syntex-errors" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1l0lmc59rsrkyj05kg80j63bd65pla7pdjzvii1bvswh5gilhavv"))))
    (arguments
     `(#:skip-build? #t ;Broken dependencies
       #:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-log" ,rust-log-0.3)
                       ("rust-rustc-serialize" ,rust-rustc-serialize-0.3)
                       ("rust-syntex-pos" ,rust-syntex-pos-0.39)
                       ("rust-term" ,rust-term-0.4)
                       ("rust-unicode-xid" ,rust-unicode-xid-0.0.3))))))

(define-public rust-syntex-pos-0.39
  (package
    (inherit rust-syntex-pos-0.58)
    (name "rust-syntex-pos")
    (version "0.39.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "syntex-pos" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "14mygrhlakw9rh80d55a3qbwim0nn0nkrrnaxd39ycmh64v2brgh"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-rustc-serialize" ,rust-rustc-serialize-0.3))))))

(define-public rust-take-0.1
  (package
    (name "rust-take")
    (version "0.1.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "take" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1i8p579k9kq21k7pcm4yzbc12xpshl39jfa5c1j6pxf1ia6qcmxi"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/carllerche/take")
    (synopsis "Cell allowing the inner value to be consumed without a
mutable reference")
    (description
     "This package provides a cell allowing the inner value to be consumed
without a mutable reference.")
    (license (list license:expat license:asl2.0))))

(define-public rust-tempfile-2
  (package
    (inherit rust-tempfile-3)
    (name "rust-tempfile")
    (version "2.2.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "tempfile" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1q61byf232rra0vqxp4qp10wwwqsqqd45qjj80ql5f34vgljzkhi"))))
    (arguments
     `(#:cargo-inputs (("rust-kernel32-sys" ,rust-kernel32-sys-0.2)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-rand" ,rust-rand-0.3)
                       ("rust-redox-syscall" ,rust-redox-syscall-0.1)
                       ("rust-winapi" ,rust-winapi-0.2))))))

(define-public rust-thiserror-core-1
  (package
    (name "rust-thiserror-core")
    (version "1.0.38")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "thiserror-core" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "15zlv507q2nypg2aphk1fvfnrqzqks59v0aqrl221frpcigk95qd"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;bad import of thiserror for tests
       #:cargo-inputs (("rust-thiserror-core-impl" ,rust-thiserror-core-impl-1))
       #:cargo-development-inputs (("rust-anyhow" ,rust-anyhow-1)
                                   ("rust-ref-cast" ,rust-ref-cast-1)
                                   ("rust-rustversion" ,rust-rustversion-1)
                                   ("rust-trybuild" ,rust-trybuild-1))))
    (home-page "https://github.com/FlorianUekermann/thiserror/tree/core")
    (synopsis "derive(Error)")
    (description
     "This is a fork of thiserror using the nightly only, experimental
error_in_core feature in no_std environments.")
    (license (list license:expat license:asl2.0))))

(define-public rust-thiserror-core-impl-1
  (package
    (name "rust-thiserror-core-impl")
    (version "1.0.38")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "thiserror-core-impl" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1b32vakaixn8z54813dkv3ymzrkrrv9d151gdg8i8c74a181rb0h"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/FlorianUekermann/thiserror/tree/core")
    (synopsis "Implementation detail of the `thiserror` crate")
    (description
     "This package provides implementation detail of the `thiserror` crate.")
    (license (list license:expat license:asl2.0))))

(define-public rust-thread-profiler-0.3
  (package
    (name "rust-thread-profiler")
    (version "0.3.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "thread-profiler" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "03aa0cw2qgf69i5ynrfn9jyrh73hi9r8sqf6ci10mavl9s871cvi"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-time" ,rust-time-0.1))))
    (home-page "https://github.com/glennw/thread_profiler")
    (synopsis
     "A thread profiling library that outputs profiles in the chromium trace
format")
    (description
     "This package provides a thread profiling library that outputs profiles
in the chromium trace format.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-tracing-tracy-0.4
  (package
    (name "rust-tracing-tracy")
    (version "0.4.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "tracing-tracy" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "02j8rrxkk9js8bvjsxlcadi30fm0vcbk7hrwjww2m119izha0s5n"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-tracing-core" ,rust-tracing-core-0.1)
                       ("rust-tracing-subscriber" ,rust-tracing-subscriber-0.2)
                       ("rust-tracy-client" ,rust-tracy-client-0.10))
       #:cargo-development-inputs (("rust-futures" ,rust-futures-0.3)
                                   ("rust-tokio" ,rust-tokio-0.2)
                                   ("rust-tracing" ,rust-tracing-0.1)
                                   ("rust-tracing-attributes" ,rust-tracing-attributes-0.1)
                                   ("rust-tracing-futures" ,rust-tracing-futures-0.2))))
    (home-page "https://github.com/nagisa/rust_tracy_client")
    (synopsis "Inspect tracing-enabled Rust applications with Tracy")
    (description "This package provdies inspection with tracing-enabled Rust
applications with Tracy.")
    (license (list license:expat license:asl2.0))))

(define-public rust-tracy-client-0.16
  (package
    (name "rust-tracy-client")
    (version "0.16.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "tracy-client" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0wj50dazg2m9073jk16v9ki7sclbx62xxsipcjw6ggd7pj36qvjl"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-loom" ,rust-loom-0.5)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-tracy-client-sys" ,rust-tracy-client-sys-0.21))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.3))))
    (home-page "https://github.com/nagisa/rust_tracy_client")
    (synopsis
     "High level bindings to the client libraries for the Tracy profiler")
    (description
     "This package provides high level bindings to the client libraries for
the Tracy profiler.")
    (license (list license:expat license:asl2.0))))

(define-public rust-tracy-client-0.10
  (package
    (inherit rust-tracy-client-0.16)
    (name "rust-tracy-client")
    (version "0.10.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "tracy-client" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0lx4rs19i1cm4klmpxi4cgj1alhibwcql6q1a153apm0gjrwv34y"))))
    (arguments
     `(#:cargo-inputs (("rust-tracy-client-sys" ,rust-tracy-client-sys-0.11))))))

(define-public rust-tracy-client-sys-0.21
  (package
    (name "rust-tracy-client-sys")
    (version "0.21.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "tracy-client-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0nj5h311wg4qkdw0v8s1390ad29597qxcvfp8135aj7h7bm1bf9c"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-cc" ,rust-cc-1))))
    (home-page "https://github.com/nagisa/rust_tracy_client")
    (synopsis
     "Low level bindings to the client libraries for the Tracy profiler")
    (description
     "This package provides low level bindings to the client libraries for
the Tracy profiler")
    (license (list license:asl2.0 license:bsd-3 license:expat))))

(define-public rust-tracy-client-sys-0.11
  (package
    (inherit rust-tracy-client-sys-0.21)
    (name "rust-tracy-client-sys")
    (version "0.11.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "tracy-client-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "18l67jx96m5bd3gq6inl59q5fhzcj9maiaxm588bcmc1hzvz1rhy"))))
    (arguments
     `(#:cargo-inputs (("rust-cc" ,rust-cc-1))))
    (license (list license:expat license:asl2.0))))

(define-public rust-try-lock-0.1
  (package
    (inherit rust-try-lock-0.2)
    (name "rust-try-lock")
    (version "0.1.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "try-lock" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1hp76pyzyxhcxxjacf083gpp6gf8cqwkg188yy02i2a3axqs8apf"))))))

(define-public rust-tokio-proto-0.1
  (package
    (name "rust-tokio-proto")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "tokio-proto" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "12833cckniq3y83zjhk2ayv6qpr99d4mj1h3hz266g1mh6p4gfwg"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-futures" ,rust-futures-0.1)
                       ("rust-log" ,rust-log-0.3)
                       ("rust-net2" ,rust-net2-0.2)
                       ("rust-rand" ,rust-rand-0.3)
                       ("rust-slab" ,rust-slab-0.3)
                       ("rust-smallvec" ,rust-smallvec-0.2)
                       ("rust-take" ,rust-take-0.1)
                       ("rust-tokio-core" ,rust-tokio-core-0.1)
                       ("rust-tokio-io" ,rust-tokio-io-0.1)
                       ("rust-tokio-service" ,rust-tokio-service-0.1))
       #:cargo-development-inputs (("rust-bytes" ,rust-bytes-0.4)
                                   ("rust-env-logger" ,rust-env-logger-0.3)
                                   ("rust-lazycell" ,rust-lazycell-0.4)
                                   ("rust-mio" ,rust-mio-0.6))))
    (home-page "https://tokio.rs")
    (synopsis
     "Network application framework for rapid development and highly scalable
production deployments of clients and servers")
    (description
     "This package provides a network application framework for rapid development and
highly scalable production deployments of clients and servers.")
    (license (list license:expat license:asl2.0))))

(define-public rust-tokio-service-0.1
  (package
    (name "rust-tokio-service")
    (version "0.1.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "tokio-service" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0qjinhax0y164kxp887mj8c5ih9829kdrnrb2ramzwg0fz825ni4"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-futures" ,rust-futures-0.1))))
    (home-page "https://github.com/tokio-rs/tokio-service")
    (synopsis "Core `Service` trait for Tokio")
    (description "This package provides the core `Service` trait for Tokio.")
    (license (list license:expat license:asl2.0))))

(define-public rust-tokio-tls-0.1
  (package
    (inherit rust-tokio-tls-0.3)
    (name "rust-tokio-tls")
    (version "0.1.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "tokio-tls" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "04yrdscn8m9qza8ms09pqipbmj6x2q64jgm5n3ipy4b0wl24nbvp"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-futures" ,rust-futures-0.1)
                       ("rust-native-tls" ,rust-native-tls-0.1)
                       ("rust-tokio-core" ,rust-tokio-core-0.1)
                       ("rust-tokio-io" ,rust-tokio-io-0.1)
                       ("rust-tokio-proto" ,rust-tokio-proto-0.1))
       #:cargo-development-inputs (("rust-cfg-if" ,rust-cfg-if-0.1)
                                   ("rust-env-logger" ,rust-env-logger-0.4)
                                   ("rust-hyper" ,rust-hyper-0.11)
                                   ("rust-openssl" ,rust-openssl-0.9)
                                   ("rust-schannel" ,rust-schannel-0.1)
                                   ("rust-security-framework" ,rust-security-framework-0.1)
                                   ("rust-tokio-service" ,rust-tokio-service-0.1)
                                   ("rust-winapi" ,rust-winapi-0.3))))))

(define-public rust-token-store-0.1
  (package
    (name "rust-token-store")
    (version "0.1.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "token-store" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0j8qws13w6x53j1n5k814igbvdr0hd8cca8mkhxi04gwfn1q71m6"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/vberger/token_store")
    (synopsis "A token-based store for arbitrary values")
    (description
     "This package provides a token-based store for arbitrary values.")
    (license license:expat)))

(define-public rust-tuple-utils-0.2
  (package
    (name "rust-tuple-utils")
    (version "0.2.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "tuple-utils" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0zhfnby28530hks9203wfs4aambsczr6mi1i7dmyk8zhp1xwvznb"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/csherratt/tuple_utils")
    (synopsis
     "A set of utilities to enable higher level operations over tuples")
    (description
     "This package provides a set of utilities to enable higher level
operations over tuples.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-twox-hash-1
  ;; bump in upstream
  (package
    (name "rust-twox-hash")
    (version "1.6.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "twox-hash" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0xgn72j36a270l5ls1jk88n7bmq2dhlfkbhdh5554hbagjsydzlp"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-cfg-if" ,rust-cfg-if-0.1)
                       ("rust-digest" ,rust-digest-0.8)
                       ("rust-digest" ,rust-digest-0.9)
                       ("rust-rand" ,rust-rand-0.7)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-static-assertions" ,rust-static-assertions-1))
       #:cargo-development-inputs (("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/shepmaster/twox-hash")
    (synopsis "Rust implementation of the XXHash and XXH3 algorithms")
    (description "This package provides a Rust implementation of the XXHash
and XXH3 algorithms.")
    (license license:expat)))

(define-public rust-type-uuid-0.1
  (package
    (name "rust-type-uuid")
    (version "0.1.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "type-uuid" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0lf0yk8rmxrw4rmw76gzm3jbi0ax81d3cp85n34hacpl8753n10m"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-amethyst" ,rust-amethyst-0.10)
                       ("rust-type-uuid-derive" ,rust-type-uuid-derive-0.1))
       #:cargo-development-inputs (("rust-uuid" ,rust-uuid-0.8))))
    (home-page "https://github.com/randomPoison/type-uuid")
    (synopsis "Safe, stable IDs for Rust types")
    (description "This package provides safe, stable IDs for Rust types.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-type-uuid-derive-0.1
  (package
    (name "rust-type-uuid-derive")
    (version "0.1.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "type-uuid-derive" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1hbdg4bwz87p97gn340q9pasmzwk4h19ddbzrxp7874nazl0w2cf"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-1)
                       ("rust-uuid" ,rust-uuid-0.8))))
    (home-page "https://github.com/randomPoison/type-uuid")
    (synopsis "Custom derive for the type-uuid crate")
    (description
     "This package provides a custom derive for the type-uuid crate")
    (license (list license:asl2.0 license:expat))))

(define-public rust-typetag-0.1
  (package
    (name "rust-typetag")
    (version "0.1.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "typetag" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "12jva00k063gb48bvx0p0ixwbq1l48411disynzvah92bd65d020"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-erased-serde" ,rust-erased-serde-0.3)
                       ("rust-inventory" ,rust-inventory-0.2)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-typetag-impl" ,rust-typetag-impl-0.1))
       #:cargo-development-inputs (("rust-bincode" ,rust-bincode-1)
                                   ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/dtolnay/typetag")
    (synopsis "Serde serializable and deserializable trait objects")
    (description
     "This package provides serde serializable and deserializable trait
objects.")
    (license (list license:expat license:asl2.0))))

(define-public rust-typetag-impl-0.1
  (package
    (name "rust-typetag-impl")
    (version "0.1.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "typetag-impl" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "03lw15ad39bgr4m6fmr5b9lb4wapkcfsnfxsbz0362635iw4f0g6"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/dtolnay/typetag")
    (synopsis "Implementation detail of the typetag crate")
    (description
     "This package provides the implementation detail of the typetag crate.")
    (license (list license:expat license:asl2.0))))

(define-public rust-unic-langid-0.3
  (package
    (name "rust-unic-langid")
    (version "0.3.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "unic-langid" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0iipg5v4b3089g60mlh6nqjazmbbd1zqvmmp4m2h086cv6j11sys"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.2)
                                   ("rust-serde" ,rust-serde-1)
                                   ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/zbraniecki/unic-locale")
    (synopsis "API for managing Unicode Language Identifiers")
    (description
     "This package provides API for managing Unicode Language Identifiers.")
    (license (list license:expat license:asl2.0))))

(define-public rust-unicode-xid-0.0.3
  (package
    (inherit rust-unicode-xid-0.2)
    (name "rust-unicode-xid")
    (version "0.0.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "unicode-xid" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1sqvl06884cy3hh14shik5afcv6bhsvb0gh2y267rv5lmyfg1prn"))))
    (arguments
     `(#:tests? #f))))

(define-public rust-user32-sys-0.1
  (package
    (inherit rust-user32-sys-0.2)
    (name "rust-user32-sys")
    (version "0.1.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "user32-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "02pqzgm7qfdvlb0zqry98h76zwvaq19idd99i0ch8b4m7fc1kdz6"))))
    (arguments
     `(#:cargo-inputs (("rust-winapi" ,rust-winapi-0.2)
                       ("rust-winapi-build" ,rust-winapi-build-0.1))))))

(define-public rust-version-sync-0.5
  (package
    (inherit rust-version-sync-0.9)
    (name "rust-version-sync")
    (version "0.5.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "version-sync" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0yrdarbyzkkcfh16qxh0zvfyxgdl2dgcibjdyk17hjk11b072hrm"))))
    (arguments
     `(#:cargo-inputs (("rust-itertools" ,rust-itertools-0.7)
                       ("rust-pulldown-cmark" ,rust-pulldown-cmark-0.1)
                       ("rust-semver-parser" ,rust-semver-parser-0.7)
                       ("rust-syn" ,rust-syn-0.11)
                       ("rust-toml" ,rust-toml-0.4)
                       ("rust-url" ,rust-url-1))))))

(define-public rust-vorbis-0.1
  (package
    (name "rust-vorbis")
    (version "0.1.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "vorbis" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0mkn538sz9fc3jpcicwdy6licbrwfyfg1bypsj48n4i48pjr62bn"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-ogg-sys" ,rust-ogg-sys-0.0.9)
                       ("rust-rand" ,rust-rand-0.3)
                       ("rust-vorbis-encoder" ,rust-vorbis-encoder-0.1)
                       ("rust-vorbis-sys" ,rust-vorbis-sys-0.0.8)
                       ("rust-vorbisfile-sys" ,rust-vorbisfile-sys-0.0.8))))
    (home-page "https://github.com/tomaka/vorbis-rs")
    (synopsis "High-level bindings for the official libvorbis library")
    (description "High-level bindings for the official libvorbis library.")
    (license license:asl2.0)))

(define-public rust-vorbis-encoder-0.1
  (package
    (name "rust-vorbis-encoder")
    (version "0.1.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "vorbis-encoder" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1g4iafccg2j20gjcxxniibz7i5r6l5lndf0vk4qd4v85vv6npdiz"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-gcc" ,rust-gcc-0.3)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-ogg-sys" ,rust-ogg-sys-0.0.9)
                       ("rust-pkg-config" ,rust-pkg-config-0.3)
                       ("rust-vorbis-sys" ,rust-vorbis-sys-0.0.8))))
    (home-page "https://github.com/tomaka/vorbis-rs")
    (synopsis "Interface for the libvorbis-encoder library")
    (description "Interface for the libvorbis-encoder library.")
    (license license:expat)))

(define-public rust-vorbis-sys-0.1
  (package
    (name "rust-vorbis-sys")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "vorbis-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1zgv7lwa4b2z091g25h83zil8bawk4frc1f0ril5xa31agpxd7mx"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-ogg-sys" ,rust-ogg-sys-0.0.9)
                       ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/tomaka/vorbis-rs")
    (synopsis "FFI for the libvorbis library")
    (description "FFI for the libvorbis library")
    (license license:expat)))

(define-public rust-vorbis-sys-0.0.8
  (package
    (inherit rust-vorbis-sys-0.1)
    (name "rust-vorbis-sys")
    (version "0.0.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "vorbis-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "17aqqnrq54gl1za6jfx50dsp0i7g88fwv84ws7kb8l2q74aiz7kj"))))))

(define-public rust-vorbisfile-sys-0.0.8
  (package
    (name "rust-vorbisfile-sys")
    (version "0.0.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "vorbisfile-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1la2j2zbzdjd93byz21ij58c540bfn1r9pi0bssrjimcw7bhchsg"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-gcc" ,rust-gcc-0.3)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-ogg-sys" ,rust-ogg-sys-0.0.9)
                       ("rust-pkg-config" ,rust-pkg-config-0.3)
                       ("rust-vorbis-sys" ,rust-vorbis-sys-0.1))))
    (home-page "https://github.com/tomaka/vorbis-rs")
    (synopsis "FFI for the vorbisfile library")
    (description "FFI for the vorbisfile library")
    (license license:expat)))

(define-public rust-want-0.0
  (package
    (inherit rust-want-0.3)
    (name "rust-want")
    (version "0.0.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "want" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1l9mbh4a0r2m3s8nckhy1vz9qm6lxsswlgxpimf4pyjkcyb9spd0"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-futures" ,rust-futures-0.1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-try-lock" ,rust-try-lock-0.1))))))

(define-public rust-wayland-kbd-0.13
  (package
    (name "rust-wayland-kbd")
    (version "0.13.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-kbd" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0xkgc5288x89h5jx2wz89zbr79l4dm2rwrc1swlrbnhpk4fgpq2g"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-dlib" ,rust-dlib-0.4)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-memmap" ,rust-memmap-0.6)
                       ("rust-wayland-client" ,rust-wayland-client-0.12))
       #:cargo-development-inputs (("rust-byteorder" ,rust-byteorder-1)
                                   ("rust-tempfile" ,rust-tempfile-2)
                                   ("rust-wayland-client" ,rust-wayland-client-0.12))))
    (home-page "https://github.com/smithay/wayland-kbd")
    (synopsis "Keyboard mapping utility for wayland-client using libxkbcommon")
    (description
     "This package provides a keyboard mapping utility for wayland-client
using libxkbcommon.")
    (license license:expat)))

(define-public rust-wayland-kbd-0.9
  (package
    (inherit rust-wayland-kbd-0.13)
    (name "rust-wayland-kbd")
    (version "0.9.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-kbd" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "13p52qa51cszf5h51q82yjp77844mirqcp61456qzr4lm085lj3m"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.7)
                       ("rust-dlib" ,rust-dlib-0.3)
                       ("rust-lazy-static" ,rust-lazy-static-0.2)
                       ("rust-memmap" ,rust-memmap-0.4)
                       ("rust-wayland-client" ,rust-wayland-client-0.9))
       #:cargo-development-inputs (("rust-byteorder" ,rust-byteorder-1)
                                   ("rust-tempfile" ,rust-tempfile-2)
                                   ("rust-wayland-client" ,rust-wayland-client-0.9))))))

(define-public rust-wayland-kbd-0.6
  (package
    (inherit rust-wayland-kbd-0.13)
    (name "rust-wayland-kbd")
    (version "0.6.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-kbd" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1w8y10b2y2dahp6hk7gl7dwa87xxb9h82picbbcq5kkc7pa6jjrb"))))
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.7)
                       ("rust-dlib" ,rust-dlib-0.3)
                       ("rust-lazy-static" ,rust-lazy-static-0.2)
                       ("rust-memmap" ,rust-memmap-0.4)
                       ("rust-wayland-client" ,rust-wayland-client-0.7))
       #:cargo-development-inputs (("rust-byteorder" ,rust-byteorder-1)
                                   ("rust-tempfile" ,rust-tempfile-2)
                                   ("rust-wayland-client" ,rust-wayland-client-0.7))))))

(define-public rust-wayland-kbd-0.3
  (package
    (inherit rust-wayland-kbd-0.13)
    (name "rust-wayland-kbd")
    (version "0.3.6")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "wayland-kbd" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0l5hirs1r1s1mwc9japw9xb1gsms893x5v7zprvhga8x9kl11g3k"))))
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.7)
                       ("rust-dlib" ,rust-dlib-0.3)
                       ("rust-lazy-static" ,rust-lazy-static-0.1)
                       ("rust-memmap" ,rust-memmap-0.2)
                       ("rust-wayland-client" ,rust-wayland-client-0.5))
       #:cargo-development-inputs (("rust-wayland-client" ,rust-wayland-client-0.5))))))

(define-public rust-windows-0.44
  (package
    (inherit rust-windows-0.46)
    (name "rust-windows")
    (version "0.44.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "windows" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0ax1ip82dhszxz4hhsrdj3b0681xw6axahnfldxcgi506nmmsx4y"))))
    (arguments
     `(#:skip-build? #t ;Windows only!
       #:cargo-inputs (("rust-windows-implement" ,rust-windows-implement-0.44)
                       ("rust-windows-interface" ,rust-windows-interface-0.44)
                       ("rust-windows-targets" ,rust-windows-targets-0.42))))))

(define-public rust-windows-0.37
  (package
    (inherit rust-windows-0.46)
    (name "rust-windows")
    (version "0.37.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "windows" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0ix6p30bq9srmbwyyxn0kgwy1x5dwg9b1ammhlfcck1ldcc47dap"))))
    (arguments
     `(#:skip-build? #t ;Windows only!
       #:cargo-inputs (("rust-windows-implement" ,rust-windows-implement-0.37)
                       ("rust-windows-interface" ,rust-windows-interface-0.37)
                       ("rust-windows-aarch64-msvc" ,rust-windows-aarch64-msvc-0.37)
                       ("rust-windows-i686-gnu" ,rust-windows-i686-gnu-0.37)
                       ("rust-windows-i686-msvc" ,rust-windows-i686-msvc-0.37)
                       ("rust-windows-x86-64-gnu" ,rust-windows-x86-64-gnu-0.37)
                       ("rust-windows-x86-64-msvc" ,rust-windows-x86-64-msvc-0.37))))))

(define-public rust-windows-aarch64-msvc-0.37
  (package
    (inherit rust-windows-aarch64-msvc-0.48)
    (name "rust-windows-aarch64-msvc")
    (version "0.37.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "windows-aarch64-msvc" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0flwfsknwn9kpccb5cb6dvdyvsydz4y0z32p7fx1dhnin9y2f8r6"))))))

(define-public rust-windows-i686-gnu-0.37
  (package
    (inherit rust-windows-i686-gnu-0.48)
    (name "rust-windows-i686-gnu")
    (version "0.37.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "windows-i686-gnu" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1qfr0yxg8ihg08lddg29xh1rfsgra262gdnl8h6p615qn385z4nk"))))))

(define-public rust-windows-i686-msvc-0.37
  (package
    (inherit rust-windows-i686-msvc-0.48)
    (name "rust-windows-i686-msvc")
    (version "0.37.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "windows-i686-msvc" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0z650xnnk0r0vdsf5mc4phqv12srpzpq71i9q4jbacg39z3pm46f"))))))

(define-public rust-windows-implement-0.44
  (package
    (inherit rust-windows-0.46)
    (name "rust-windows-implement")
    (version "0.44.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "windows-implement" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1ij5q9khlcfn43a1p3ypjbn711k50s9pc8la5bf04ys1wfl7rs3c"))))))

(define-public rust-windows-implement-0.37
  (package
    (inherit rust-windows-implement-0.46)
    (name "rust-windows-implement")
    (version "0.37.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "windows-implement" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "055v4rdxj9p9s068bq4ya93imi3fgi7ysc0izmk9szazalp0d8b7"))))
    (arguments
     `(#:skip-build? #t ;Windows only!
       #:cargo-inputs (("rust-syn" ,rust-syn-1)
                       ("rust-windows-tokens" ,rust-windows-tokens-0.37))))))

(define-public rust-windows-interface-0.44
  (package
    (inherit rust-windows-interface-0.46)
    (name "rust-windows-interface")
    (version "0.44.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "windows-interface" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0zwwwfzjdf087gvgy48bbfq9yd0fsh1fj5wzs88gim7cj6jnjgw5"))))))

(define-public rust-windows-interface-0.37
  (package
    (inherit rust-windows-interface-0.46)
    (name "rust-windows-interface")
    (version "0.37.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "windows-interface" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "09frbkwg0f0akq13cvkpwcq18p96h3h2rggxjw5hj4nwj00449p4"))))))

(define-public rust-windows-tokens-0.37
  (package
    (name "rust-windows-tokens") ;more upstream from rust-windows-0.32
    (version "0.37.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "windows-tokens" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0sbim19lgiik8g18s82wqf3aks29p430rwcgnyarjhbh25gx4qrj"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/microsoft/windows-rs")
    (synopsis "Code gen support for the windows crate")
    (description "This package provides code generation support for the
windows crate.")
    (license (list license:expat license:asl2.0))))

(define-public rust-windows-x86-64-gnu-0.37
  (package
    (inherit rust-windows-x86-64-gnu-0.48)
    (name "rust-windows-x86-64-gnu")
    (version "0.37.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "windows-x86-64-gnu" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0ggk1ahj7b2rk4i76qjmxz77gdfbgm142lvx8lr6nblghahgparb"))))))

(define-public rust-windows-x86-64-msvc-0.37
  (package
    (inherit rust-windows-x86-64-msvc-0.48)
    (name "rust-windows-x86-64-msvc")
    (version "0.37.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "windows-x86-64-msvc" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1795abfwjzq4rnx675lfjj0igyqqcfwdabl26dxwz11dvz3nvpgl"))))))

(define-public rust-xcb-0.8
  (package
    (inherit rust-xcb-0.9)
    (name "rust-xcb")
    (version "0.8.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "xcb" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1ph27r9nxyfy3hh1c7x85g6dciwxcinf6514pvw9ybhl4hzpm4ay"))))))

(define-public rust-xi-unicode-0.2
  (package
    (inherit rust-xi-unicode-0.3)
    (name "rust-xi-unicode")
    (version "0.2.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "xi-unicode" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1cj8pp53w6nlxwrzmfy247b8q6i09maqfhc3bi5szgxqn7c8a6z7"))))))

(define-public rust-xkbcommon-0.5
  (package
    (name "rust-xkbcommon")
    (version "0.5.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "xkbcommon" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "123b96fbp693z43z2f2gbadb9dzf523n2d21j3izcaz9k6sjbnsj"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-as-raw-xcb-connection" ,rust-as-raw-xcb-connection-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-memmap2" ,rust-memmap2-0.7))
       #:cargo-development-inputs (("rust-evdev" ,rust-evdev-0.11))))
    (inputs (list libxkbcommon))
    (home-page "https://github.com/rust-x-bindings/xkbcommon-rs")
    (synopsis "Rust bindings and wrappers for libxkbcommon")
    (description
     "xkbcommon-rs is a set of bindings and safe wrappers for libxkbcommon.")
    (license license:expat)))

(define-public rust-xml-rs-0.6
  (package
    (inherit rust-xml-rs-0.8)
    (name "rust-xml-rs")
    (version "0.6.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "xml-rs" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "18g7krn8zx8ifml83w91w2hvw437j5q3vaw4cvx3kryccj5860pl"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1))))))

(define-public rust-xml-rs-0.3
  (package
    (inherit rust-xml-rs-0.8)
    (name "rust-xml-rs")
    (version "0.3.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "xml-rs" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0fzyr8892g9zry6f17pnfdaxlsaqmyx64wbxgagldik0haln94kj"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.7))))))

(define-public rust-xml-rs-0.2
  (package
    (inherit rust-xml-rs-0.8)
    (name "rust-xml-rs")
    (version "0.2.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "xml-rs" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "02vagvn411nxi1kzbf8nn7gy2hjmxxfhnqwg2ls05j039y7j2zpf"))))
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-0.3))))))

(define-public rust-xml-rs-0.1
  (package
    (inherit rust-xml-rs-0.8)
    (name "rust-xml-rs")
    (version "0.1.26")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "xml-rs" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "068mcrpww6x7mlxcwr80hfhmr2vd6fgv59sanp9jvnr45gc8zb2b"))))
    (arguments
     `(#:skip-build? #t ;broken crate
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-2))))))

(define-public rust-zstd-0.12
  ;; bump upstream
  (package
    (name "rust-zstd")
    (version "0.12.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "zstd" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0g654jj8z25rvzli2b1231pcp9y7n6vk44jaqwgifh9n2xg5j9qs"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f ;Not all files included
       #:cargo-inputs (("rust-zstd-safe" ,rust-zstd-safe-6))
       #:cargo-development-inputs (("rust-clap" ,rust-clap-4)
                                   ("rust-humansize" ,rust-humansize-2)
                                   ("rust-partial-io" ,rust-partial-io-0.5)
                                   ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "https://github.com/gyscos/zstd-rs")
    (synopsis "Binding for the zstd compression library")
    (description
     "This package provides a binding to the Zstd compression library.")
    (license license:expat)))

(define-public rust-zstd-safe-6
  ;; bump upstream
  (package
    (name "rust-zstd-safe")
    (version "6.0.6")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "zstd-safe" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "10cm0v8sw3jz3pi0wlwx9mbb2l25lm28w638a5n5xscfnk8gz67f"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-zstd-sys" ,rust-zstd-sys-2))))
    (home-page "https://github.com/gyscos/zstd-rs")
    (synopsis "Safe low-level bindings for the zstd compression library")
    (description
     "This package provides safe low-level bindings to the zstd compression
library.")
    (license (list license:expat license:asl2.0))))
